title: New Release: Tor Browser 10.5.2 (Windows, macOS, Linux)
---
pub_date: 2021-07-13
---
author: sysrqb
---
tags:

tor browser
tbb-10.5
tbb
---
categories: applications
---
summary: Tor Browser 10.5.2 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.5.2 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5.2/">distribution directory</a>.</p>
<p>This version updates Firefox to 78.12.0esr. This version includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-29/">security updates</a> to Firefox.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser will <em><b>stop</b> supporting <u>version 2 onion services</u></em> later <em><b>this year</b></em>. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.5">Tor Browser 10.5</a>:</p>
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.12.0esr</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40497">Bug 40497</a>: Cannot set multiple pages as home pages in 10.5a17</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40507">Bug 40507</a>: Full update is not downloaded after applying partial update fails</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40510">Bug 40510</a>: open tabs get redirected to about:torconnect on restart</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292471"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292471" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>CentosUser (not verified)</span> said:</p>
      <p class="date-time">July 28, 2021</p>
    </div>
    <a href="#comment-292471">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292471" class="permalink" rel="bookmark">Starting with release 10.5,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Starting with release 10.5, TorBrowser will not launch under CentOS7:<br />
./start-tor-browser.desktop --verbose<br />
Launching './Browser/start-tor-browser --detach --verbose'...<br />
Fontconfig warning: "/opt/tor-browser_en-US/Browser/TorBrowser/Data/fontconfig/fonts.conf", line 85: unknown element "blank"<br />
/opt/tor-browser_en-US/Browser/firefox.real: /lib64/libc.so.6: version `GLIBC_2.18' not found (required by ./TorBrowser/Tor/libstdc++/libstdc++.so.6)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292474"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292474" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>MiniMe (not verified)</span> said:</p>
      <p class="date-time">July 28, 2021</p>
    </div>
    <a href="#comment-292474">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292474" class="permalink" rel="bookmark">Is there a aggrigated list…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there a aggrigated list of all "security fixes" listed by release version?</p>
<p>I have gotten in the habiy of only updating for security fixes as other fixes tend to come with their own added bug bonus.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292684"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292684" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 25, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292474" class="permalink" rel="bookmark">Is there a aggrigated list…</a> by <span>MiniMe (not verified)</span></p>
    <a href="#comment-292684">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292684" class="permalink" rel="bookmark">The ChangeLog is linked in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There isn't one for Tor Browser, but there is for tor(.exe). Tor Browser inherits updates from Firefox and doesn't go out of the way to change merges from Mozilla that don't look like they would lower privacy or security. For someone like you, it's better to let stable Tor Browser update automatically but get involved with issue reports for Tor Browser so you can help catch "bonuses" in alphas before they pass into stable releases.</p>
<p>For the Tor Browser Bundle, the ChangeLog is linked in blog posts for the stable, non-alpha releases.<br />
<a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt" rel="nofollow">https://gitweb.torproject.org/builders/tor-browser-build.git/plain/proj…</a></p>
<p>Its git directory is here:<br />
<a href="https://gitweb.torproject.org/builders/tor-browser-build.git/tree/" rel="nofollow">https://gitweb.torproject.org/builders/tor-browser-build.git/tree/</a></p>
<p>Its GitLab repositories, where issues are reported, are here:<br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues</a><br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issu…</a></p>
<p>For tor(.exe), the ReleaseNotes list the "major" and "minor" fixes for stable versions of tor.exe that are included in the Tor Browser Bundles. The ChangeLog lists fixes for all versions of tor.exe including alpha versions that are never included in the Tor Browser Bundles.<br />
<a href="https://gitweb.torproject.org/tor.git/plain/ReleaseNotes" rel="nofollow">https://gitweb.torproject.org/tor.git/plain/ReleaseNotes</a><br />
<a href="https://gitweb.torproject.org/tor.git/plain/ChangeLog" rel="nofollow">https://gitweb.torproject.org/tor.git/plain/ChangeLog</a><br />
<a href="https://gitlab.torproject.org/tpo/core/tor/-/issues" rel="nofollow">https://gitlab.torproject.org/tpo/core/tor/-/issues</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292478"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292478" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2021</p>
    </div>
    <a href="#comment-292478">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292478" class="permalink" rel="bookmark">Can you give an example when…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you give an example when snowflake is a better choice than obfs 4 for a bridge?</p>
</div>
  </div>
</article>
<!-- Comment END -->
