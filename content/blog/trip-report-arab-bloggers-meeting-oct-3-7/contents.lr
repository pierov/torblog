title: Trip report, Arab Bloggers Meeting, Oct 3-7
---
pub_date: 2011-10-16
---
author: arma
---
tags:

training
internet censorship
arab spring
surveillance
trip report
---
categories:

circumvention
community
reports
---
_html_body:

<p>Jake, Arturo, and I went to <a href="http://arabloggers.com/blog/" rel="nofollow">Tunisia</a> Oct 3-7 to teach a bunch of bloggers from Arab countries about Tor and more generally about Internet security and privacy. The previous meetings were in Lebanon; it's amazing to reflect that the world has changed enough that <a href="http://samibengharbia.com/" rel="nofollow">Sami</a> can hold it in his home country now.</p>

<p>The conference was one day of keynotes with lots of press attention, and then three days of <a href="http://en.wikipedia.org/wiki/Unconference" rel="nofollow">unconference</a>-style workshops. </p>

<p>On the keynote day, Jake and Arturo did a talk on mobile privacy, pointing out the wide variety of ways that the telephone network is "the best surveillance tool ever invented". The highlight for the day was when Moez Chakchouk, the head of the Tunisian Internet Agency (ATI), did a talk explicitly stating that Tunisia had been using Smartfilter since 2002, that Smartfilter had been giving Tunisia discounts in exchange for beta-testing their products for other countries in the region like Saudi Arabia, and that it was time for Tunisia to stop wasting money on expensive filters that aren't good for the country anyway.</p>

<p>We did a four-hour Tor training on the first workshop day. We covered what to look for in a circumvention or privacy tool (open source good, open design good, open analysis of security properties good, centralization bad). All the attendees left with a working Tor Browser Bundle install (well, all the attendees except the fellow with the ipad). We got many of them to install <a href="http://www.pidgin.im/" rel="nofollow">Pidgin</a> and <a href="http://www.cypherpunks.ca/otr/" rel="nofollow">OTR</a> as well, but ran into some demo bugs around the Jabber connect server config that derailed some users. I look forward to having the <a href="https://blog.torproject.org/blog/tor-im-browser-bundle-discontinued-temporarily" rel="nofollow">Tor IM Browser Bundle</a> back in action now that we've fixed some Pidgin security bugs. </p>

<p>We did a three-hour general security and privacy Q&amp;A on the second workshop day, covering topics like whether Skype is safe, how else can they do VoIP, how can they trust various software, a demo of what sniffing the network can show, iphone vs android vs blackberry, etc. It ended with a walk-through of how <em>*we*</em> keep our laptops secure, so people could see how far down the rabbit hole they can go.</p>

<p>Syria and Israel seem to be the scariest adversaries in the area right now, in terms of oppression technology and willingness to use it. Or said another way, if you live in Syria or Palestine, you are especially screwed. We heard some really sad and disturbing stories; but those stories aren't mine to tell here.</p>

<p>We helped to explain the implications of the 54 gigs of Bluecoat logs that got published from inside Syria, detailing URLs and the IP addresses that fetched them. (The IP addresses were scrubbed from the published version of the logs, but the URLs, user agents, timestamps, etc still contain quite sensitive info.)<br />
<a href="http://advocacy.globalvoicesonline.org/2011/10/10/bluecoat-us-technology-surveilling-syrian-citizens-online/" rel="nofollow">http://advocacy.globalvoicesonline.org/2011/10/10/bluecoat-us-technolog…</a></p>

<p>Perhaps most interesting in the Bluecoat logs is the evidence of Bluecoat devices phoning home to get updates. So much for Bluecoat's claims that they don't provide support to Syria. If the US government chose to enforce its existing laws against American companies selling surveillance tools to Syria, it would be a great step toward making Tor users safer in Syria right now: no doubt Syria has some smart people who can configure things locally, but it's way worse when Silicon Valley engineers provide new filter rules to detect protocols like Tor for no additional charge.</p>

<p>The pervasiveness of video cameras and journalists at the meeting was surprising. I'm told the previous Arab blogger meeting was just a bunch of geeks sitting around their laptops talking about how to improve their countries and societies. Now that the Twitter Revolution is hot in the press, I guess all the Western media now want a piece of the action.</p>

<p>On the third workshop day we learned that there was a <a href="http://info-tunisie.net/high-tech/actualite/tic/21955-tunisie--iptelecom-expo-2011-les-6-et-7-octobre" rel="nofollow">surveillance corporate expo</a> happening in the same hotel as the blogger meeting. We crashed it and collected some brochures. We also found a pair of students from a nearby university who had set up a booth to single-handedly try to offset the evil of the expo. They were part of a security student group at their university that had made a magazine that talked among other things about Tor, Tunisian filtering, etc. We gave them a big pile of Tor stickers.</p>

<p>On our extra day after the workshops, we visited Moez at his Internet Agency and interviewed him for a few hours about the state of filtering in his country. He confirmed that they renewed their Smartfilter license until Sept 2012, and that they still filter "the groups that want it" (government and schools), but for technical reasons they have turned off the global filters (they broke and nobody has fixed them). We pointed out that since an external company operates their filters — including for their military — then that company not only has freedom to censor anything they want, but they also get to see every single request when deciding whether to censor it. Moez used the phrase "national sovereignty" when explaining why it isn't a great idea for Tunisia to outsource their filtering. Great point: it would be foolish to imagine that this external company isn't logging things for their own purposes, whether that's "improving their product" or something more sinister. As we keep seeing, collecting a large data set and then hoping to keep it secret never seems to work out.</p>

<p>One of the points Jake kept hammering on throughout the week was "if <em>*anything*</em> is being filtered, then you have to realize that they're surveilling <em>*everything*</em> in order to make those filtering decisions." The Syrian logs help to drive the point home but it seems like a lot of people haven't really internalized it yet. We still find people thinking of Tor solely as an "anti-filter" tool and not considering the surveillance angle.</p>

<p>After the meeting with Moez, we went to visit one of the universities.  We talked to a few dozen students who were really excited to find us there — to the point that they quickly located a video camera and interviewed us on the spot. They brought us to their security class, and informed the professor that we would be speaking for the first half hour of it. We gave an impassioned plea for them to learn more about Tor and teach other people in their country how to be safe online. I think the group of students there could be really valuable for creating local technical Tor resources. As a bonus, the traditional path for a computer science graduate of this university is to go work at Tunisia Telecom, the monopoly telco that hosts the filtering boxes &amp;mdash the more we can influence the incoming generations, the more the change will grow.</p>

---
_comments:

<a id="comment-12167"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12167" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2011</p>
    </div>
    <a href="#comment-12167">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12167" class="permalink" rel="bookmark">hi,
can you please recommend</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi,</p>
<p>can you please recommend me an advanced security and privacy guide. or a soft copy of the above event Context</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12168"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12168" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2011</p>
    </div>
    <a href="#comment-12168">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12168" class="permalink" rel="bookmark">Wow, that&#039;s awesome.
Are any</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wow, that's awesome.<br />
Are any records available online? I'm especially interested in the "a walk-through of how *we* keep our laptops secure" part.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12171"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12171" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2011</p>
    </div>
    <a href="#comment-12171">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12171" class="permalink" rel="bookmark">علامة للموت</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>علامة للموت</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12172"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12172" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2011</p>
    </div>
    <a href="#comment-12172">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12172" class="permalink" rel="bookmark">why tor is too slow?
any</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>why tor is too slow?<br />
any plan to increase speed?</p>
<p>from iran</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12176"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12176" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12172" class="permalink" rel="bookmark">why tor is too slow?
any</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12176">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12176" class="permalink" rel="bookmark">increasing speed, through</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>increasing speed, through donations, do the two organisations mentioned here:<br />
<a href="https://blog.torproject.org/blog/support-tor-network-donate-exit-node-providers" rel="nofollow">https://blog.torproject.org/blog/support-tor-network-donate-exit-node-p…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12203"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12203" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12176" class="permalink" rel="bookmark">increasing speed, through</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12203">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12203" class="permalink" rel="bookmark">URL keeps getting mangled.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>URL keeps getting mangled. Here it is in two parts:</p>
<p><a href="https://blog.torproject.org/blog/" rel="nofollow">https://blog.torproject.org/blog/</a></p>
<p>support-tor-network-donate-exit-node-providers</p>
<p>put them together</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12179"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12179" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 18, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12172" class="permalink" rel="bookmark">why tor is too slow?
any</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12179">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12179" class="permalink" rel="bookmark">Tor speed is very ok,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor speed is very ok, nowadays. Of course, if you dont try to put it no hight traffic f.ex. torrents, or streaming media, witch is not recomennded. There is a lot of speed improvements, comparied to earlier builds (2007-2009). It is important to understand, than Tor ir multiplexer of TCP technices, it is no vay to get same up/down speed and latency as in traditional TCP connection client-server over ISP backboune. You may try to choose better bridge entry nodes, or run Tor over SSH/VPN tunnel. Better SSH, cos it is also as administrative coverage, as I understand most of VPN techniques are recognized and blocked in Your country. Good luck bro's.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12178"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12178" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 18, 2011</p>
    </div>
    <a href="#comment-12178">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12178" class="permalink" rel="bookmark">&gt;&quot;It ended with a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;"It ended with a walk-through of how *we* keep our laptops secure,<br />
&gt;so people could see how far down the rabbit hole they can go."</p>
<p>Could you describe for everybody how you were securing your laptops,<br />
either here or maybe on a seperate page on torproject.org?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12196"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12196" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12178" class="permalink" rel="bookmark">&gt;&quot;It ended with a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12196">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12196" class="permalink" rel="bookmark">I was going to request the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I was going to request the same thing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12346"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12346" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12196" class="permalink" rel="bookmark">I was going to request the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12346">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12346" class="permalink" rel="bookmark">I agree. I&#039;d like to compare</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree. I'd like to compare notes. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-12194"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12194" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2011</p>
    </div>
    <a href="#comment-12194">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12194" class="permalink" rel="bookmark">So is Skype safe? I&#039;m really</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So is Skype safe? I'm really curious to hear some details about whether skype is safe or not since Google Searches on the topic doesn't really reveal any in-depth information.</p>
<p>According to Skype themselves, they do not "store" skype-to-skype or skype-to-phone conversations on their servers in any way (the conversations are supposedly 256 bit encrypted too). </p>
<p>So is skype safe? Why or why not? who could access the conversations? Would it be easy?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12222"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12222" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 21, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12194" class="permalink" rel="bookmark">So is Skype safe? I&#039;m really</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12222">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12222" class="permalink" rel="bookmark">Skype, is definetly not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Skype, is definetly not safe. Yes they don't store voice and messaging traffic in threir PostgreSQL cloud due to this not really get sense, BUT as there is so called police mode, where law enforcement on request can trigger survilence on particular skype nickname or node-&gt;node traffic. Many says Skype traffic ir decentralised(user traffic is, as far as police mode is off), that is bullshit, skype client by itself is contacting their central servers, and when police mode is active, these connections are made using other nodes, not directly, as for example on login, or profile management. Did someone says torrify Skype?? Guys, even if you do transparet proxying, skype protocol by itself will identify you(ok not, if you are in VM whitout any other skype account before on that VM). On windows, there is know that someone called "master"(i suppose this is subject of police mode) can call reverse shell cmd.exe, and most interesting thing that is in skype, something like only-most-important-last-used method. It is quite interesting. Maybe someone of you remember that CIH Cernobyl computer virus, infected PE files only on close. On skype, there is very similar solution. Skype binary can read(reads) interesting for them files, and memory regions only when user works with theese objects. It is nice itrick, so no suspicious HDD activity, no suspicious hooks, no suspicious memory management api. And it is quite clever, because as user works with it, then it is important. And of course all public know stuff, how polymorph skype executable is, then issues when skype opened passwd file on Linux, or tried to dump first bytes of Bios with 16 bit executable on Windows. There are difficulities to control Skype executable in windows, because don't wait to see some suspicious activity if you have more that 3-4 kernel hooks(mostt of AV/F W have). On Linux you can try to strace Skype executable, and you will see, how different will results be depend on situation.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12239"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12239" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 22, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12222" class="permalink" rel="bookmark">Skype, is definetly not</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12239">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12239" class="permalink" rel="bookmark">can a tor programmer please</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>can a tor programmer please comment on what this guy just said? Is all of it true? Is skype really completely unsafe, and is it safer to use landlines or cell phones than skype?</p>
<p>Please, Tor guys comment on this! I only trust tor developers since 2 years ago!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12252"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12252" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">October 23, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12239" class="permalink" rel="bookmark">can a tor programmer please</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12252">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12252" class="permalink" rel="bookmark">While I can&#039;t speak to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>While I can't speak to everything the commenter said, Skype is well documented for being extremely sketchy. See: <a href="https://en.wikipedia.org/wiki/Skype_security#Flaws_and_potential_flaws" rel="nofollow">https://en.wikipedia.org/wiki/Skype_security#Flaws_and_potential_flaws</a></p>
<p>The safest way to deal with it is to run it in Tails in a VM: <a href="http://tails.boum.org" rel="nofollow">http://tails.boum.org</a></p>
<p>If you run Skype on Tails outside of a VM, it quite possibly could read your Ethernet MAC address as a unique identifier to track you. Tails is working on MAC spoofing support, but it is not done yet: <a href="http://tails.boum.org/todo/macchanger/" rel="nofollow">http://tails.boum.org/todo/macchanger/</a></p>
<p>There are probably fun subtleties too. You may have difficulty getting a circuit with low enough latency to be comfortable for use with Skype.</p>
<p>As to your question about landlines or cell phones being better? Depends on your situation. Most likely, it is probably safe to assume the PSTN is even worse than Skype, especially if you jail Skype in a VM of some sort.</p>
<p>Your safest option is to communicate using Tor with a chat client that supports OTR encrypted chat (such as Pidgin or Adium). We are working to bring the Pidgin-based IM bundles back to support this use case safely and easily.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12278"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12278" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to mikeperry</p>
    <a href="#comment-12278">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12278" class="permalink" rel="bookmark">Yes Pidgin for multi</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes Pidgin for multi protocol IM, Ekiga for VoIP, and <a href="https://imo.im" rel="nofollow">https://imo.im</a> for Skype releated conversations.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-12200"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12200" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2011</p>
    </div>
    <a href="#comment-12200">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12200" class="permalink" rel="bookmark">It seems to me (I very hope</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It seems to me (I very hope that it is only my unfounded  conjecture) that the Tor team now works only on problems Arab, Iran, Chineez bloggers but not for Tor users from the USA, Europe and Russia which needed in more anonymous decisions that the above-mentioned people.<br />
That my suspection based on facts that the Tor team works about only so-called user-friendly decisions such as TBB.<br />
But it is well-known that for strong anonymity it needs to use UNIX-like operating systems with transparently-torified users. And that starting the Tor under the same user started browser it is not a good decision because exploits on searching sites can conpromise that Tor and deanonymize the user of Tor.<br />
And it is so strange that Tor developers insist on such  disadvantageous decision.<br />
It seems to me that it is intrigues of secret services of countries of the Group of Eight which interested in decreasing the level of Internet anonymity in their countries and increasing it in such countries as Arab, Iran, China and etc.<br />
I very hope that I am wrong.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12213"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12213" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 20, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12200" class="permalink" rel="bookmark">It seems to me (I very hope</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12213">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12213" class="permalink" rel="bookmark">For strong anonymity, I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For strong anonymity, I recommend (and we fund) Tails:<br />
<a href="http://tails.boum.org/" rel="nofollow">http://tails.boum.org/</a></p>
<p>As for whether we're focusing on one country or another, I think part of the challenge is that we're busy *doing* stuff and we're not as good as we could be at keeping everybody informed about all the stuff. I thought the Tunisia training might be particularly interesting for people to hear about, but I've done many more trainings in the United States this month than I have done in Tunisia. I've also not mentioned the usual conversations with policy-makers and law-makers in the US to try to teach them about the Internet and Tor.</p>
<p>As for usability on Unix, you're right that we've been focusing more on usability for Windows and OS X users than for Linux users. We'd love to have some help being able to focus on the more advanced users. They're a challenge in large part because of the diversity of installs and configurations. We see this tension internally since most of the developers use Linux and some are pushing back against TBB as the recommended default. You don't need to resort to conspiracy theories though. The fact is that the stock Firefox in your distro is not safe enough to use. What to do so everybody is happy? I don't know an easy answer.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12622"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12622" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 11, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-12622">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12622" class="permalink" rel="bookmark">Tails is a good decision for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tails is a good decision for some purposes but not good for some other.<br />
So, if I need anonymity and privacy in my everyday work it seems to me that it is not good way to use liveCD or liveUSB for it.<br />
For general purposes I use a full-functional linux system wich include Tor and etc. with hdd-encryption.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-12220"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12220" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 21, 2011</p>
    </div>
    <a href="#comment-12220">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12220" class="permalink" rel="bookmark">to the guy two posts above:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>to the guy two posts above: Arab is not a country.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12267"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12267" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2011</p>
    </div>
    <a href="#comment-12267">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12267" class="permalink" rel="bookmark">i&#039;m from Syria And i&#039;m</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i'm from Syria And i'm Really fried &gt; but any way thx to tor Team how make us a little fill safely !!!  \ Syrian FREE</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12453"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12453" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 31, 2011</p>
    </div>
    <a href="#comment-12453">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12453" class="permalink" rel="bookmark">Wery nice thanks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wery nice thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12525"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12525" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2011</p>
    </div>
    <a href="#comment-12525">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12525" class="permalink" rel="bookmark">For China:
I think it is not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For China:<br />
I think it is not a technical issue. It is hard to against a country like China, as what you are facing is an evil power with almost unlimited resource.</p>
<p>But on the other hand if the world wants to help people in China via TOR, it is a diffrent story. Syaing that I am suggesting TOR to increase its marketing activities to get more public exposure. Here are some suggestions:<br />
1. Raise more funding, provide secure ways for people in China to contribute.<br />
2. With funding, try to advitise on Social networks to get more helps<br />
3. Work with government, orginizations etc to put bridgtes on their public servers, which Chinese government can not block.<br />
4. Mobile version to increase the bridges<br />
5. TOR application for Tomato or DD-WRT etc<br />
6. Create "Honey pots" for bridges attackers</p>
<p>Again, I think non-technical issue will have to be settled in a non-technical way.</p>
<p>Thanks for your hard work,<br />
Sincerely,<br />
Internet Refugees from China</p>
</div>
  </div>
</article>
<!-- Comment END -->
