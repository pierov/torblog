title: Tor Weekly News — January 14th, 2015
---
pub_date: 2015-01-14
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the second issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>What to do if meek gets blocked</h1>

<p>Regular readers of Tor Weekly News will be familiar with <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">meek</a>, the pluggable transport developed by David Fifield. Where most existing transports work by connecting clients to “bridge” relays that are difficult for the adversary to discover (or identify as relays), meek makes all of a client’s Tor traffic appear as though it is destined for a domain that is “too big to block” — in other words, web platforms so popular that a censor cannot prevent access to them without disrupting lots of unrelated Internet activity in its sphere of control — when in fact the traffic is sent to meek servers running on those platforms, which in turn relay it into the regular Tor network. Google, Amazon, and Microsoft are some of the services whose domain names currently work as disguises for meek.</p>

<p>Unfortunately, that doesn’t mean meek is unblockable. As David <a href="https://lists.torproject.org/pipermail/tor-talk/2015-January/036410.html" rel="nofollow">wrote</a> to the tor-talk mailing list, “that’s the wrong way to think about the problem”. “It is designed to be difficult and expensive to block […] but suppose a censor makes that call, and blocks Google/Amazon/whatever. What then?”</p>

<p>Two easy solutions are selecting a different backend (meek-amazon instead of meek-google, for example) or using a different DNS server: “The most common way to block a domain name is by DNS poisoning; i.e., the IP address behind the name is accessible, but the local DNS server gives you a false address. Try a public DNS server such as 8.8.8.8. But if that works, be aware that it’s probably only a temporary fix, as censors have historically figured out the alternate-DNS trick pretty fast.”</p>

<p>“What you really want to do”, David suggested, “if the easy things don’t work, is choose a different front domain.” Please see David’s message for a fuller explanation of the difference between the backend and the “front domain”, and a guide to configuring new domains — as well as one to setting up your own meek web app, if all else fails.</p>

<h1>Miscellaneous news</h1>

<p>sycamoreone <a href="https://lists.torproject.org/pipermail/tor-talk/2015-January/036425.html" rel="nofollow">announced</a> orc, a Go library that implements parts of Tor’s control protocol. “I do have some ideas for a higher-level interface, but no fixed plan yet. The next step will probably be to add net/http-like handlerFuncs to handle (asynchronous) replies from the onion router.”</p>

<p>taxakis <a href="https://lists.torproject.org/pipermail/tor-talk/2015-January/036420.html" rel="nofollow">linked</a> to “<a href="http://eprint.iacr.org/2015/008" rel="nofollow">Post-Quantum Secure Onion Routing</a>” by Satrajit Ghosh and Aniket Kate, a new paper proposing a successor to the currently-used ntor handshake protocol that would be “resilient against quantum attacks, but also at the same time allow OR nodes to use the current DH public keys, and consequently require no modification to the current Tor public key infrastructure.” Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-talk/2015-January/036429.html" rel="nofollow">wondered</a> whether “anybody around here has the cryptographic background to comment on the PQ part of their scheme?”, and compared it to Yawning Angel’s <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007977.html" rel="nofollow">experimental “basket” protocol</a>.</p>

<p>Nick also sent out a draft of <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008115.html" rel="nofollow">proposal 240</a>, describing “a simple way for directory authorities to perform signing key revocation”.</p>

<p>Daniel Forster <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008099.html" rel="nofollow">asked</a> for advice on proposed research into splitting traffic over multiple entry guards in combination with traffic padding: “Is the approach heading in a not so great direction w.r.t. the Tor Project’s ‘only one entry node’ decision?”</p>

<p>Karsten Loesing submitted his <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000744.html" rel="nofollow">status report for December</a>, and George Kadianakis sent out the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000745.html" rel="nofollow">report for SponsorR</a>.</p>

<p>“After CCC I have a list of people that I have given raspberry pi’s with ooniprobe, and I would like to start coordinating with them via a mailing list”, wrote <a href="https://bugs.torproject.org/14140" rel="nofollow">Arturo Filastò</a>, and the result is the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/ooni-operators" rel="nofollow">ooni-operators mailing list</a>. If you regularly run ooniprobe, or want to start, be sure to sign up!</p>

<p>Aleksejs Popovs shared with the ooni-dev mailing list <a href="https://lists.torproject.org/pipermail/ooni-dev/2015-January/000220.html" rel="nofollow">the results</a> of an OONI investigation into Latvian internet censorship, conducted using ooniprobe.</p>

<p>Dan O’Huiginn started a <a href="https://lists.torproject.org/pipermail/ooni-dev/2015-January/000208.html" rel="nofollow">conversation</a> about how to ensure users are informed of the possible consequences of running OONI tests.</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-January/000828.html" rel="nofollow">John Knoll</a> and <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-January/000835.html" rel="nofollow">Monsieur Tino</a> for running mirrors of the Tor Project’s website and software archive!</p>

<p>“How do we prevent a website mirror admin from tampering with the served files?”, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-January/000844.html" rel="nofollow">wondered</a> Frédéric Cornu. Christian Krbusek <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-January/000845.html" rel="nofollow">clarified</a> that “in fact, you can’t prevent that, but you are also mirroring the signature files. So anybody downloading from any mirror — even the original host — should verify the downloads”. Andrew Lewman <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-January/000848.html" rel="nofollow">added</a> that “the binaries are signed by well-known keys of tor packagers and developers. The mirror update script randomly selects a binary and verifies it each time it runs. If the binaries don't match, the mirror is removed from the public list.”</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

