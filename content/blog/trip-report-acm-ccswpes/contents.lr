title: Trip report, ACM CCS/WPES
---
pub_date: 2012-12-17
---
author: arma
---
tags:

research
internet censorship
trip report
conference
---
categories:

circumvention
community
reports
research
---
_html_body:

<p>In October I attended <a href="http://hatswitch.org/wpes2012/" rel="nofollow">WPES</a> and the first day of <a href="http://www.sigsac.org/ccs/CCS2012/" rel="nofollow">CCS</a>. There are a bunch of new Tor-related research papers:</p>

<ul>
<li><a href="http://freehaven.net/anonbib/#wpes12-cogs" rel="nofollow">"Changing of the Guards: A Framework for Understanding and Improving Entry Guard Selection in Tor"</a>.  Basically Tariq has written a suite of tools for analyzing how likely an adversary is to be able to see a Tor user's circuits given various values for guard selection and guard rotation. Early results include "three guards is probably the wrong number" and "we probably shouldn't rotate guards so often". Later results I hope will include "here's an algorithm for assigning the Guard flag to relays that makes users safer".</li>
<li><a href="http://freehaven.net/anonbib/#wpes12-torchestra" rel="nofollow">"Torchestra: Reducing interactive traffic delays over Tor"</a>. This paper suggests having two parallel TLS connections between each pair of relays — one for the loud circuits and one for the quiet ones. I've had a series of debates with Rob Jansen over whether this should really help, or if it's just shifting the round-robining to a deeper level (e.g. kernel-level buffers). My current opinion is that it should really help, not because it writes important bytes out faster, but because the other side can *read* important bytes faster — in the current state a relay can't predict which incoming bytes are going to be high-priority, but when high-priority bytes come on a separate TCP connection, it's easy to tell.</li>
<li><a href="http://freehaven.net/anonbib/#ccs2012-classification" rel="nofollow">"Enhancing Tor's Performance using Real-time Traffic Classification"</a>. I haven't read it in detail yet, but it looks at using machine learning to classify circuits depending on what sort of traffic they're carrying. This direction is worthwhile, but it skips over the question that Rob and I are currently wrestling with, which is "ok, so you've decided to give lower priority to a circuit. What should you actually do to make that circuit put less load on the network?" See also Rob's Usenix Security paper: <a href="http://freehaven.net/anonbib/#throttling-sec12" rel="nofollow">http://freehaven.net/anonbib/#throttling-sec12</a></li>
<li><a href="http://freehaven.net/anonbib/#ccs2012-skypemorph" rel="nofollow">"SkypeMorph: Protocol Obfuscation for Tor Bridges"</a>. The idea is to use the actual Skype program to make a (TCP) video call between user and bridge, and then drop the call and start up your own UDP traffic flows that mimic Skype's UDP flows in terms of size and timing. I'm not clear that trying to look like Skype is a game we can win (especially with DPI-level adversaries already playing the arms race to detect and block 'real' Skype, and with the wasted bandwidth that comes with pretending to be a Skype video call), but I think we can get a lot of mileage out of a Tor pluggable transport that carries Tor traffic over a UDP flow — especially with recent rumors of a Syrian ISP throttling all TCP flows.</li>
<li><a href="http://freehaven.net/anonbib/#ccs2012-stegotorus" rel="nofollow">"StegoTorus: A Camouflage Proxy for the Tor Anonymity System"</a>. Stegotorus is an <a href="https://www.torproject.org/projects/obfsproxy" rel="nofollow">obfsproxy</a> fork with two goals: first, chop up Tor traffic flows so you can't recognize Tor traffic just by looking for more 586-byte TCP packets than usual; and second, transport those chopped-up flows over a variety of steg modules, to hide the traffic in protocols that the adversary is unwilling to block (as opposed to obfsproxy's goal, which is to make a flow that's hard enough to DPI for that the adversary has to choose between blocking all unrecognized protocols or letting the flows through). Unfortunately, there aren't any great steg modules yet; rather, it aims to be a framework where if you *did* have a great steg module, you could just pop it in.</li>
<li><a href="http://freehaven.net/anonbib/#ccs2012-censorspoofer" rel="nofollow">"CensorSpoofer: Asymmetric Communication using IP Spoofing for Censorship-Resistant Web Browsing"</a>. It's designed for censored users who mostly consume bytes rather than produce them. This design also uses a UDP stream to deliver the bytes, but they spoof the stream as coming from a plausible-looking voip client rather than from the real source. Then they need some low-latency low-bandwidth way (e.g. instant messaging) to get the upstream packets to the server.</li>
<li>There's also <a href="http://freehaven.net/anonbib/#ccs2012-fingerprinting" rel="nofollow">"Touching from a Distance: Website Fingerprinting Attacks and Defenses"</a>. But I confess I haven't looked at it yet. Website fingerprinting remains a huge and open issue that needs more attention.</li>
</ul>

<p>This recent variety of pluggable-transport designs and research papers is fantastic. It also makes me realize that somebody should put some effort into identifying the various components that a Tor transport system needs, and which papers provide which components. For example, it sounds like SkypeMorph and CensorSpoofer could share the same details for the link encryption and flow control for their UDP stream, rather than inventing two separately. Similarly, the "small upstream channel" requirement from CensorSpoofer reminds me of the similar goal that David's <a href="http://freehaven.net/anonbib/#flashproxy-pets12" rel="nofollow">Flashproxy</a> design faces. I see that Tariq and Ian have <a href="http://cacr.uwaterloo.ca/techreports/2012/cacr2012-33.pdf" rel="nofollow">a new tech report</a> out that gives that a start.</p>

---
_comments:

<a id="comment-18205"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18205" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2012</p>
    </div>
    <a href="#comment-18205">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18205" class="permalink" rel="bookmark">I like the Changing of the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I like the Changing of the Guards paper, very good work. Is there a way to increase the rotation period of entry guards, say from 30-60 days to 120-180 days? I only see UseEntryGuards and NumEntryGuards as torrc options.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18211"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18211" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 17, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18205" class="permalink" rel="bookmark">I like the Changing of the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18211">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18211" class="permalink" rel="bookmark">Alas, no. Right now the best</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Alas, no. Right now the best way is to go change the source code:<br />
  /* Choose expiry time smudged over the past month. The goal here<br />
   * is to a) spread out when Tor clients rotate their guards, so they<br />
   * don't all select them on the same day, and b) avoid leaving a<br />
   * precise timestamp in the state file about when we first picked<br />
   * this guard. For details, see the Jan 2010 or-dev thread. */<br />
  entry-&gt;chosen_on_date = time(NULL) - crypto_rand_int(3600*24*30);</p>
<p>That said, you probably don't want to change it on your own, since then you'll be "that guy who changed his guard rotation period".</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-18256"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18256" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2012</p>
    </div>
    <a href="#comment-18256">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18256" class="permalink" rel="bookmark">&quot; ...results include &quot;three</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>" ...results include "three guards is probably the wrong number" and "we probably shouldn't rotate guards so often". "</p>
<p>Could you say that to the TAILS-developers.<br />
Implement a EntryGuard-solution like the Bridge-mode in bootmenu.<br />
Make this with 'persistence' is a not so good solution.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18262"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18262" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2012</p>
    </div>
    <a href="#comment-18262">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18262" class="permalink" rel="bookmark">Wow, &quot;Touching from a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wow, "Touching from a Distance: Website Fingerprinting Attacks and Defenses" rocked my Tor boat. It's amazing how much information the authors of this research paper were able to glean, simply by observing the patterns of upstream/downstream transmissions, despite those transmissions being encrypted. An amazing analysis using the DLSVM pattern matching vector algorithm.</p>
<p>I think the BUFLO protocol sounds fascinating. I understand it will add on average, a little over 40% overhead to the Tor network. Maybe the Tor network can get that 40% back by "Throttling Tor Bandwidth Parasites"? I haven't read the bandwidth parasite paper yet, but I'm looking forward to it. Pay proxies and VPN's are being sold for under $50 a year. Show no mercy for "bandwidth parasites" who improperly use the Tor network for high bandwidth activities!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18332"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18332" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2012</p>
    </div>
    <a href="#comment-18332">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18332" class="permalink" rel="bookmark">&quot;Implement a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Implement a EntryGuard-solution like the Bridge-mode in bootmenu.<br />
Make this with 'persistence' is a not so good solution."</p>
<p>Ehhhm,especially when someone using TAILS on DVD...........<br />
Noone do that... apparently</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18376"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18376" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 26, 2012</p>
    </div>
    <a href="#comment-18376">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18376" class="permalink" rel="bookmark">&quot;Could you say that to the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Could you say that to the TAILS-developers.<br />
Implement a EntryGuard-solution like the Bridge-mode in bootmenu.<br />
Make this with 'persistence' is a not so good solution."</p>
<p>little bit strange that TAILS will be a secure Tor solution and has no implemented option to set manually static EntryGuard.<br />
Entry Guards are elementary for Tor network design.<br />
Therefore the question: WHY its not implemented?</p>
</div>
  </div>
</article>
<!-- Comment END -->
