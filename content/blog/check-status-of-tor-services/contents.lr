title: Check the status of Tor services with status.torproject.org
---
pub_date: 2021-05-05
---
author: anarcat
---
tags:

news
sysadmin
---
summary:

The Tor Project now has a status page which shows the state of our major services.

You can check status.torproject for news about major outages in Tor services, including v3 and v2 onion services, directory authorities, our website (torproject.org), and the check.torproject.org tool. The status page also displays outages related to Tor internal services, like our GitLab instance.

This post documents why we launched status.torproject.org, how the service was built, and how it works.
---
_html_body:

<p>The Tor Project now has a <a href="https://status.torproject.org/">status page</a> which shows the state of our major services.</p>
<p><strong>You can check <a href="https://status.torproject.org">status.torproject</a> for news about major outages in Tor services</strong>, including v3 and v2 onion services, directory authorities, our website (<a href="https://torproject.org">torproject.org</a>), and the <a href="https://check.torproject.org/">check.torproject.org</a> tool. The status page also displays outages related to Tor internal services, like our GitLab instance.</p>
<p>This post documents why we launched <a href="https://status.torproject.org">status.torproject.org</a>, how the service was built, and how it works.</p>
<h1 id="why-a-status-page">Why a status page</h1>
<p>The first step in setting up a service page was to realize we needed one in the first place. I <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021#survey-results">surveyed internal users</a> at the end of 2020 to see what could be improved, and one of the suggestions that came up was to "document downtimes of one hour or longer" and generally improve communications around monitoring. The latter is still on the <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021">sysadmin roadmap</a>, but a status page seemed like a good solution for the former.</p>
<p>We already have two monitoring tools in the sysadmin team: <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/nagios">Icinga</a> (a fork of Nagios) and <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/prometheus/">Prometheus</a>, with <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/grafana">Grafana</a> dashboards. But those are hard to understand for users. Worse, they also tend to generate false positives, and don't clearly show users which issues are critical.</p>
<p>In the end, a manually curated dashboard provides important usability benefits over an automated system, and <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/status#example-sites">all major organisations have one</a>.</p>
<h1 id="picking-the-right-tool">Picking the right tool</h1>
<p>It wasn't my first foray in status page design. In another life, I had setup a status page using a tool called <a href="https://cachethq.io/">Cachet</a>. That was already a great improvement over the previous solutions, which were to use first a wiki and then a blog to post updates. But Cachet is a complex <a href="https://laravel.com/">Laravel</a> app, which also requires a web browser to update. It generally requires more maintenance than what we'd like, needing silly things like a SQL database and PHP web server.</p>
<p>So when I found <a href="https://github.com/cstate/cstate">cstate</a>, I was pretty excited. It's basically a theme for the <a href="https://gohugo.io/">Hugo</a> static site generator, which means that it's a set of HTML, CSS, and a sprinkle of Javascript. And being based on Hugo means that the site is generated from a set of <a href="https://en.wikipedia.org/wiki/Markdown">Markdown</a> files and the result is just plain HTML that can be hosted on any web server on the planet.</p>
<h1 id="deployment">Deployment</h1>
<p>At first, I wanted to deploy the site through <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/ci">GitLab CI</a>, but at that time we didn't have GitLab pages set up. Even though we do <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/gitlab#publishing-gitlab-pages">have GitLab pages set up now</a>, it's not (yet) integrated with our mirroring infrastructure. So, for now, the source is hosted and built in our legacy <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/git">git</a> and <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/jenkins">Jenkins</a> services.</p>
<p>It is nice to have the content hosted in a git repository: sysadmins can just edit Markdown in the git repository and push to deploy changes, no web browser required. And it's trivial to setup a local environment to preview changes:</p>
<pre>
hugo serve --baseUrl=http://localhost/
firefox https://localhost:1313/
</pre><p>Only the sysadmin team and gitolite administrators have access to the repository, at this stage, but that could be improved if necessary. Merge requests can also be issued on the <a href="https://gitlab.torproject.org/tpo/tpa/status-site/">GitLab repository</a> and then pushed by authorized personnel later on, naturally.</p>
<h1 id="availability">Availability</h1>
<p>One of the concerns I have is that the site is hosted inside our normal mirror infrastructure. Naturally, if an outage occurs there, the site goes down. But I figured it's a bridge we'll cross when we get there. Because it's so easy to build the site from scratch, it's actually trivial to host a copy of the site on <em>any</em> GitLab server, thanks to the <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #339933;">.</span>gitlab<span style="color: #339933;">-</span>ci<span style="color: #339933;">.</span>yml</code></span> file shipped (but not currently used) in the repository. If push comes to shove, we can just publish the site elsewhere and point DNS there.</p>
<p>And, of course, if DNS fails us, then we're in trouble, but that's the situation anyway: we can always register a new domain name for the status page when we need to. It doesn't seem like a priority at the moment.</p>
<p>Comments and feedback are welcome!</p>

---
_comments:

<a id="comment-291739"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291739" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 06, 2021</p>
    </div>
    <a href="#comment-291739">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291739" class="permalink" rel="bookmark">Clicking on torproject.org,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Clicking on torproject.org, or Tor Check link on this status page is 404 Page not found.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291740"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291740" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  anarcat
  </article>
    <div class="comment-header">
      <p class="comment__submitted">anarcat said:</p>
      <p class="date-time">May 06, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291739" class="permalink" rel="bookmark">Clicking on torproject.org,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291740">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291740" class="permalink" rel="bookmark">That&#039;s right: components…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's right: components which don't have documented outages have this bizarre behavior that they give a 404 instead of a more helpful message like, say, "there were no reported incidents on this component".</p>
<p>It seems like it's actually been <a href="https://github.com/cstate/cstate/issues/51" rel="nofollow">fixed upstream somewhat</a> but for some reason it still doesn't work on our end. The "affected/404" page doesn't get triggered by the ErrorDocument but anyways, it doesn't get built in the current configuration. I filed <a href="https://github.com/cstate/cstate/issues/183" rel="nofollow">upstream issue 183</a> to discuss that over there.</p>
<p>Thanks for the report!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291745"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291745" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 06, 2021</p>
    </div>
    <a href="#comment-291745">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291745" class="permalink" rel="bookmark">The onion v2 description on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The onion v2 description on the status page could perhaps include a warning about the deprecation coming later this year. Mainly so it does not result in a lot of confusion when it goes down permanently following <a href="https://blog.torproject.org/v2-deprecation-timeline" rel="nofollow">https://blog.torproject.org/v2-deprecation-timeline</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291748"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291748" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  anarcat
  </article>
    <div class="comment-header">
      <p class="comment__submitted">anarcat said:</p>
      <p class="date-time">May 06, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291745" class="permalink" rel="bookmark">The onion v2 description on…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291748">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291748" class="permalink" rel="bookmark">that is a great idea! here&#039;s…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>that is a great idea! here's the <a href="https://gitlab.torproject.org/tpo/tpa/status-site/-/merge_requests/4" rel="nofollow">merge request where we're handling it</a>. thanks for the suggestion!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291785"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291785" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 11, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to anarcat</p>
    <a href="#comment-291785">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291785" class="permalink" rel="bookmark">Not the same person. But…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not the same person. But have you also thought about implementing a yellow then red depreciation warning on the onion "lock logo" itself similar to HTTP security warnings? This would be the best way in my opinion to solve the fact that some people are just never going to find out themselves and suddenly lose their v2 address without redirecting users to their new v3 onion first.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291803"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291803" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 14, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291785" class="permalink" rel="bookmark">Not the same person. But…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291803">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291803" class="permalink" rel="bookmark">Using colors to indicate…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Using colors to indicate status brings problems with localization and vision accessibility. They've been proposed many times. For example:</p>
<p><a href="https://blog.torproject.org/comment/224985#comment-224985" rel="nofollow">https://blog.torproject.org/comment/224985#comment-224985</a><br />
<a href="https://blog.torproject.org/comment/281827#comment-281827" rel="nofollow">https://blog.torproject.org/comment/281827#comment-281827</a><br />
<a href="https://blog.torproject.org/comment/284093#comment-284093" rel="nofollow">https://blog.torproject.org/comment/284093#comment-284093</a><br />
<a href="https://blog.torproject.org/comment/285388#comment-285388" rel="nofollow">https://blog.torproject.org/comment/285388#comment-285388</a><br />
<a href="https://blog.torproject.org/comment/291752#comment-291752" rel="nofollow">https://blog.torproject.org/comment/291752#comment-291752</a></p>
<p>A symbol is better, but either would have to come with an explanation or legend. Here are some recent tickets with more ideas to accelerate the transition to v3 onion services:</p>
<p><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40402" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/404…</a><br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40410" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/404…</a><br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40411" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/404…</a><br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40415" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/404…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291790"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291790" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Not the OP (not verified)</span> said:</p>
      <p class="date-time">May 12, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to anarcat</p>
    <a href="#comment-291790">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291790" class="permalink" rel="bookmark">Thanks for promptly…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for promptly implementing a good suggestion from a user!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-291761"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291761" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 07, 2021</p>
    </div>
    <a href="#comment-291761">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291761" class="permalink" rel="bookmark">I surveyed internal users at…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>I <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021#survey-results" rel="nofollow">surveyed internal users</a> at the end of 2020 to see what could be improved</p></blockquote>
<p>"I outline the blog first because it's one of the most frequently used service, yet it's one of the "saddest", so it should probably be made a priority in 2021."</p>
<p>Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291767"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291767" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 07, 2021</p>
    </div>
    <a href="#comment-291767">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291767" class="permalink" rel="bookmark">Very useful, but also a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very useful, but also a sobering reminder of just how centralized the tor network is :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291775"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291775" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 08, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291767" class="permalink" rel="bookmark">Very useful, but also a…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291775">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291775" class="permalink" rel="bookmark">&gt; how centralized the tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; how centralized the tor network is</p>
<p>Not as centralized as your comment might sound. I suggest you read:</p>
<ul>
<li><a href="https://community.torproject.org/onion-services/overview/" rel="nofollow">How do onion services work?</a></li>
<li>Talk about onions: <a href="https://community.torproject.org/onion-services/talk/#network-sustainability" rel="nofollow">Network sustainability</a>, and <a href="https://community.torproject.org/onion-services/talk/#decentralization" rel="nofollow">Decentralization</a></li>
<li>Metrics glossary: <a href="https://metrics.torproject.org/glossary.html#directory-authority" rel="nofollow">directory authority</a></li>
<li><a href="https://blog.torproject.org/how-bandwidth-scanners-monitor-tor-network" rel="nofollow">How Bandwidth Scanners Monitor The Tor Network</a></li>
<li><a href="https://blog.torproject.org/lifecycle-new-relay" rel="nofollow">The life cycle of a new relay</a></li>
<li>Other Tor Blog posts that contain the phrase, <a href="https://blog.torproject.org/search/node?keys=%22directory%20authorities%22" rel="nofollow">"directory authorities"</a></li>
<li><a href="https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt" rel="nofollow">Tor directory protocol, version 3</a></li>
<li><a href="https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt" rel="nofollow">Tor Protocol Specification</a></li>
<li>Other <a href="https://spec.torproject.org/" rel="nofollow">specification documents</a></li>
<li><a href="https://2019.www.torproject.org/docs/faq.html.en#PrivateTorNetwork" rel="nofollow">How do I set up my own private Tor network?</a></li>
<li>Tell me about all the keys Tor uses: <a href="https://2019.www.torproject.org/docs/faq.html.en#KeyManagement" rel="nofollow">Coordination</a></li>
<li><a href="https://2019.www.torproject.org/docs/faq.html.en#PowerfulBlockers" rel="nofollow">What about powerful blocking mechanisms?</a></li>
</ul>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291786"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291786" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 11, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291775" class="permalink" rel="bookmark">&gt; how centralized the tor…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291786">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291786" class="permalink" rel="bookmark">Thank you very much for all…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you very much for all the links!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
