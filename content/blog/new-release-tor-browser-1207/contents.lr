title: New Release: Tor Browser 12.0.7
---
author: richard
---
pub_date: 2023-06-08
---
categories:
applications
releases
---
summary: Tor Browser 12.0.7 is now available from the Tor Browser download page and also from our distribution directory.
---
body:

Tor Browser 12.0.7 is now available from the Tor Browser [download page](https://www.torproject.org/download/) and also
from our [distribution directory](https://dist.torproject.org/torbrowser/12.0.7/).

This release updates Firefox to 102.12.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-19/). We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-20/) from Firefox 114.

## Build-Signing Infrastructure Updates

We are once again able to code-sign our executable Windows installer, so new installations on the Windows platform no longer need to perform a build-to-build update from an older version. We apologize for all the inconvenience this caused.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 12.0.6](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-12.0/projects/browser/Bundle-Data/Docs/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated NoScript to 11.4.22
  - Updated OpenSSL to 1.1.1u
  - [Bug tor-browser#41764](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41764): TTP-02-004 OOS: No user-activation required to download files (Low)
  - [Bug tor-browser#41794](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41794): Rebase Tor Browser and Base Browser stable to 102.12esr
- Windows + macOS + Linux
  - Updated Firefox to 102.12esr
  - [Bug tor-browser#41777](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41777): Internally shippped manual does not adapt to RTL languages (it always align to the left)
- Android
  - Updated GeckoView to 102.12esr
  - [Bug tor-browser#41805](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41805): Backport Android-specific security fixes from Firefox 114 to ESR 102.12-based Tor Browser
