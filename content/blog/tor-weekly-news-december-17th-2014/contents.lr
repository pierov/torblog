title: Tor Weekly News — December 17th, 2014
---
pub_date: 2014-12-17
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the fiftieth issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Solidarity against online harassment</h1>

<p>Following “a sustained campaign of harassment” directed at a core Tor developer over the past few months, the Tor Project published a <a href="https://blog.torproject.org/blog/solidarity-against-online-harassment" rel="nofollow">statement</a> in which it declared “support for her, for every member of our organization, and for every member of our community who experiences this harassment”: “In categorically condemning the urge to harass, we mean categorically: we will neither tolerate it in others, nor will we accept it among ourselves. We are dedicated to both protecting our employees and colleagues from violence, and trying to foster more positive and mindful behavior online ourselves… We are working within our community to devise ways to concretely support people who suffer from online harassment; this statement is part of that discussion. We hope it will contribute to the larger public conversation about online harassment and we encourage other organizations to sign on to it or write one of their own.”</p>

<p>As of this writing, there are 448 signatories to the statement, including Tor developers and community members, academics, journalists, lawyers, and many others who are lending their support to this movement in its early stages. If you want to add your name to the list, please send an email to <a href="mailto:tor-assistants@lists.torproject.org" rel="nofollow">tor-assistants@lists.torproject.org</a>.</p>

<h1>Tails 1.2.2 is out</h1>

<p>The Tails team <a href="https://tails.boum.org/news/version_1.2.2/" rel="nofollow">announced</a> a pointfix release of the amnesic live operating system. The only difference between this version and the recent 1.2.1 release is that the automatic Tails Updater now expects a different certificate authority when checking for a new Tails version. As the team explained, “On January 3rd, the SSL certificate of our website hosting provider, boum.org, will expire. The new certificate will be issued by a different certificate authority […] As a consequence, versions previous to 1.2.2 won’t be able to do the next automatic upgrade to version 1.2.3 and will receive an error message from Tails Upgrader when starting Tails after January 3rd”.</p>

<p>This, along with a <a href="https://labs.riseup.net/code/issues/8449" rel="nofollow">bug</a> that prevents automatic updates from 1.2.1 to 1.2.2, means that all Tails users will need to upgrade manually: either to version 1.2.2 before January 3rd or (if for some reason that is not possible) to version 1.2.3 following its release on January 14th. Please see the team’s post for more details and download instructions.</p>

<h1>Miscellaneous news</h1>

<p>George Kadianakis, Karsten Loesing, Aaron Johnson, and David Goulet <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007968.html" rel="nofollow">requested feedback</a> on the design and code they have developed for the <a href="https://gitweb.torproject.org/karsten/tor.git/log/?h=task-13192-5" rel="nofollow">Tor branch</a> that will enable the collection of statistics on Tor hidden services, hoping to answer the questions “Approximately how many hidden services are there?” and “Approximately how much traffic in the Tor network is going to hidden services?”: “Our plan is that in approximately a week we will ask volunteers to run the branch. Then in a month from now we will use those stats to write a blog post about the approximate size of Tor hidden services network and the approximate traffic it’s pushing.” Please join in with your comments on the relevant <a href="https://bugs.torproject.org/13192" rel="nofollow">ticket</a>!</p>

<p>Philipp Winter <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007973.html" rel="nofollow">announced</a> an early version of “zoossh”, which as the name implies is a speedy parser written in Go that will help to “detect sybils and other anomalies in the Tor network” by examining Tor’s archive of network data. While it is not quite ready for use, “I wanted folks to know that I’m working on that and I’m always happy to get feedback and patches.”</p>

<p>Yawning Angel <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007977.html" rel="nofollow">announced</a> the existence of “basket”, a “stab at designing something that significantly increases Tor’s resistance to upcoming/future attacks”, combining post-quantum cryptographic primitives with “defenses against website fingerprinting (and possibly end-to-end correlation) attacks”. You can read full details of the cryptographic and other features of “basket” in Yawning’s post, which is replete with warnings against using the software at this stage: “It’s almost at the point where brave members of the general public should be aware that it exists as a potential option in the privacy toolbox… [but] seriously, unless you are a developer or researcher, you REALLY SHOULD NOT use ‘basket’.” If you are gifted or foolhardy enough to ignore Yawning’s advice and test “basket” for yourself, please let the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-dev" rel="nofollow">tor-dev mailing list</a> know what you find.</p>

<p>Sukhbir Singh and Arlo Breault <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007981.html" rel="nofollow">requested feedback</a> on an alpha version of Tor Messenger. It is an instant messaging client currently under development that intends to send all traffic over Tor, use Off-the-Record (OTR) encryption of conversations by default, work with a wide variety of chat networks, and have an easy-to-use graphical user interface localized into multiple languages.</p>

<p>TheCthulhu announced that his mirrors of two Tor network tools are now <a href="https://lists.torproject.org/pipermail/tor-talk/2014-December/035982.html" rel="nofollow">available over Tor hidden services</a>. <a href="https://globe.thecthulhu.com" rel="nofollow">Globe</a> can be accessed via <a href="http://globe223ezvh6bps.onion" rel="nofollow">http://globe223ezvh6bps.onion</a> and <a href="https://atlas.thecthulhu.com" rel="nofollow">Atlas</a> via <a href="http://atlas777hhh7mcs7.onion" rel="nofollow">http://atlas777hhh7mcs7.onion</a>. The mirrors provided by the Cthulhu run on their own instance of Onionoo, so in the event that the primary sites hosted by Tor Project are offline, both of these new mirrors should still be available for use either through the new hidden services or through regular clearnet access.</p>

<p>The Tails team <a href="https://mailman.boum.org/pipermail/tails-dev/2014-December/007632.html" rel="nofollow">published</a> a signed list of SHA256 hashes for every version of Tails (and its predecessor, amnesia) that it had either built or verified at the time of release.</p>

<p>Vlad Tsyrklevich <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007957.html" rel="nofollow">raised</a> the issue of the discoverability risk posed to Tor bridges by the default setting of their ORPorts to 443 or 9001. Using data from Onionoo and internet-wide scans, Vlad found that “there are 4267 bridges, of which 1819 serve their ORPort on port 443 and 383 serve on port 9001. That’s 52% of tor bridges. There are 1926 pluggable-transports enabled bridges, 316 with ORPort 443 and 33 with ORPort 9001. That’s 18% of Tor bridges… I realized I was also discovering a fair amount of private bridges not included in the Onionoo data set.” Vlad recommended that operators be warned to change their ORPorts away from the default; Aaron Johnson <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007959.html" rel="nofollow">suggested</a> possible alternative solutions, and Philipp Winter <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007963.html" rel="nofollow">remarked</a> that while bridges on port 443 “would easily fall prey to Internet-wide scanning”, “they would still be useful for users behind captive portals” and other adversaries that restrict connections to a limited range of ports.</p>

<p>Alden Page <a href="https://lists.torproject.org/pipermail/tor-talk/2014-December/035989.html" rel="nofollow">announced</a> that development will soon begin on a free-software tool to counteract “stylometry” attacks, which attempt to deanonymize the author of a piece of text based on their writing style alone. “I hope you will all agree that this poses a significant threat to the preservation of the anonymity of Tor users”, wrote Alden. “In the spirit of meeting the needs of the privacy community, I am interested in hearing what potential users might have to say about the design of such a tool.” Please see Alden’s post for further discussion of stylometry attacks and the proposed countermeasures, and feel free to respond with your comments or questions.</p>

<h1>Tor help desk roundup</h1>

<p>Because Tor Browser prevents users from running it as root, Kali Linux users starting Tor Browser will see an error message saying Tor should not be run as root. </p>

<p>In Kali, all userspace software runs as root by default. To run Tor Browser in Kali Linux, create a new user account just for using Tor Browser. Unpack Tor Browser and chown -R your whole Tor Browser directory. Run Tor Browser as your created Tor Browser user account.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, TheCthulhu, Matt Pagan, Arlo Breault, and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

