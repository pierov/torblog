title: Landmark for Hidden Services: .onion names reserved by the IETF
---
pub_date: 2015-10-25
---
author: ioerror
---
tags:

onion
ietf
rfc7686
---
categories: onion services
---
_html_body:

<p>The <a href="https://www.ietf.org/" rel="nofollow">Internet Engineering Task Force</a> (IETF), the body that sets standards for the Internet, has formally recognized <a href="https://en.wikipedia.org/wiki/.onion" rel="nofollow">.onion</a> names. We think that this is a small and important landmark in the movement to build privacy into the structure of the Internet. This standardization work for .onion is joint work between <a href="//www.facebook.com/notes/alec-muffett/rfc-7686-and-all-that/10153809113970962" rel="nofollow">Facebook</a> and the <a href="https://www.torproject.org" rel="nofollow">Tor Project</a> amongst others in an effort to help secure users everywhere.</p>

<p>Over the last few years, The Tor Project has been working with other members of the Peer to Peer community led by Dr. Christian Grothoff, founder of the <a href="https://gnunet.org" rel="nofollow">GNUnet project</a> to <a href="https://datatracker.ietf.org/doc/draft-grothoff-iesg-special-use-p2p-names/" rel="nofollow">register several</a> Special-Use Domain Names. IETF name reservations are part of a <a href="https://www.rfc-editor.org/info/rfc6761" rel="nofollow">lesser known process</a> that ensures a registered Special-Use Domain Name will not become a Top Level Domain (TLD) to be sold by the <a href="https://www.icann.org" rel="nofollow">Internet Corporation For Assigned Names and Numbers</a> (ICANN). Special-Use Domain Names have special considerations documented as part of their registration. Some of these names may sound familiar, such as <a href="https://en.wikipedia.org/wiki/.local" rel="nofollow">.local</a> which is widely deployed by <a href="https://www.apple.com" rel="nofollow">Apple</a> and others for Multicast Domain Name Service (mDNS).</p>

<p>During our long journey which began in the Summer of Snowden, Alec Muffett and I were encouraged to split out <a href="https://en.wikipedia.org/wiki/.onion" rel="nofollow">.onion</a> from the list of other peer to peer names and to make a separate draft to register .onion as a Special-Use Domain Name. In this draft we listed security and privacy considerations that we believe will help to protect end users from targeted and mass-surveillance. We're happy to say that the first name reservation was just published as <a href="https://www.rfc-editor.org/info/rfc7686" rel="nofollow">RFC7686</a>.</p>

<p>Our internet standard reflects on considerations for handling .onion names on the internet as well as officially <a href="https://www.iana.org/assignments/special-use-domain-names/special-use-domain-names.xhtml" rel="nofollow">reserving .onion as a Special-Use-Domain-Name</a> with the <a href="https://www.iana.org" rel="nofollow">Internet Assigned Numbers Authority</a> (IANA). With this registration, it is should also be possible to buy Extended Validation (EV) SSL/TLS certificates for .onion services thanks to <a href="//cabforum.org/2015/02/18/ballot-144-validation-rules-dot-onion-names/" rel="nofollow">a recent decision</a> by the Certification Authority Browser Forum. We hope that in the future we'll see easy to issue certificates from the <a href="https://letsencrypt.org/" rel="nofollow">Let's Encrypt</a> project for .onion services. We also <a href="https://gnunet.org/ietf93dnsop" rel="nofollow">hope to see more Peer to Peer names</a> such as .gnu registered as Special-Use-Domain-Names by the IETF.</p>

<p>It is now easier than ever to <a href="https://www.torproject.org/docs/hidden-services.html.en" rel="nofollow">deploy</a>, <a href="https://onionshare.org/" rel="nofollow">share</a> and <a href="https://www.ricochet.im/" rel="nofollow">use</a> Tor Hidden Services.</p>

<p>We greatly enjoyed our efforts with the IETF and plan to continue <a href="https://datatracker.ietf.org/rg/hrpc/charter/" rel="nofollow">actively participate</a> with the IETF in the future. We'd also like to thank everyone who helped with this process including but not limited to Mark Nottingham, Roger Dingledine, Linus Nordberg, Seth David Schoen, Leif Ryge, Helekin Wolf, Matthias Wachs and Dr. Christian Grothoff.</p>

---
_comments:

<a id="comment-113135"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113135" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2015</p>
    </div>
    <a href="#comment-113135">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113135" class="permalink" rel="bookmark">Good to know .onion is now</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good to know .onion is now officially recognized by the IETF, great progress!<br />
But this "It is now easier than ever to deploy, share and use Tor Hidden Services." doesn't make sense to me, why would it be easier to deploy hidden services when it's recognized by the IETF?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113246"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113246" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113135" class="permalink" rel="bookmark">Good to know .onion is now</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113246">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113246" class="permalink" rel="bookmark">Maybe your interpretation</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Maybe your interpretation wasn't the one intended.</p>
<p>The statement is true regardless of this new development, thanks to continued incremental progress in onion services.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113805"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113805" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113246" class="permalink" rel="bookmark">Maybe your interpretation</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113805">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113805" class="permalink" rel="bookmark">The logic you challenge is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The logic you challenge is solid; it will read that way to many.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-113275"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113275" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113135" class="permalink" rel="bookmark">Good to know .onion is now</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113275">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113275" class="permalink" rel="bookmark">I assume this mean that you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I assume this mean that you will be able to buy proper signed SSL-certificates for .onion domians now which will be a great improvement</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113311"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113311" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113275" class="permalink" rel="bookmark">I assume this mean that you</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113311">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113311" class="permalink" rel="bookmark">Why is that important, onion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why is that important, onion services are already end-to-end encrypted, and we can even trust that a onion service is even harder to "fake" or "MITM" than a regular SSL-certificate website.<br />
I think it's very important that onion domains are reserved fr special use as Tor services, but I don't see why it is important to have SSL certificates...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113480"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113480" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 30, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113311" class="permalink" rel="bookmark">Why is that important, onion</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113480">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113480" class="permalink" rel="bookmark">Actually, Facebook et al</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually, Facebook et al wanted the https certificate primarily so that browsers would treat in-browser data the way it should for https sites (otherwise the browser has to be specially taught that "addresses ending in .onion are sort of like https sites"), and so that their server back-end would not need to have the ssme exceptions built-in (which would open it up to more bugs, maybe, too).</p>
<p>Read more about the Facebook .onion https part here:<br />
<a href="https://blog.torproject.org/blog/facebook-hidden-services-and-https-certs" rel="nofollow">https://blog.torproject.org/blog/facebook-hidden-services-and-https-cer…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-113287"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113287" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113135" class="permalink" rel="bookmark">Good to know .onion is now</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113287">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113287" class="permalink" rel="bookmark">One problem I see with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>One problem I see with this"official recognition" of the .onion domain is that there is probably a central repository of hidden services lurking somewhere just waiting to be tapped by malicious actors( once the location of the repository has been located ) so while the service is to all intents and purposes decentralized it is also( unbeknown to the casual user) earmarked for very close scrutiny, expect an increase of anwelcome activity as a consequence.( but iIdo hope I'm wrong!! )</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113479"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113479" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 30, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113287" class="permalink" rel="bookmark">One problem I see with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113479">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113479" class="permalink" rel="bookmark">I encourage you to learn</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I encourage you to learn about the onion service protocol, and especially the way that hidden service descriptors are distributed around the relays, so that there is no central repository of onion addresses.</p>
<p><a href="https://spec.torproject.org/rend-spec" rel="nofollow">https://spec.torproject.org/rend-spec</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-137229"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-137229" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-137229">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-137229" class="permalink" rel="bookmark">isn&#039;t hidden service name ==</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>isn't hidden service name == (specific kind of) ip-number? in this case dns should support say ATOR record same as AAAA (for ipv6). so you can ask ATOR for googlemicrosoftapple.nsa.gov (just as AAAA).<br />
and anyway client can select dns server (tcp over tor?) so he can redefine say apple.com as localnet address (just as they do to .local)<br />
in other words its enough to setup official tor DNS server and begin registering (public) hidden services - lets say chat.torproject.onion. thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-113218"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113218" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2015</p>
    </div>
    <a href="#comment-113218">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113218" class="permalink" rel="bookmark">This was sponsored by</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This was sponsored by Facebook..?  Uhh..</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113466"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113466" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 29, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113218" class="permalink" rel="bookmark">This was sponsored by</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113466">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113466" class="permalink" rel="bookmark">If by &quot;sponsored&quot; you mean</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If by "sponsored" you mean "there was a person who helped work on it, and he is an employee of Facebook", yes absolutely. Yay Alec.</p>
<p>If by "sponsored" you mean "Facebook gave money to Tor", alas no.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-113271"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113271" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2015</p>
    </div>
    <a href="#comment-113271">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113271" class="permalink" rel="bookmark">Do you have list of cert</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you have list of cert authorities that will provide ssl certs for .onion ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113481"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113481" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 30, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113271" class="permalink" rel="bookmark">Do you have list of cert</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113481">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113481" class="permalink" rel="bookmark">Facebook did Digicert&#039;s. It</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Facebook did Digicert's. It would be nice to see a second one happen in practice.</p>
<p>[Edit: as the commenter below points out, yes, I mean Digicert did Facebook's. Good times.]</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113494"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113494" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 30, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-113494">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113494" class="permalink" rel="bookmark">You mean Digicert did</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You mean Digicert did Facebook's, he.</p>
<p>Maybe Let's Encrypt will do it? A couple of days ago I saw a Let's Encrypt certificate in the wild for the first time, the beta is already ongoing!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113495"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113495" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 30, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113494" class="permalink" rel="bookmark">You mean Digicert did</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113495">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113495" class="permalink" rel="bookmark">Currently the CAB forum has</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Currently the CAB forum has said that only EV certs can be issued for .onion addresses. Also, currently Let's Encrypt has promised never to issue an EV cert.</p>
<p>It's just a matter of time, and patience, and people putting energy into the standard process. I'm optimistic. You could help!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113540"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113540" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 30, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-113540">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113540" class="permalink" rel="bookmark">What was the CAB&#039;s rationale</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What was the CAB's rationale for not allowing DV certs as well? Couldn't they just be validated by signing the CSR with the onion key, or something along those lines?</p>
<p>How do we encourage/pressure the CAB to allow DV?</p>
<p>Obviously having FB pushing for EV helped, but presumably FB have no interest in helping with DV (they probably use EV for everything), so we're on our own and need to find some other way to get leverage, right?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-137230"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-137230" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-137230">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-137230" class="permalink" rel="bookmark">why not tor/onion CA? i do</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>why not tor/onion CA? i do not trust commercial CAs. Of course 'not a (very) good idea' but at least for mass users quite acceptible. just add it to browser's CAs list. (i have no problems with a small private CA for a long time).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-113320"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113320" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2015</p>
    </div>
    <a href="#comment-113320">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113320" class="permalink" rel="bookmark">Contratulations Tor team for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Contratulations Tor team for .onion recognition.  Future milestones, just as this one will be much easier to accomplish.</p>
<p>Keep at it.<br />
imu.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-113349"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113349" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2015</p>
    </div>
    <a href="#comment-113349">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113349" class="permalink" rel="bookmark">Is it safe for a user to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it safe for a user to deploy a hidden service from home?  Can one do that using Tor Browser Bundle?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113465"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113465" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 29, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113349" class="permalink" rel="bookmark">Is it safe for a user to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113465">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113465" class="permalink" rel="bookmark">It should be reasonably safe</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It should be reasonably safe to run an onion service from home, yes. It really depends what your threat model is -- that is, what you're trying to protect against.</p>
<p>If I were running a Wikileaks submission server, I'd probably stick it on a computer somewhere else, so there's defense in depth.</p>
<p>But I regularly run a hidden service from my laptop, for example as part of the Ricochet program.</p>
<p>There is not currently support inside Tor Browser for setting up a hidden service. There have been a variety of tools over the years that aim to help you set one up, but none of them have really emerged as winners. The real trouble in each case is setting up the webserver in a way that doesn't leak info, since webservers are so complex.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-137233"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-137233" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-137233">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-137233" class="permalink" rel="bookmark">what do you think about</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>what do you think about lighttpd? it can be compiled for win32 too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-137232"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-137232" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113349" class="permalink" rel="bookmark">Is it safe for a user to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-137232">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-137232" class="permalink" rel="bookmark">it is just TWO lines in tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it is just TWO lines in tor config file!!! should not be a problem for any educated creature.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-113579"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113579" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 30, 2015</p>
    </div>
    <a href="#comment-113579">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113579" class="permalink" rel="bookmark">Congrats for that.
However,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Congrats for that.</p>
<p>However, guess what happened, when I clicked on the link to <a href="https://www.ietf.org/" rel="nofollow">https://www.ietf.org/</a> (using tor):</p>
<p><strong>Error 1006 Ray ID: xxxxxxxxxxxxxxx • 2015-10-30 18:04:17 UTC<br />
Access denied</strong></p>
<p><strong>What happened?<br />
The owner of this website (<a href="http://www.ietf.org" rel="nofollow">www.ietf.org</a>) has banned your IP address (IP_of_exit_relay).</strong></p>
<p>:-)</p>
<p>Oh, the irony ... Yeah, whatever ..</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113643"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113643" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 31, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113579" class="permalink" rel="bookmark">Congrats for that.
However,</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113643">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113643" class="permalink" rel="bookmark">Sounds like www.ietf.org</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sounds like <a href="http://www.ietf.org" rel="nofollow">www.ietf.org</a> needs an onion address! :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113804" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-113804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113804" class="permalink" rel="bookmark">Could such a clearnet</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could such a clearnet failure auto redirect to the relevant .onion?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-137234"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-137234" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113579" class="permalink" rel="bookmark">Congrats for that.
However,</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-137234">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-137234" class="permalink" rel="bookmark">look how this spy guys are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>look how this spy guys are exposed... they all wants to get you. so you can extend Snowden's list of NSA controled companies. i wish i see some hi9dden service which will lists such websites...</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-113634"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113634" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 31, 2015</p>
    </div>
    <a href="#comment-113634">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113634" class="permalink" rel="bookmark">can you reset the password</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>can you reset the password for cypherpunks? make sure there's a way the pass wont get changed....</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-113693"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113693" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 31, 2015</p>
    </div>
    <a href="#comment-113693">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113693" class="permalink" rel="bookmark">Thanks for your efforts to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for your efforts to ensure IETF did the right thing!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-113802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113802" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2015</p>
    </div>
    <a href="#comment-113802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113802" class="permalink" rel="bookmark">What about Astoria and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about Astoria and Hornet?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-138802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-138802" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 04, 2015</p>
    </div>
    <a href="#comment-138802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-138802" class="permalink" rel="bookmark">&gt; There is not currently</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; There is not currently support inside Tor Browser for setting up a hidden service. There have been a variety of tools over the years that aim to help you set one up, but none of them have really emerged as winners. The real trouble in each case is setting up the webserver in a way that doesn't leak info, since webservers are so complex.</p>
<p>If it is technically feasible, I would like to see a fork of Tails which</p>
<p>o is specialized for running a HS (from home or a server room)</p>
<p>o is usable "out of the box" by anyone with a DVD read/write drive</p>
<p>o is designed to be run off a read-only DVD on a machine with no hard drive</p>
<p>o accepts content, written on another computer using ordinary Tails, which is added via an encrypted USB drive</p>
<p>This would require a new team of developers since the Tails team already has plenty of worthy tasks on their todo list.</p>
</div>
  </div>
</article>
<!-- Comment END -->
