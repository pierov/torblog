title: Major changes to OS X Vidalia Bundle with 0.2.2.1-alpha
---
pub_date: 2009-09-03
---
author: phobos
---
tags:

vidalia bundle
drag and drop OS X install
updated packages
---
categories:

applications
releases
---
_html_body:

<p>As highlighted in the <a href="http://blog.torproject.org/blog/tor-0221alpha-released" rel="nofollow">0.2.2.1-alpha release notes</a>, the Vidalia Bundle for OS X includes some major changes.  Many of these are for ease of use and user experience improvements.  The release of OS X 10.6 (Snow Leopard) gave me a fine excuse to release the improvements.</p>

<p>It's best to <a href="https://www.torproject.org/docs/tor-doc-osx.html.en#uninstall" rel="nofollow">un-install Tor/Vidalia</a> and then install this new bundle; rather than upgrade.  If you want to upgrade, you'll need to update the paths for Tor and Polipo in the Vidalia Settings window. </p>

<p>There has been a lot of testing since this <a href="http://blog.torproject.org/blog/experimental-os-x-drag-and-drop-vidalia-bundle-installer" rel="nofollow">test release</a> of the drag and drop installer for OS X in January.  The main goal was to make installation far easier, less error prone, and keep all of the bundle in a single directory for easier configuration and un-installation.  </p>

<p>The changes are:</p>

<blockquote><p>
- Upgrade Vidalia from 0.1.15 to 0.2.3 in the Windows and OS X installer bundles. See <a href="https://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.2.3/CHANGELOG" rel="nofollow">https://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.2.3/CHA…</a> for details of what's new in Vidalia 0.2.3.<br />
- OS X Vidalia Bundle: move to Polipo 1.0.4 with Tor specific configuration file, rather than the old Privoxy.<br />
- OS X Vidalia Bundle: Vidalia, Tor, and Polipo are compiled as x86-only for better compatibility with OS X 10.6, aka Snow Leopard.<br />
- OS X Vidalia Bundle: The multi-package installer is now replaced by a simple drag and drop to the /Applications folder. This change occurred with the upgrade to Vidalia 0.2.3.
</p></blockquote>

