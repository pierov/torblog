title: This is What a Tor Supporter Looks Like: Shari Steele
---
pub_date: 2015-12-16
---
author: ssteele
---
tags:

funding
crowdfunding
---
categories: fundraising
---
_html_body:

<p><a href="https://www.torproject.org/donate/donate.html.en" rel="nofollow"><br />
<img src="https://extra.torproject.org/blog/2015-12-16-what-tor-supporter-looks-shari-steele/blog-shari_hanna_tor_supporters.jpg" alt="Shari Steele and Her Daughter Hanna" /><br />
</a></p>

<p>I first heard of what was to become the Tor Project around 2002.  At that time, I don't think any of us realized how essential Tor was going to be to the Internet freedom movement.</p>

<p>Back in the 1990s, I had been a staff attorney at the Electronic Frontier Foundation (EFF) and was part of the legal team that sued the government on behalf of mathematician Dan Bernstein to make the use of encryption legal for non-military purposes like privacy protection.  At that time, releasing encryption on the Internet required a license to be an arms dealer.  The government claimed that its classification of encryption as a munition--right alongside B-1 bombers and flamethrowers--was a national security decision, making it difficult to challenge in court.  EFF challenged the classification on First Amendment grounds, resulting in a court ultimately ruling that cryptographic source code was protected speech and making the use of encryption legal.  This paved the way for electronic commerce, because now credit cards could be used on the Internet, with credit card numbers encrypted as part of the transactions.  It also paved the way for individuals to use encryption to protect their private communications.</p>

<p>However, in reality, early attempts at widespread encryption were clunky and hard to use, and very few individuals were actually using encryption to protect their own privacy.  Roger Dingledine began work on The Onion Router, or Tor, in 2002. Nick Mathewson was soon to follow (since he wanted it to work on his laptop).  Many EFF staffers were familiar with Tor from the outset, and they believed it was one of the most promising tools being developed with the potential for widespread deployment of encryption for individual privacy protection.</p>

<p>In 2004, Nick and Roger approached EFF to see if we could help them find funding. EFF staffers were concerned that the Tor Project would fail if we didn't help.  By this time I was EFF's executive director, and in October I asked the EFF board to amend our budget to allow for EFF to fund Tor ourselves.  The board voted unanimously on the budget change, and tor.eff.org was born.  EFF attorneys helped to write Tor's original FAQ; one of EFF's technologists helped to design the original Tor onion logo; and Tor was generally considered an EFF project at that time.  When Nick and Roger were ready to go out on their own, I continued to help as best as I could, having EFF serve as Tor's fiscal sponsor, which enabled them to receive funding with nonprofit status until their own 501(c)(3) determination came through.</p>

<p>I've always been immensely proud of the Tor Project.  What started as a proof of concept became what is today the strongest, most censorship-resistant privacy network in the world.  Tor is an essential part of the Internet freedom infrastructure.  And now I'm back working with Nick and Roger, this time building out Tor's operational side to complement its amazing technology.  But building out the organization requires funding that is not restricted.  That's why this end-of-year crowdfunding campaign is so important.  We need your support to help Tor become sustainable over the long term.  We have raised $75,000 since kicking it off, and need your help to break the $100K mark!  <a href="https://www.torproject.org/donate/donate.html.en" rel="nofollow">Please give what you can to The Tor Project today</a>. Don't forget: for a limited time, donations will be matched thanks to the generous contributions of <a href="https://blog.torproject.org/blog/double-your-donation-rabbi-rob-and-lauren-thomas-announce-matching-challenge-supporttor" rel="nofollow">Rabbi Rob and Lauren Thomas</a>!</p>

---
_comments:

<a id="comment-141911"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-141911" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2015</p>
    </div>
    <a href="#comment-141911">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-141911" class="permalink" rel="bookmark">Thank you Tor, Thank you EFF.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you Tor, Thank you EFF.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-141949"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-141949" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2015</p>
    </div>
    <a href="#comment-141949">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-141949" class="permalink" rel="bookmark">Censorship resistant? What a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Censorship resistant? What a lie.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-142192"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142192" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-141949" class="permalink" rel="bookmark">Censorship resistant? What a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-142192">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142192" class="permalink" rel="bookmark">Yeah, so many sites and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yeah, so many sites and services blocking Tor, Cloudflare being the largest and most toxic to Tor.<br />
I've noticed a considerable increase in Cloudflare captcha's lately... it's reaaly frustrating.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-142252"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142252" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142192" class="permalink" rel="bookmark">Yeah, so many sites and</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-142252">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142252" class="permalink" rel="bookmark">Is theregister.co.uk</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is theregister.co.uk blocking Tor now?  Bad news if so.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-142489"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142489" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142252" class="permalink" rel="bookmark">Is theregister.co.uk</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-142489">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142489" class="permalink" rel="bookmark">It appears so, but maybe not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It appears so, but maybe not intentionally. Their IT crew are a bit of a shower. </p>
<p>Meanwhile, Ars Tech UK works with Tor and fills much the same role.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-143181"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143181" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142489" class="permalink" rel="bookmark">It appears so, but maybe not</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-143181">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143181" class="permalink" rel="bookmark">I need &#039;em both.  
@Shari:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I need 'em both.  </p>
<p>@Shari: can you work with The Register to resolve the issue?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-142249"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142249" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-141949" class="permalink" rel="bookmark">Censorship resistant? What a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-142249">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142249" class="permalink" rel="bookmark">Tor beats censorship imposed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor beats censorship imposed by governments and ISPs, and you know it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-142253"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142253" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-141949" class="permalink" rel="bookmark">Censorship resistant? What a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-142253">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142253" class="permalink" rel="bookmark">Are you having trouble using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are you having trouble using Tor in an unfriendly nation?  If so, it's possible that people here can help you solve the issue if you provide more information about what happens when you try to use Tor Browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-142147"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142147" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2015</p>
    </div>
    <a href="#comment-142147">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142147" class="permalink" rel="bookmark">@ Shari
I would like to ask</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Shari</p>
<p>I would like to ask how you can know that you have raised $75,000 so far? Does this include all ways to donate?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-142203"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142203" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2015</p>
    </div>
    <a href="#comment-142203">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142203" class="permalink" rel="bookmark">What she means by censorship</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What she means by censorship resistant, is that Tor browsing is used for national firewall circumvention all over the world.  Places where you are not intended to be able to get to the open internet, you can reach it because of Tor.  </p>
<p>Likewise, sites such as Wikileaks which host information which would otherwise be subject to censorship by one authority, set up repositories as .onion sites as Tor hidden services.</p>
<p>Respectfully, it isn't a lie.  It's used by hundreds of thousands of people as we speak.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-142243"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142243" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2015</p>
    </div>
    <a href="#comment-142243">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142243" class="permalink" rel="bookmark">A big thank you to the Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A big thank you to the Tor project!<br />
Greetings from Amman, Jordan.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-143363"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143363" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2015</p>
    </div>
    <a href="#comment-143363">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143363" class="permalink" rel="bookmark">YOU ALL ROCK .......NEW TO</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>YOU ALL ROCK .......NEW TO SERIOUS CONSIDERATIONS AND use of Tor and Onion.Will be fun and interesting and will seek a place I may possibly help. Not a coder damned it.  Happy 2015 holidays and stunning life events changing NewYear.James Smallwood of Idaho.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-143522"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143522" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2015</p>
    </div>
    <a href="#comment-143522">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143522" class="permalink" rel="bookmark">As far as i can tell Paypal</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As far as i can tell Paypal objects to tor used to donate to tor.  I just tried it!<br />
They also seem to  be discriminating against tor users for trying to pay torproject.  I don't get it!  Why?  They claim it has something to do with the ip address having 1600 people on it.<br />
I also did not see where i could choose the tee-shirt size. </p>
<p>Nor matching.</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-144336"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-144336" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-143522" class="permalink" rel="bookmark">As far as i can tell Paypal</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-144336">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-144336" class="permalink" rel="bookmark">Is it too soon to dream</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it too soon to dream about a post-Tor world which has *not* turned into some kind of techno-fascist neo-Orwellian nightmare?</p>
<p>Imagine thousands of citizens in each US city who support privacy-enabling technology.  Who wear anti-surveillance clothing with a Pi-sized device which functions as an uncensorable node for a strongly end-to-end encrypted citywide citizen messaging network which bypasses the Internet entirely.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-146508"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-146508" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 31, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-144336" class="permalink" rel="bookmark">Is it too soon to dream</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-146508">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-146508" class="permalink" rel="bookmark">hmm...i saw yet hundreds of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hmm...i saw yet hundreds of citizens wearing anti-surveillance clothing (anonymous party) and they went in jail ... or almost ...<br />
this 'surveillance' state will continue as long as usa will go outside their frontiers so ... us go home !<br />
the future is dark, you will not have enough citizens for 'bypassing the internet entirely'</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-147305"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-147305" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 03, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-146508" class="permalink" rel="bookmark">hmm...i saw yet hundreds of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-147305">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-147305" class="permalink" rel="bookmark">&gt; this &#039;surveillance&#039; state</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; this 'surveillance' state will continue as long as usa will go outside their frontiers so ... us go home !</p>
<p>Many people in the US strongly disagree with the worst of the US government's foreign policies. But foreign wars do not explain the rise of the surveillance state in the US (or anywhere else).  The dragnet is in all times and places primarily intended to suppress domestic political opposition.  Because as public anger mounts against misgovernment, minority rule, and brutal oppression, all the world's ruling elites fear losing their wealth and prerogatives through revolution.</p>
<p>In other words: I agree that the USG has been rather effective in the last few decades at destroying other nations, but this should not obscure the fact that it is also hard at work destroying the USA itself.  And that will ultimately be bad for the entire world.</p>
<p>It's all of them against all of us.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-149776"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-149776" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2016</p>
    </div>
    <a href="#comment-149776">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-149776" class="permalink" rel="bookmark">Terrific interview of Shari</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Terrific interview of Shari by Cyrus Farivar in Ars Technica:</p>
<p><a href="http://arstechnica.com/security/2016/01/going-forward-the-tor-project-wants-to-be-less-reliant-on-us-govt-funding/" rel="nofollow">http://arstechnica.com/security/2016/01/going-forward-the-tor-project-w…</a><br />
Two months after FBI debacle, Tor Project still can’t get an answer from CMU<br />
Ars Q&amp;A: We sit down with Tor Project's new executive director, Shari Steele.<br />
Cyrus Farivar<br />
10 Jan 2016</p>
<p>&gt; The two biggest things I want to work on: First is to build up an infrastructure and second is to build up the reputation of the organization and bring in money from alternative sources. A significant amount of the money right now is coming from various US government grants. That's great that there's money coming in, but most of that is restricted money, and you have to work on the specific things that are talked about in the proposal and the grant issuance. So we're looking to find some additional funding sources. There's a big crowdfunding going on right now to get individual donations.</p>
<p>Totally agree.  I greatly hope that the funding drive is a big success.</p>
<p>&gt; As someone who has observed Tor for years and years from the outside, it's actually kind of mind-blowing, the difference between what the project is actually about, the service, and how essential it is to the infrastructure of freedom versus the public's reaction to it is and how it has been received in papers. That really is one of the things that I'm hoping to change.</p>
<p>Hear!  Hear!</p>
<p>&gt; There's a sort of fantasy—how will Tor grow, what would that look like if we had unlimited resources, and how would we make that more accessible—and the fantasy is that maybe someday it's built-in to a privacy option on regular apps that you use. You wouldn't normally have it turned on, and instead when you do your Google search, you would click a switch and say “I would like to browse privately now”—that would be Tor. That's kind of the way we're thinking about it.</p>
<p>Excellent.  Sometimes when big positive changes happen, they happens very quickly.</p>
<p>&gt; CMU isn't talking to Tor. Tor isn't getting the actual facts of what happened... Clearly CMU takes federal money in order to do research that is attacking Tor, and Tor knows about that. So how deeply was CMU involved? Whether CMU actually did the searches for the FBI, or provided the FBI with the vulnerability, we don't know the details.... [CMU and Tor Project] always used to talk to each other. With this particular event, CMU is not talking to Tor. Tor has tried on multiple occasions, particularly when the abstract for the paper first got published, to find out, 'what's the vulnerability, let's get it plugged!' But CMU, they are not talking. Obviously there are individuals at CMU who are friends of ours that we still talk to, but the researchers who are involved in this have not been returning our phone calls.... And this is a little bit of a concern that this is going to affect CERT, because that comes out of CMU... It's very frustrating because CMU is a friend, they should be a friend, we're all working in the same space and we should be all working together. It's very frustrating that our friends are actually attacking the network.</p>
<p>With friends like that, who needs enemies?</p>
<p>&gt; But we now have some serious things in place to pay attention to when a bunch of new nodes are all showing up from the same location or from something similar. It could be disguised if we didn't identify when all the new nodes are coming from the same place, but there are alarms now that go off. In fact, the CMU stuff, they saw the new nodes coming on and it didn't see it as a threat at the time. Now it gets elevated to threat level. So today, hopefully we'll be able to catch at least that vulnerability. It's a cat and mouse game where we're constantly going to have to be vigilant about that.</p>
<p>Exactly.  It's an arms race.  It is actually a huge positive step that the Project has recognized (thanks to people like Jacob Appelbaum, who published a snippet of NSA source code proving that the agency is attacking Directory Authorities) that the USG, or at least parts of the USG, cannot possibly be regarded as a friend of Tor, or indeed as a friend of freedom.</p>
<p>&gt;  the State Department wants Tor for activists and people living in repressive regimes. And then you have another arm of the US government that's actively trying to break it, actively trying to surveil it, actively trying to infiltrate it, and do all kinds of nefarious things. So you have different arms of the government fighting each other.  Not even necessarily different arms, but within the State Department there is offensive and defensive. The same branch of the government can be both trying to defend the network and trying to go out there and attack other people. Yeah, it's pretty psychotic, actually.</p>
<p>One recalls the curious case of a US arson investigator who had won numerous awards for his investigations of numerous arsons.  It turned out that the arsonist was the investigator himself.   One recalls also the case of a forensic science consultant who had won high praise for her role in convicting many persons in high profile murder cases.  It turned out she had been faking all the evidence, which led to an enormously expensive [but cooked] review of all those convictions, because the verdicts had been based upon false evidence.  One recalls the case of the highly regarded DEA agent who arrested a money laundering suspect who had been using Bitcoin for shady transactions.  It later turned out that agent (and a Secret Service colleague) had been themselves using Bitcoin for illegal transactions, and had even tipped off their main suspect.</p>
<p>One recalls the case of the highly regarded FBI cybersecurity expert who had stoutly defended FBI against those who insisted the agency had a "mole" who was tipping off foreign espionage agencies to FBI investigations.  It turned out the mole was the highly regarded FBI agent.  One recalls the case of the highly regarded CIA analyst... </p>
<p>But you get the idea: a large portion of the US federal government has broken bad.</p>
<p>The default in the Tor Project (and indeed in US academia) has been that US government agencies (and their employees) are trustworthy until proven otherwise.  I believe it is a very good thing that the default is becoming: these people must be regarded as untrustworthy, even as adversaries, until they prove otherwise.  (For example by risking their careers/freedom by helping us to make Tor better by informing us about USG zerodays affecting Tor-using products.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-151309"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-151309" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 15, 2016</p>
    </div>
    <a href="#comment-151309">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-151309" class="permalink" rel="bookmark">Something which might be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Something which might be useful should some politician or reporter challenge a TP spokesperson with the claim that "most US persons don't care about privacy" [sic]:</p>
<p><a href="http://thehill.com/policy/technology/265851-poll-most-dont-find-privacy-tradeoff-of-social-media-acceptable" rel="nofollow">http://thehill.com/policy/technology/265851-poll-most-dont-find-privacy…</a><br />
Poll: Most don’t find privacy trade-off of social media acceptable<br />
Mario Trujillo<br />
14 Jan 2016</p>
<p>&gt; a majority of people, 51 percent, said they do not see it as an acceptable trade-off to get free access to a social media service in exchange for that company using their information to deliver targeted ads.<br />
&gt; ...<br />
&gt; The social media scenario was one of two in which a majority of people said the privacy trade-off would be unacceptable.<br />
&gt;<br />
&gt; The other, which 55 percent found unacceptable, dealt with a “smart thermostat” in the home that could save energy but would also gather some information about when you are home and moving from room to room.</p>
<p>I wonder what they'd think of MIT CSAIL's $300 consumer device which allows your neighbors-- and the cops--- to image your body as you move around your apartment, right through the walls.  (The device uses radio waves, and has already been promoted in cop news letters as a "must have" COTS surveillance device.  Roger, please tell them where they can stick their device.  Please.)</p>
<p>Something which might be useful if a Republican lawmaker challenges a Tor spokesperson about encryption: as an illustration of the facts that</p>
<p>o encryption is everywhere</p>
<p>o politicians have no idea what they are talking about, in matters of cybersecurity</p>
<p>see this amusing incident which shows that this exemplar of what passes for the "main line" GOP not only wears a product which (I guess) uses encryption to make and recieve calls, but has no idea how "wearables" work:</p>
<p><a href="http://thehill.com/blogs/blog-briefing-room/news/265782-bushs-editorial-meeting-interrupted-by-apple-watch" rel="nofollow">http://thehill.com/blogs/blog-briefing-room/news/265782-bushs-editorial…</a><br />
Bush newspaper interview interrupted by Apple Watch<br />
Jesse Byrnes<br />
13 Jan 2016</p>
<p>&gt; "My watch can't be talking," the former Florida governor said, before tapping at the device. A voice was heard on the other end. "Is it my watch?" a bemused Bush asks.<br />
&gt;<br />
&gt; "I'll call you back," Bush quips to the voice on the line. "That's the coolest thing in the world."</p>
<p>Really? It was probably a robocall from Rightscorp.</p>
<p>&gt; Bush has sported the smartwatch on the campaign trail for months, though confessed last month that "it's not as intuitive" as other Apple products.</p>
<p>That's really the problem, inn't?  Politicians have no "intuition" for technology.  Certainly they hardly ever have the slightest idea how encryption works or why we all very badly need it to work.  Despite all those "summits" with techco CEOs, they just can't seem to understand the facts of modern life.</p>
</div>
  </div>
</article>
<!-- Comment END -->
