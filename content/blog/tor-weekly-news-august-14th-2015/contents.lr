title: Tor Weekly News — August 14th, 2015
---
pub_date: 2015-08-14
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirty-first issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Browser 5.0 and 5.5a1 are out</h1>

<p>The Tor Browser team put out two new releases of the privacy-preserving web browser. <a href="https://blog.torproject.org/blog/tor-browser-50-released" rel="nofollow">Version 5.0</a>, the first release in the new stable series, is based on Firefox 38ESR, “which should mean improved support for HTML5 video on Youtube, as well as a host of other improvements”. Updates to Tor Browser are now downloaded automatically in the background, removing the need for users to go through the update wizard manually. New privacy features in this release include first-party domain bar isolation of more identifier sources, and “defenses from the 5.0-alpha series for keystroke (typing) fingerprinting and some instances of performance/timing fingerprinting”.</p>

<p>The first <a href="https://blog.torproject.org/blog/tor-browser-55a1-released" rel="nofollow">alpha release</a> in the 5.5 series, meanwhile, fixes the recent <a href="https://blog.mozilla.org/security/2015/08/06/firefox-exploit-found-in-the-wild/" rel="nofollow">pdf.js exploit</a> to which users of 5.0a3 and 5.0a4 had been vulnerable; it also contains a refined version of the new font fingerprinting defenses in which “Tor Browser now ships with a standard set of fonts, and prefers to use the provided fonts instead of native ones in most cases”.</p>

<p>For full changelogs and download instructions, please see the team’s announcements. Both of these new releases contain important security updates, so please upgrade your Tor Browser as soon as you can.</p>

<h1>Tails 1.5 is out</h1>

<p>The Tails developers announced <a href="https://tails.boum.org/news/version_1.5/" rel="nofollow">version 1.5</a> of the anonymous live operating system. This release disables access to the local network in Tor Browser, restricting this activity to Tails’ “unsafe browser”. It also ships with Tor Browser 5.0, and a 32-bit GRUB EFI bootloader, so “Tails should now start on some tablets with Intel Bay Trail processors, among others”.</p>

<p>For a list of all the changes in this release, please see the team’s announcement. This is an important security update, so please download your copy as soon as possible, either from the Tails website or via the incremental updater.</p>

<h1>OnioNS beta testing version is out</h1>

<p>Jesse Victors <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009221.html" rel="nofollow">announced</a> the first beta testing release of his Tor Summer of Privacy project, the Onion Name System (OnioNS). OnioNS is a distributed system that links hard-to-remember and hard-to-verify onion service addresses (such as “onions55e7yam27n.onion”) to domain names that are easier for humans to read and recall (like “example.tor”).</p>

<p>The software that comprises OnioNS is divided into three main parts: OnioNS-HS, OnioNS-client, and OnioNS-server. These are respectively intended to be run by onion services wishing to claim domain names, clients (such as Tor Browser users) wanting to visit services using these names, and the servers that let the system function. Whichever software you download will also require the OnioNS-common library in order to work.</p>

<p>This is a beta testing version, so Jesse warns that it is not ready to be used on production onion services and that name-claims made now may not survive in the long term. If you’re willing to give the system a try, however, please see Jesse’s message for further information, and feel free to send “feedback as to how usable the system is and areas where it could be improved” to the tor-dev mailing list, or file issues on the <a href="https://github.com/Jesse-V?tab=repositories" rel="nofollow">bug tracker of the relevant software package</a>.</p>

<h1>Miscellaneous news</h1>

<p>Karsten Loesing deployed version 2.4 of <a href="https://onionoo.torproject.org" rel="nofollow">Onionoo</a> , the Tor network data observatory. This release implements an optional “effective_family” field to Onionoo details documents, listing all the relays with which the relay in question is in an effective, mutual family relationship. “The main goal here is to make it easier to detect misconfigured relay families.  This can be relay operators or friendly people watching over the Tor network and reminding relay operators to fix their configurations.”</p>

<p>Colin Childs sent out a <a href="https://lists.torproject.org/pipermail/tor-talk/2015-August/038713.html" rel="nofollow">call</a> for new volunteers to man the Tor help desk, which offers individual support to Tor users all over the world. If you can use Tor Browser and other Tor software with confidence and have a good understanding of the theory behind Tor, know how to use GnuPG (or are willing to learn), and are an active member of the Tor community who wants to help users on an ongoing basis, then please see Colin’s message for more details.</p>

<p>The Tails project sent out its monthly report for <a href="https://tails.boum.org/news/report_2015_07/" rel="nofollow">July</a>, featuring development updates, upcoming events, and summaries of ongoing discussions.</p>

<p>George Kadianakis sent out the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-August/000896.html" rel="nofollow">SponsorR report</a>, and also submitted his own status report for <a href="https://lists.torproject.org/pipermail/tor-reports/2015-August/000894.html" rel="nofollow">July</a>.</p>

<p>Alec Muffett <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009223.html" rel="nofollow">revived</a> the discussion around possible human factors to consider when devising a new and more secure system of onion addresses (such as the one suggested in <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/224-rend-spec-ng.txt" rel="nofollow">proposal 224</a>).</p>

<p>Sue Gardner <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009235.html" rel="nofollow">invited</a> active Tor community members to take part in a short survey as part of her work to devise a long-term strategic plan for the Tor Project.</p>

<p>Thomas White put out a <a href="https://lists.torproject.org/pipermail/tor-talk/2015-August/038718.html" rel="nofollow">call</a> for “good guides on using Tor with common applications” to form part of a “small site dedicated to Tor usage [that] will convey, in as simple as possible terms, how to put as many applications as possible through Tor”.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

