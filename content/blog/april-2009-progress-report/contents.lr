title: April 2009 Progress Report
---
pub_date: 2009-05-12
---
author: phobos
---
tags:

progress report
tor browser bundle
bug fixes
---
categories:

applications
releases
reports
---
_html_body:

<p><strong>New releases</strong><br />
On April 12, we released 0.2.1.14-rc.  Read the details <a href="http://blog.torproject.org/blog/tor-02114rc-released" rel="nofollow">in the announcement</a>.</p>

<p><strong>Outreach</strong><br />
Roger attended an ITSG conference in Chicago.</p>

<p>Roger, Nick, Jacob, and Mike attended the CodeCon conference in San Francisco, <a href="http://www.codecon.org/2009/" rel="nofollow">http://www.codecon.org/2009/</a>.</p>

<p>Andrew met with the Center for Democracy and Human Rights in Saudi Arabia to discuss using Tor for their mission, <a href="http://cdhr.info" rel="nofollow">http://cdhr.info</a>.</p>

<p>Roger and Andrew met with the Department of Justice CyberCrime Division to give an overview of how Tor works and how we could better work with law enforcement.</p>

<p>Wendy, Roger, and Andrew had a dinner with Internews Central Asia media development staff.</p>

<p>Andrew attended the CIMI/NED panel on World Press Freedom, <a href="http://cima.ned.org/860/world-press-freedom-day-2009.html" rel="nofollow">http://cima.ned.org/860/world-press-freedom-day-2009.html</a>.  </p>

<p>Andrew attended Boston Barcamp4 and spoke about Free Network Services and Online Privacy, <a href="http://www.barcampboston.org/" rel="nofollow">http://www.barcampboston.org/</a>.  </p>

<p>Roger and Andrew met with Human Rights in China to give an overview of Tor and possible applications for their mission.</p>

<p><strong>Scalability</strong><br />
From the 0.2.1.14-rc changelog:<br />
Clients replace entry guards that were chosen more than a few months ago. This change should significantly improve client performance, especially once more people upgrade, since relays that have been a guard for a long time are currently overloaded. </p>

<p>Continued work on TorFlow, a tool for scanning the public Tor network and detecting misconfigured, overloaded, and evil nodes.  </p>

<p><strong>Translations</strong><br />
Count and languages updated:<br />
20 Japanese website<br />
16 Portugese website<br />
3 Polish website<br />
3 Chinese website<br />
7 French website<br />
14 Italian website<br />
31 Norwegian website<br />
1 Danish website<br />
1 Vietnamese torbutton<br />
1 Turkish torbutton<br />
1 Greek torbutton<br />
1 Arabic torbutton<br />
1 Ukranian torcheck<br />
1 Netherland torcheck<br />
1 Thai torcheck<br />
1 Burmese torcheck<br />
1 German website<br />
2 Russian website<br />
1 Hindi torcheck<br />
1 Greek torcheck</p>

---
_comments:

<a id="comment-1257"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1257" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 11, 2009</p>
    </div>
    <a href="#comment-1257">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1257" class="permalink" rel="bookmark">better work with law enforcement</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What on earth do you mean by "better work with law enforcement"? Unless you mean better educate the fruitlessness of going after Tor users/operators. Otherwise it is a scary thing to suggest as it implies Tor's node operators or developers are making it possible to penetrate Tor!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1314"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1314" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1257" class="permalink" rel="bookmark">better work with law enforcement</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1314">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1314" class="permalink" rel="bookmark">re:  better work with law enforcement</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'll do a blog post about this, since people seem to assume the worst about us talking to law enforcement.  We talk to them to educate them.  If they know about Tor, it's capabilities and realities separate from the myths, it's better for everyone in the end.</p>
<p>For what people seem to think, no, there is not a backdoor in Tor, nor will we ever put one in.  Our FAQ clearly answers this as well, <a href="https://www.torproject.org/faq#Backdoor" rel="nofollow">https://www.torproject.org/faq#Backdoor</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1260"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1260" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 11, 2009</p>
    </div>
    <a href="#comment-1260">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1260" class="permalink" rel="bookmark">How does CALEA</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How does CALEA (Communications Assistance for Law Enforcement Act) impact Tor users in the US? It doesn't seem like it actually makes it possible for the US government to locate a user in practice as is suggested by the wikipedia entry on Tor. If I'm not mistaken this would require the simultaneous monitoring of all Tor entry nodes. Normally one might think this would require a search warrant. However the PATRIOT act Section 216 expanded the government's authority to conduct surveillance in criminal investigations using pen registers or trap and trace devices. No warrant is required so long as the government can show relevance to its investigation. Might that open up warrant-less monitoring to the extent a simple timing attack can be performed? Eventually a US Tor user is bound to end up connecting to a Tor entry node within the US that the government can monitor and so long as they monitor all entry nodes without a warrant Tor users are helpless against government spying. Maybe this is not something Tor is trying to protect against- but is it not oppressive governments Tor is trying to protect users from?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1261"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1261" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 11, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1260" class="permalink" rel="bookmark">How does CALEA</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1261">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1261" class="permalink" rel="bookmark">*an overly broad search</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>*an overly broad search warrant that is.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1315"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1315" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1260" class="permalink" rel="bookmark">How does CALEA</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1315">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1315" class="permalink" rel="bookmark">re:  How does CALEA</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is not a telco, therefore we're not subject to CALEA, at least as far as we've been advised.  </p>
<p>There are no pen registers, trap, or trace devices that will work with Tor, so I suppose this is right out as well.  These terms are also telco terms, as I understand it, and Tor is not a telco.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1438"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1438" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 29, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1260" class="permalink" rel="bookmark">How does CALEA</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1438">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1438" class="permalink" rel="bookmark">Easy for NSA to monitor you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They don't have to monitor every entry node, they can monitor you at the ISP level.</p>
<p>your comuter - your ISP - Tor Entrynode - Tot Middlenode - Tor Exitnode - Internet</p>
<p>Then when your using an NSA controlled Exitnode they got a match.<br />
The NSA nodes(like nixnix....) are known for "hijacking" circuits witch gives you an increased chance of using one of there nodes if you don't block them in torrc</p>
<p>PS Admin why do you force people to use cookies, you shouldn't do such a thing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1448"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1448" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 31, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1438" class="permalink" rel="bookmark">Easy for NSA to monitor you</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1448">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1448" class="permalink" rel="bookmark">re: NSA monitoring</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Let me point you to: <a href="https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#WhatProtections" rel="nofollow">https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#WhatProtectio…</a></p>
<p>and <a href="http://git.torproject.org/checkout/tor/master/doc/design-paper/tor-design.html" rel="nofollow">http://git.torproject.org/checkout/tor/master/doc/design-paper/tor-desi…</a></p>
<p>See Section 3.1, "A global passive adversary is the most commonly assumed threat when analyzing theoretical anonymity designs. But like all practical low-latency systems, Tor does not protect against such a strong adversary. Instead, we assume an adversary who can observe some fraction of network traffic; who can generate, modify, delete, or delay traffic; who can operate onion routers of his own; and who can compromise some fraction of the onion routers. "</p>
<p>If your adversary can observe the entire Internet at once, such as NSA is believed capable, Tor won't protect you.  I suspect the NSA's actual capabilities and what people believe they can do are highly divergent.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1836"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1836" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anybody (not verified)</span> said:</p>
      <p class="date-time">July 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-1836">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1836" class="permalink" rel="bookmark">NSA &amp; DHS</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes and no.  I have little doubt that if the NSA decided to concentrate a large fraction of it's resources (electronic and protoplasmic) on a single secured target (e.g., you or me), that target would fall quickly, no matter what security measures it employed.  The reality, though, is that while NSA resources are vast and of highest quality, they remain finite, and neither you nor I is likely to become a target at that level of interest - the organization has too many "mandatory" adversaries.  All security, information and otherwise, rests solely on the premise of making the compromise of a target more expensive than the value of the compromised results.  Period.  I worry more about DHS.  They don't (yet) have the resources of NSA, but they have been expanding what they have rapidly, their charter is schizophrenic, and unlike NSA their mission has been highly politicized from the inception.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-1337"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1337" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://tracesofevil.blogspot.com">Keir (not verified)</a> said:</p>
      <p class="date-time">May 15, 2009</p>
    </div>
    <a href="#comment-1337">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1337" class="permalink" rel="bookmark">Blogspot blocked in China</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>After all the work I put into my sites, I wake up to find the Chinese fascists here have shut down Blogspot and my proxy.pac no longer works...<br />
Meanwhile youtube is STILL completely blocked!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1359"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1359" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 21, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1337" class="permalink" rel="bookmark">Blogspot blocked in China</a> by <a rel="nofollow" href="http://tracesofevil.blogspot.com">Keir (not verified)</a></p>
    <a href="#comment-1359">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1359" class="permalink" rel="bookmark">You can try setting up a I2P</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can try setting up a I2P website, its safe the host is anonymous<br />
and its faster then Tor's hidden service, it also has mechanism against ddos.<br />
you can also share your stuff via bt/mule client over I2P.<br />
(i2p is not just a proxy, please use Tor for normal web browsing)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1344"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1344" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 18, 2009</p>
    </div>
    <a href="#comment-1344">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1344" class="permalink" rel="bookmark">Thank you -- a Mideast activist</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would just like to write a personal thank you for all your efforts. We, the always threatened Middle East human rights activists, could never work here without such software while keeping other technical security precautions in mind.</p>
<p>You job is amazing for <em>real democracy and freedom</em> for us -- the people -- under the current Mideast oppressive regimes which are also ironically supported directly and indirectly by the United States. </p>
<p>That's the kind of freedom the people here are in urgent need for, not the Mcdonalds and CocaCola "democracy" kind.</p>
<p>I would just like to finally ask a small question: what is the level of security damage that can be caused if someone working in activism in hostile regions contributed to Tor using his <em>real</em> identity?</p>
<p>Warm thanks, an NGO delegate.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1353"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1353" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1344" class="permalink" rel="bookmark">Thank you -- a Mideast activist</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1353">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1353" class="permalink" rel="bookmark">re: Thank you -- a Mideast activist</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank for the kind comments.  As far as the security damage,  the risk is to the person, not the Tor network.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1418"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1418" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 26, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1344" class="permalink" rel="bookmark">Thank you -- a Mideast activist</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1418">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1418" class="permalink" rel="bookmark">security damage</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you would be at risk in your Country if you express your thoughts about democracy and freedom IRL then it would probably be safest for you to work under an alias when contributing to Tor.</p>
<p>It could be easier for your government to map/track/decrypt your other activities on the internet if they can link some of the traffic that they knew is yours back to you.<br />
Worst case for you would probably mean jail/death depending on what country &amp; how serious these activist activitys are seen by your government.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1351"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1351" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 20, 2009</p>
    </div>
    <a href="#comment-1351">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1351" class="permalink" rel="bookmark">MacOS version...</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'd love to get in touch with the current maintainer of the MacOS versions of TOR.<br />
A while back there was an "expert version" of TOR that included TOR &amp; Privoxy (both including nice /Library/StartupItems/ launch scripts. Unfortunately recent "expert versions" only include TOR.</p>
<p>I think it would be great to add Privoxy (including a launchscript) again, because the Vidalia Packet really sucks, compared to the automatically launched TOR &amp; Privoxy that just "wait in the background" until the user enables the firefox "TOR-Button".</p>
<p>This was soooo much nicer.<br />
I'd love to work with the current maintainer on such a package - unfortunately i couldn't find the old packages that included privoxy and the launchscripts.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1354"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1354" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1351" class="permalink" rel="bookmark">MacOS version...</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1354">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1354" class="permalink" rel="bookmark">MacOS version...</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The change to just Tor in the expert package was to be consistent with the other packages we create, where Tor for experts is just Tor and nothing else.  Many people didn't like Tor and Privoxy in a package, they wanted just Tor.</p>
<p>You can always download the Vidalia bundle and just not install vidalia.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1818"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1818" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>IranHelp (not verified)</span> said:</p>
      <p class="date-time">July 14, 2009</p>
    </div>
    <a href="#comment-1818">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1818" class="permalink" rel="bookmark">Question about CALEA &amp; ISP</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>After some technical issues (with HTTP blocks) brought up the subject of Tor, my ISP sent this comment:</p>
<p>"Using the TOR application opens yourself to multiple possible legal ramifications.  Child Porn, illegal downloading, etc.  It will not be a situation where the authorities come to us and tell us to tell the user to stop.  It will be a situation where it will be beyond our control filed with a  CALEA request, in which you will never even be able to tell that they are sniffing your traffic, and we will not be able to tell you either.  Encrypted or not, I don’t think I would let myself be open to anything that is beyond my control."</p>
<p>Discouraging, to say the least, regarding being an exit node.</p>
<p>1. Can I be thrown in jail because someone overseas in doing child porn using our network as an exit?<br />
2. Doesn't Tor itself police this?<br />
3. Doesn't the Tor client purposely avoid having the exit node and the entry node on the same subnet?  In other words, why would law enforcement think that we were at the beginning of the relay?<br />
4. On the other hand, how do I prove that the request for porn DIDN'T come from my network?  After all, the last unencrypted hop did lead back to our network...</p>
<p>Our goal is to help oppressed people in Iran and elsewhere.  If we are attacked by law enforcement and saddled by huge legal bills while defending our innocence, what good has that done?  We are using virtual machines for Tor, so the law is likely to seize all our servers and destroy our ability to do business for an extended period, since they don't know where the virtual machine is.</p>
<p>Please pass on some answers.  Yes, I realize Tor isn't an ISP or Telco-- but the Tor exit traffic must pass through ours, and they will quietly tell the cops everything they know about me in a heartbeat.</p>
<p>Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1832"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1832" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>IranHelp (not verified)</span> said:</p>
      <p class="date-time">July 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1818" class="permalink" rel="bookmark">Question about CALEA &amp; ISP</a> by <span>IranHelp (not verified)</span></p>
    <a href="#comment-1832">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1832" class="permalink" rel="bookmark">Things are getting worse. </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Things are getting worse.  Our ISP says our IP range is now on the DNSBL, and they have cut our bandwidth.  I assume this is because I had allowed e-mail exit, and jerks on the other end used our servers for SPAM.</p>
<p>We run a business here; I cannot afford this kind of issue.  I want to help the freedom fighters of Iran, not spam toads.  We will no longer run an exit, at least not until I am certain we can avoid having our reputation trashed by greedy jerks out there who don't have the guts to identify themselves.</p>
<p>Please advise; I assume the people running Tor read this blog.</p>
<p>Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1837"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1837" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1818" class="permalink" rel="bookmark">Question about CALEA &amp; ISP</a> by <span>IranHelp (not verified)</span></p>
    <a href="#comment-1837">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1837" class="permalink" rel="bookmark">policing content</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I cannot answer for the TOR team, but I can tell you that policing and making morality judgments on content piped over the TOR network is probably the very last thing that they would want to get involved in.  Not only are such judgments frequently highly debatable and disputable, but under some precedent in internet-related law, creating the ability to police content can create a legal obligation to do so, by the standards of the prevalent jurisdiction.  You appear to be in an unenviable position, and you have my sympathy, but you have been put in that position by authoritarian government (it isn't just for foreigners any more) and ISPs that willingly serve as its lackeys, not by TOR or its participants.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1842"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1842" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Not a legal advisor (not verified)</span> said:</p>
      <p class="date-time">July 16, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1818" class="permalink" rel="bookmark">Question about CALEA &amp; ISP</a> by <span>IranHelp (not verified)</span></p>
    <a href="#comment-1842">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1842" class="permalink" rel="bookmark">re: Question about CALEA &amp; ISP</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Your ISP is misguided.  Do they actively filter out all bad things on the Internet and vet all customers before they take a single payment?  Do they actively hunt down customers infected with trojans/botnets/zombies and help them clean it up?  Do they actively watch content looking for 'bad things' and disconnect customers who do so?</p>
<p>I bet in every question, the answer is no.</p>
<p>1. Can I be thrown in jail because someone overseas in doing child porn using our network as an exit?</p>
<p>No.  Tor is an anonymizing layer on the Internet, it doesn't concern itself with content.  This is like a road system, does the M1 care if bank robbers or grandmothers drive on it?</p>
<p>2. Doesn't Tor itself police this?</p>
<p>See the answer to #1.  I don't want Tor to police anything.  I want raw, unfiltered access to the Internet.  I'll filter what I want, not the network.  I want the fat stupid pipe my ISP is supposed to be, but frequently isn't.</p>
<p>3. Doesn't the Tor client purposely avoid having the exit node and the entry node on the same subnet? In other words, why would law enforcement think that we were at the beginning of the relay?</p>
<p>Because they don't understand how Tor works.  Or they're just being stupid to scare you into compliance.  They should talk to the Tor people to find out more.</p>
<p>4. On the other hand, how do I prove that the request for porn DIDN'T come from my network? After all, the last unencrypted hop did lead back to our network...</p>
<p>You can't prove a negative.  In most countries, you are innocent by default.  Quickly the world is moving to everyone is a criminal by default, which is a sad state of affairs.  Perhaps citizens will throw out the stupid politicians before they have no right to do so.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1850"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1850" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>IranHelp (not verified)</span> said:</p>
      <p class="date-time">July 16, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1842" class="permalink" rel="bookmark">re: Question about CALEA &amp; ISP</a> by <span>Not a legal advisor (not verified)</span></p>
    <a href="#comment-1850">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1850" class="permalink" rel="bookmark">Misguided or not, my ISP</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Misguided or not, my ISP doesn't want a Tor relay anywhere on their large customer base.  They claim that we are violating their TOS, because the client allows inbound connections (on port 80 or whatever) which we do not originate.  In other words, we are running a web server (in the form of an open proxy) which they don't allow.</p>
<p>Based on several conversations, I think you are right about law enforcement-- they don't understand what Tor does, and therefore may confiscate every computer in the relay, even though there is nothing on it they can use unless they can seize the entire relay chain (and even that is questionable).  This greatly concerns the ISP.  In their defense, I can see that what we are doing could cause them headaches, which they don't get paid for.</p>
<p>We will not do Tor again except on a VSP, assuming we can find one that allows it.  I also wonder if there are settled court cases which back up your "Tor relay innocent until proven guilty" claim in the US.  In other words, I don't want to lose our business and spend $20K+ being the precedent setting case!</p>
<p>Thanks for the reply.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1867"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1867" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 17, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1850" class="permalink" rel="bookmark">Misguided or not, my ISP</a> by <span>IranHelp (not verified)</span></p>
    <a href="#comment-1867">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1867" class="permalink" rel="bookmark">re: Misguided or not, my ISP</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Depending how much you want to fight your ISP, ask them for the exact clause Tor violates in the ToS.  Some ToS clearly state "no proxy servers of any kind", which technically includes ssh.  Perhaps you should re-think your choice of ISP if they're this heavy handed about something as simple as Tor.</p>
<p>Law enforcement will simply see an IP address show up in some log file somewhere.  Since they don't know what's behind that IP, they will go collect the machine to find out.  </p>
<p>To our knowledge, no one has gone to court over running a Tor node.</p>
<p>If you're concerned about losing business, then run the Tor node(s) on distinct hardware from your business servers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-1940"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1940" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Traduceri legalizate   ">Traduceri lega… (not verified)</span> said:</p>
      <p class="date-time">July 27, 2009</p>
    </div>
    <a href="#comment-1940">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1940" class="permalink" rel="bookmark">Running the Tor nodes on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Running the Tor nodes on distinct hardware is one of the solutions. I still don't understand why someone might have problems if they use Tor. It's the internet! The virtual land of freedom and all possibilities...</p>
</div>
  </div>
</article>
<!-- Comment END -->
