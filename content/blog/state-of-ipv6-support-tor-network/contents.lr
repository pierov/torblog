title: The State of IPv6 support on the Tor network
---
pub_date: 2021-01-14
---
author: gaba
---
tags:

ipv6
RIPE
tor
---
categories: network
---
summary:

In our last article, published in RIPE's website, we described the work that happened in 2020 related to giving IPv6 support to the Tor network. Tor 0.4.5.1-alpha is the first release that includes all the work described in the RIPE article. Relays running 0.4.5.1-alpha are the first to report IPv6 bandwidth statistics. 
---
_html_body:

<p style="line-height:1.38">In <a href="https://labs.ripe.net/Members/tor_grants/a-look-into-the-tor-network-work-on-supporting-ipv6">our last article</a>, published in <a href="https://labs.ripe.net/Members/tor_grants/a-look-into-the-tor-network-work-on-supporting-ipv6">RIPE's website</a>, we described the work that happened in 2020 related to giving <a href="https://blog.torproject.org/ipv6-future-i-hear">IPv6 support</a> to the Tor network.</p>
<p style="line-height:1.38"><a href="https://blog.torproject.org/node/1949  ">Tor 0.4.5.1-alpha</a> is the first release that includes all the work described in the RIPE article. Relays running 0.4.5.1-alpha are the first to report IPv6 bandwidth statistics.</p>
<p><img alt="Tor Versions" src="/static/images/blog/inline-images/versions.png" class="align-center" /></p>
<p>
 </p>
<p style="line-height:1.38">As of December 2, 2020, 54% of the relays on the network run a version of Tor that supports IPv6. Of the 6852 relays in the network, 3587 are running <a href="https://metrics.torproject.org/versions.html">version 0.4.4 </a>and 8 relays are running the <a href="https://blog.torproject.org/node/1958">latest Tor version 0.4.5</a>. From all those, 1588 are announcing an IPv6 address and port for the OR protocol. 1587 relays are reachable on IPv6 by the directory authorities. 626 permit exiting to <a href="https://metrics.torproject.org/relays-ipv6.html">IPv6 targets</a>.</p>
<p> </p>
<p><img alt="Graph of Relays by IP Version" src="/static/images/blog/inline-images/byipversion_0.png" class="align-center" /></p>
<p style="line-height:1.38"> The latest consensus published in November contains:</p>
<ul>
<li aria-level="1" style="list-style-type:disc">6538 relays:
<ul>
<li aria-level="2">1562 of these (36.39% TCW) have an IPv6 ORPort.</li>
<li aria-level="2">1067 of these (24.86% TCW) support at least partial IPv6 reachability checks.</li>
<li aria-level="2"> 55 of these (1.90% TCW) support full IPv6 reachability checks.</li>
</ul>
</li>
<li aria-level="1" style="list-style-type:disc">2527 "usable guards" and Wgd=0.
<ul>
<li aria-level="2">641 of these (29.73% UGCW) support IPv6 clients.</li>
<li aria-level="2">446 of these (21.33% UGCW) support at least partial IPv6 reachability checks.</li>
<li aria-level="2">36 of these (2.92% UGCW) support full IPv6 reachability checks.</li>
</ul>
</li>
</ul>
<p> </p>
<p><img alt="Fraction of reported bytes for which IP version is known" src="/static/images/blog/inline-images/fraction.png" class="align-center" /></p>
<p> </p>
<p style="line-height:1.38">The IPv6 stats that we are collecting are:</p>
<ul>
<li aria-level="1">the number of Tor relays that support IPv6 reachability checks (full versus partial).</li>
</ul>
<ul>
<li aria-level="1">the number of Tor relays that have an IPv6 ORPort.</li>
</ul>
<ul>
<li aria-level="1">the number of connections, and consumed bandwidth, using IPv4 and IPv6. </li>
</ul>
<ul>
<li aria-level="1">IPv6 bandwidth statistics.</li>
</ul>
<p> </p>
<p style="line-height:1.38">Reminder that for relays to report IPv6 bandwidth statistics, they would have to set the option ConnDirectionStatistics to 1 and leave the ExtraInfoStatistics option enabled (it is on by default) to be able to report IPv6 bandwidth statistics. This is the relay operators’  decision, since relays are operated by volunteers. We will keep the statistics code in the codebase for when <a href="https://gitlab.torproject.org/tpo/core/tor/-/milestones/66 ">0.4.5 </a>is released and relays start reporting it.</p>

---
_comments:

<a id="comment-290896"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290896" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Søborg (not verified)</span> said:</p>
      <p class="date-time">January 15, 2021</p>
    </div>
    <a href="#comment-290896">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290896" class="permalink" rel="bookmark">`ConnDirectionStatisticsin`…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>`ConnDirectionStatisticsin` should be `ConnDirectionStatistics`</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290899"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290899" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gaba
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gaba</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gaba said:</p>
      <p class="date-time">January 15, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290896" class="permalink" rel="bookmark">`ConnDirectionStatisticsin`…</a> by <span>Søborg (not verified)</span></p>
    <a href="#comment-290899">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290899" class="permalink" rel="bookmark">thanks! changing it now</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks! changing it now</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
