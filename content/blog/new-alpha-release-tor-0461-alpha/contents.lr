title: New Alpha Release: Tor 0.4.6.1-alpha
---
pub_date: 2021-03-18
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.6.1-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely next week.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.4.6.1-alpha is the first alpha release in the 0.4.6.x series. It improves client circuit performance, adds missing features, and improves some of our DoS handling and statistics reporting. It also includes numerous smaller bugfixes.</p>
<p>Below are the changes since 0.4.5.7. (Note that this release DOES include the fixes for the security bugs already fixed in 0.4.5.7.)</p>
<h2>Changes in version 0.4.6.1-alpha - 2021-03-18</h2>
<ul>
<li>Major features (control port, onion services):
<ul>
<li>Add controller support for creating version 3 onion services with client authorization. Previously, only v2 onion services could be created with client authorization. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40084">40084</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Major features (directory authorityl):
<ul>
<li>When voting on a relay with a Sybil-like appearance, add the Sybil flag when clearing out the other flags. This lets a relay operator know why their relay hasn't been included in the consensus. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40255">40255</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major features (metrics):
<ul>
<li>Relays now report how overloaded they are in their extrainfo documents. This information is controlled with the OverloadStatistics torrc option, and it will be used to improve decisions about the network's load balancing. Implements proposal 328; closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40222">40222</a>.</li>
</ul>
</li>
<li>Major features (relay, denial of service):
<ul>
<li>Add a new DoS subsystem feature to control the rate of client connections for relays. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40253">40253</a>.</li>
</ul>
</li>
<li>Major features (statistics):
<ul>
<li>Relays now publish statistics about the number of v3 onion services and volume of v3 onion service traffic, in the same manner they already do for v2 onions. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/23126">23126</a>.</li>
</ul>
</li>
<li>Major bugfixes (circuit build timeout):
<ul>
<li>Improve the accuracy of our circuit build timeout calculation for 60%, 70%, and 80% build rates for various guard choices. We now use a maximum likelihood estimator for Pareto parameters of the circuit build time distribution, instead of a "right-censored estimator". This causes clients to ignore circuits that never finish building in their timeout calculations. Previously, clients were counting such unfinished circuits as having the highest possible build time value, when in reality these circuits most likely just contain relays that are offline. We also now wait a bit longer to let circuits complete for measurement purposes, lower the minimum possible effective timeout from 1.5 seconds to 10ms, and increase the resolution of the circuit build time histogram from 50ms bin widths to 10ms bin widths. Additionally, we alter our estimate Xm by taking the maximum of the top 10 most common build time values of the 10ms histogram, and compute Xm as the average of these. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40168">40168</a>; bugfix on 0.2.2.14-alpha.</li>
<li>Remove max_time calculation and associated warning from circuit build timeout 'alpha' parameter estimation, as this is no longer needed by our new estimator from 40168. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34088">34088</a>; bugfix on 0.2.2.9-alpha.</li>
</ul>
</li>
<li>Major bugfixes (signing key):
<ul>
<li>In the tor-gencert utility, give an informative error message if the passphrase given in `--create-identity-key` is too short. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40189">40189</a>; bugfix on 0.2.0.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (bridge):
<ul>
<li>We now announce the URL to Tor's new bridge status at <a href="https://bridges.torproject.org/">https://bridges.torproject.org/</a> when Tor is configured to run as a bridge relay. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/30477">30477</a>.</li>
</ul>
</li>
<li>Minor features (build system):
<ul>
<li>New "make lsp" command to auto generate the compile_commands.json file used by the ccls server. The "bear" program is needed for this. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40227">40227</a>.</li>
</ul>
</li>
<li>Minor features (command-line interface):
<ul>
<li>Add build informations to `tor --version` in order to ease reproducible builds. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32102">32102</a>.</li>
<li>When parsing command-line flags that take an optional argument, treat the argument as absent if it would start with a '-' character. Arguments in that form are not intelligible for any of our optional-argument flags. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40223">40223</a>.</li>
<li>Allow a relay operator to list the ed25519 keys on the command line by adding the `rsa` and `ed25519` arguments to the --list-fingerprint flag to show the respective RSA and ed25519 relay fingerprint. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33632">33632</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (control port, stream handling):
<ul>
<li>Add the stream ID to the event line in the ADDRMAP control event. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40249">40249</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (dormant mode):
<ul>
<li>Add a new 'DormantTimeoutEnabled' option for coarse-grained control over whether the client can become dormant from inactivity. Most people won't need this. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40228">40228</a>.</li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>Change the DoS subsystem heartbeat line format to be more clear on what has been detected/rejected, and which option is disabled (if any). Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40308">40308</a>.</li>
<li>In src/core/mainloop/mainloop.c and src/core/mainloop/connection.c, put brackets around IPv6 addresses in log messages. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40232">40232</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (performance, windows):
<ul>
<li>Use SRWLocks to implement locking on Windows. Replaces the "critical section" locking implementation with the faster SRWLocks, available since Windows Vista. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/17927">17927</a>. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor features (protocol, proxy support, defense in depth):
<ul>
<li>Close HAProxy connections if they somehow manage to send us data before we start reading. Closes another case of ticket <a href="https://bugs.torproject.org/tpo/core/tor/40017">40017</a>.</li>
</ul>
</li>
<li>Minor features (tests, portability):
<ul>
<li>Port the hs_build_address.py test script to work with recent versions of python. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40213">40213</a>. Patch from Samanta Navarro.</li>
</ul>
</li>
<li>Minor features (vote document):
<ul>
<li>Add a "stats" line to directory authority votes, to report various statistics that authorities compute about the relays. This will help us diagnose the network better. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40314">40314</a>.</li>
</ul>
</li>
<li>Minor bugfixes (build):
<ul>
<li>The configure script now shows whether or not lzma and zstd have been used, not just if the enable flag was passed in. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40236">40236</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compatibility):
<ul>
<li>Fix a failure in the test cases when running on the "hppa" architecture, along with a related test that might fail on other architectures in the future. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40274">40274</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller):
<ul>
<li>Fix a "BUG" warning that would appear when a controller chooses the first hop for a circuit, and that circuit completes. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40285">40285</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory authorities, voting):
<ul>
<li>Add a new consensus method (31) to support any future changes that authorities decide to make to the value of bwweightscale or maxunmeasuredbw. Previously, there was a bug that prevented the authorities from parsing these consensus parameters correctly under most circumstances. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/19011">19011</a>; bugfix on 0.2.2.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (ipv6):
<ul>
<li>Allow non-SOCKSPorts to disable IPv4, IPv6, and PreferIPv4. Some rare configurations might break, but in this case you can disable NoIPv4Traffic and NoIPv6Traffic as needed. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33607">33607</a>; bugfix on 0.4.1.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (key generation):
<ul>
<li>Do not require a valid torrc when using the `--keygen` argument to generate a signing key. This allows us to generate keys on systems or users which may not run Tor. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40235">40235</a>; bugfix on 0.2.7.2-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (onion services, logging):
<ul>
<li>Downgrade the severity of a few rendezvous circuit-related warnings from warning to info. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40207">40207</a>; bugfix on 0.3.2.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Reduce the compression level for data streaming from HIGH to LOW. This should reduce the CPU and memory burden for directory caches. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40301">40301</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Remove the orconn_ext_or_id_map structure and related functions. (Nothing outside of unit tests used them.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33383">33383</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Code simplification and refactoring (metrics, DoS):
<ul>
<li>Move the DoS subsystem into the subsys manager, including its configuration options. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40261">40261</a>.</li>
</ul>
</li>
<li>Removed features (relay):
<ul>
<li>Because DirPorts are only used on authorities, relays no longer advertise them. Similarly, self-testing for DirPorts has been disabled, since an unreachable DirPort is no reason for a relay not to advertise itself. (Configuring a DirPort will still work, for now.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40282">40282</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291380"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291380" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>iwanlegit (not verified)</span> said:</p>
      <p class="date-time">March 19, 2021</p>
    </div>
    <a href="#comment-291380">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291380" class="permalink" rel="bookmark">&gt;Major features (statistics)…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;Major features (statistics):<br />
&gt;    Relays now publish statistics about the number of v3 onion services and volume of v3 onion service traffic, in the same manner they already do for v2 onions. Closes ticket 23126.<br />
Does this mean that Tor Metrics will publish a v3 version of "Unique .onion addresses" ? I want it!<br />
<a href="https://metrics.torproject.org/hidserv-dir-onions-seen.html" rel="nofollow">https://metrics.torproject.org/hidserv-dir-onions-seen.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291389"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291389" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291380" class="permalink" rel="bookmark">&gt;Major features (statistics)…</a> by <span>iwanlegit (not verified)</span></p>
    <a href="#comment-291389">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291389" class="permalink" rel="bookmark">What caused the amount of v2…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What caused the amount of v2 addresses to <a href="https://metrics.torproject.org/hidserv-dir-onions-seen.html?start=2019-12-01&amp;end=2021-03-22" rel="nofollow">more than double</a> in 2020-03 and 2020-04? <a href="https://blog.torproject.org/remote-work-personal-safety" rel="nofollow">SARS-CoV-2</a> alone?? A new OnionBalance supporting v3 <a href="https://blog.torproject.org/cooking-onions-reclaiming-onionbalance" rel="nofollow">had been released</a> before it, but I highly doubt it could've led to that large of an increase. <a href="https://blog.torproject.org/more-onions-porfavor" rel="nofollow">#MoreOnionsPorFavor</a> wasn't until 2020-08. The <a href="https://blog.torproject.org/v2-deprecation-timeline" rel="nofollow">Onion Service version 2 deprecation timeline</a> wasn't published until 2020-07, so that didn't cause it, and it explains why the addresses were not v3. Onion traffic in the same period <a href="https://metrics.torproject.org/hidserv-rend-relayed-cells.html?start=2019-12-01&amp;end=2021-03-22" rel="nofollow">did not jump</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291390"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291390" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 23, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291389" class="permalink" rel="bookmark">What caused the amount of v2…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291390">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291390" class="permalink" rel="bookmark">interestingly it&#039;s stayed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>interestingly it's stayed that high ever since</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-291532"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291532" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anony-mouse (not verified)</span> said:</p>
      <p class="date-time">April 06, 2021</p>
    </div>
    <a href="#comment-291532">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291532" class="permalink" rel="bookmark">Does this version resolve v2…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does this version resolve v2 addresses ?<br />
I noted all v2 addresses I tried were not working but v3 was ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291542"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291542" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">April 08, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291532" class="permalink" rel="bookmark">Does this version resolve v2…</a> by <span>anony-mouse (not verified)</span></p>
    <a href="#comment-291542">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291542" class="permalink" rel="bookmark">This version removes support…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This version removes support for v2 onion addresses.  For more information, see <a href="https://blog.torproject.org/v2-deprecation-timeline" rel="nofollow">https://blog.torproject.org/v2-deprecation-timeline</a> .</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291548"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291548" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 10, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to nickm</p>
    <a href="#comment-291548">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291548" class="permalink" rel="bookmark">Would it not be correct then…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would it not be correct then to list this as a "major" feature change? Rather than not listing it at all.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
