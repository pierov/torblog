title: Tor Weekly News — December 10th, 2014
---
pub_date: 2014-12-10
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the forty-ninth issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Browser 4.0.2 and 4.5-alpha-2 are out</h1>

<p>Georg Koppen announced new stable and alpha releases by the Tor Browser team. <a href="https://blog.torproject.org/blog/tor-browser-402-released" rel="nofollow">Tor Browser 4.0.2</a> fixes the Windows compiler bugs that were resulting in frequent crashes, ensures entries in the cache are once again isolated by URL bar domain, and prevents the user’s locale setting from being leaked by the JavaScript engine. <a href="https://blog.torproject.org/blog/tor-browser-45-alpha-2-released" rel="nofollow">Tor Browser 4.5-alpha-2</a> brings further improvements to Torbutton’s new circuit visualization panel, which can now be turned off by visiting about:config and setting “extensions.torbutton.display_circuit” to “false”, as well as to the security slider.</p>

<p>Both releases contain important security updates and all users should upgrade as soon as possible; please see Georg’s post for full details. You can obtain your copy from the <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">project page</a>, or through the in-browser updater.</p>

<h1>Tails 1.2.1 is out</h1>

<p>The Tails team <a href="https://tails.boum.org/news/version_1.2.1/" rel="nofollow">announced</a> a new version of the amnesic live operating system. Alongside updates to Linux and Tor Browser, Tails 1.2.1 finally disables the Truecrypt encryption manager, which was abandoned by its developers earlier this year. There have been warnings about this change for several months, but users who have not yet migrated their data away from Truecrypt (or who are not able to) can still access these volumes with cryptsetup by following <a href="https://tails.boum.org/doc/encryption_and_privacy/truecrypt/" rel="nofollow">Tails’ own guide</a>.</p>

<p>The default configuration of GnuPG has also been changed in line with <a href="https://help.riseup.net/en/security/message-security/openpgp/best-practices" rel="nofollow">accepted best practices</a>. If you want to take advantage of this, there is a simple step you need to perform; please see the team’s post for more details, and get your copy of the new Tails from the <a href="https://tails.boum.org/download/" rel="nofollow">download page</a> or through the incremental updater.</p>

<h1>More monthly status reports for November 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of November continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000718.html" rel="nofollow">Pearl Crescent</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000719.html" rel="nofollow">Sukhbir Singh</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000720.html" rel="nofollow">Leiah Jansen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000721.html" rel="nofollow">Matt Pagan</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000723.html" rel="nofollow">Arlo Breault</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000724.html" rel="nofollow">Colin C.</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000725.html" rel="nofollow">Nicolas Vigier</a>.</p>

<p>Karsten Loesing reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000722.html" rel="nofollow">Tor Network Tools team</a>, and Roger Dingledine sent out the report for <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000726.html" rel="nofollow">SponsorF</a>.</p>

<h1>Miscellaneous news</h1>

<p>George Kadianakis <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007928.html" rel="nofollow">sent out</a> an updated draft of the proposal to safely collect hidden service statistics from Tor relays.</p>

<p>Nick Mathewson gave a <a href="https://www.youtube.com/watch?v=rIf_VZQr-dw" rel="nofollow">talk</a> to the Computer Systems Security class at MIT on the subject of “Anonymous Communication”.</p>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007916.html" rel="nofollow">summarized</a> the costs incurred in November by the infrastructure for the meek pluggable transport.</p>

<p>The Tails team <a href="https://mailman.boum.org/pipermail/tails-dev/2014-December/007580.html" rel="nofollow">wondered</a> about the best way to prioritize adding support for pluggable transports: “Assuming we add support for Scramblesuit in Tails 1.3, then what usecases won’t we be supporting, that we could support better with obfs4 or meek?”</p>

<p>usprey wrote up a <a href="https://lists.torproject.org/pipermail/tor-relays/2014-December/005907.html" rel="nofollow">guide</a> to configuring a Tor relay on a server running Arch Linux: “All and any feedback will be appreciated! Are there any privacy concerns about using pdnsd to cache DNS locally?”</p>

<p>Jacob Appelbaum <a href="https://mailman.boum.org/pipermail/tails-dev/2014-December/007537.html" rel="nofollow">recommended</a> possible ways to reduce the attack surface presented by the kernel and the firewall in Tails. He also <a href="https://mailman.boum.org/pipermail/tails-dev/2014-December/007588.html" rel="nofollow">compiled</a> a dataset containing historical hashes and signatures of Tails files: “In the future, I’ll write a program that uses the dataset in a useful manner. In an ideal world, we’d have a way to use a Tails disk to verify any other Tails disk.”</p>

<h1>Tor help desk roundup</h1>

<p>Users often write to find out how they can help the Tor Project. There are several ways to help out.</p>

<p>If you have access to a server, consider setting up a <a href="https://www.torproject.org/docs/debian" rel="nofollow">Tor relay</a> to expand the network, or a <a href="https://trac.torproject.org/projects/tor/wiki/doc/PluggableTransports#Howtosetupabridgewithpluggabletransports" rel="nofollow">bridge relay</a> to help internet users stuck behind censorship.</p>

<p>If you’re a coder, see if any of the projects on our <a href="https://www.torproject.org/getinvolved/volunteer#Projects" rel="nofollow">volunteer page</a> capture your interest. You can also look for tickets on our <a href="https://trac.torproject.org/projects/tor/report" rel="nofollow">bug tracker</a> that are filed with the “easy” component if you want to submit some patches.</p>

<p>If you’re interested in doing outreach, consider joining the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">Tor Weekly News team</a>.</p>

<p>If you’d like to get involved with translations, please join a team on our <a href="https://www.transifex.com/projects/p/torproject/" rel="nofollow">Transifex</a>. If a team for the language you’d like to translate into does not yet exist (check carefully), please go ahead and request a new team. It will take a day or two for the team to be approved, so please be patient.</p>

<h1>News from Tor StackExchange</h1>

<p>strand raised a question about the code regarding <a href="https://tor.stackexchange.com/q/848/88" rel="nofollow">rendezvous and introduction points</a>. Within src/or/rendservice.c there are several occurrences of onion_address, and strand wants to know which function catches what from a hidden service. If you can answer this question, please come to Tor’s Q&amp;A page and give us some insights.</p>

<h1>This week in Tor history</h1>

<p><a href="https://lists.torproject.org/pipermail/tor-news/2013-December/000024.html" rel="nofollow">A year ago this week</a>, the Freedom of the Press Foundation launched its “Encryption Tools for Journalists” <a href="https://freedom.press/bundle/encryption-tools-journalists" rel="nofollow">crowdfunding campaign</a>, distributing the proceeds to five free software security projects, including the Tor Project and Tails. As of this writing, 1256 donors have contributed $136,977.05 in support of journalists’ right to communicate with sources and carry out research without being subjected to invasive surveillance. Thanks to the FPF and to everyone who has donated so far!</p>

<p>This issue of Tor Weekly News has been assembled by Matt Pagan, qbi, David Fifield, Arlo Breault, Karsten Loesing, and Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

