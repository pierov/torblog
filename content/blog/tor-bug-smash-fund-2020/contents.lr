title: Tor’s Bug Smash Fund: Year Two!
---
pub_date: 2020-07-30
---
author: isabela
---
tags:

bugs
fundraising
---
categories:

fundraising
releases
---
summary: Starting August 1, every donation we receive during the month of August will count towards the Bug Smash Fund 2020. The Bug Smash Fund allows the Tor Project to find and fix bugs in our software and conduct routine maintenance.
---
_html_body:

<p>The Bug Smash Fund is back for its second year! In 2019, <a href="https://blog.torproject.org/tors-bug-smash-fund-help-tor-smash-all-bugs">we launched Tor’s Bug Smash Fund</a> to find and fix bugs in our software and conduct routine maintenance. Maintenance isn’t a flashy new feature, and that makes it less interesting to many traditional funders, but it’s what keeps the reliable stuff working--and with your support, we were able to close 74 tickets as a result.</p>
<p>These bugs and issues ranged from maintenance on mechanisms for sending bridges via email and collecting metrics data to improving tor padding, testing, onion services, documentation, Tor Browser UX, and tooling for development. This work keeps Tor Browser, the Tor network, and the <a href="https://blog.torproject.org/strength-numbers-entire-ecosystem-relies-tor">many tools that rely on Tor</a> strong, safe, and running smoothly.</p>
<p>And there’s so much more we can accomplish. Nineteen tickets tagged <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues?label_name%5B%5D=BugSmashFund&amp;scope=all&amp;state=all">BugSmashFund</a> are still open, and as you know, a big part of building software is ensuring that you can address issues when you find them. As such, starting August 1, every donation we receive during the month of August will count towards the Bug Smash Fund 2020.</p>
<p><a href="https://donate.torproject.org"><img alt="A link to the Tor Project's donate page" src="/static/images/blog/inline-images/tor-donate-button_14.png" class="align-center" /></a></p>
<p>There’s no doubt that a lot has changed since August 2019. Close to half of the 2019 Bug Smash Fund came from your donations in person at DEFCON. But this year the pandemic has had a big impact on our ability to meet our donors in person. And it is with direct donations that we can successfully smash bugs.</p>
<p>We’ve seen a strong show of support from our donors over the last several months, and this demonstrates how important Tor is during a time of protest, social distance, and change. Our goal is to match the amount we raised in the 2019 Bug Smash Fund: $86,081, and we need your support to get there.</p>
<p>There are many different ways to contribute to the Bug Smash Fund, and all of them count towards reaching this goal:</p>
<ul>
<li><a href="https://donate.torproject.org">Make a one-time donation</a> (and get swag like Tor stickers and t-shirts in return)</li>
<li><a href="https://donate.torproject.org/cryptocurrency">Donate in ten different cryptocurrencies</a></li>
<li><a href="https://donate.torproject.org/monthly-giving">Become a monthly donor and get your own Defenders of Privacy patch</a></li>
<li><a href="https://donate.torproject.org/champions-of-privacy">Make a contribution of $1,000 and join the major donor group Champions of Privacy</a></li>
<li><a href="https://opencollective.com/thetorproject">Transfer your Open Collective gift card</a></li>
<li>Use the hashtag #TorBugSmash to share the campaign</li>
</ul>
<p>Your support keeps Tor strong. Thank you for being part of the fight for privacy online.</p>

---
_comments:

<a id="comment-288935"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288935" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 09, 2020</p>
    </div>
    <a href="#comment-288935">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288935" class="permalink" rel="bookmark">Очень прошу - обратите…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Очень прошу - обратите внимание на Беларусь!!! У нас лег практически весь интернет! Даже ProtonVPN несколько раз срывался, пока наконец не подключился более-менее устойчиво....TOR работает через мосты, но все остальное просто лежит, даже знаменитый Telegram....Это все потому, что сегодня выборы президента у нас в стране и диктатор Лукашенко идет на свой 6-й срок, а ведь именно в эти выборы у него очень мощные соперники.....Обратите на нашу страну внимание, пожалуйста....Вам это будет интересно изучать даже исходя из научно-технической точки зрения....Потому что перекрыли практически все социальные сети, Telegram, Google, YouTube, VK и многое другое....</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288941"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288941" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2020</p>
    </div>
    <a href="#comment-288941">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288941" class="permalink" rel="bookmark">I tried to report a bug. I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I tried to report a bug. I registered on gitlab.torproject.org with a throwaway email address, and wrote in the box what the account was for, but I guess by the time someone got around to manually approving my account, the email was gone. I never got it. I just wanted to file a simple bug report. Not really worth going through setting up a real email and everything just for that. What's with the new walled garden approach? I really don't like where this is headed.</p>
</div>
  </div>
</article>
<!-- Comment END -->
