title: Tor 0.2.1.13-alpha released
---
pub_date: 2009-03-13
---
author: phobos
---
tags:

bug fixes
alpha release
security fixes
---
categories: releases
---
_html_body:

<p>Tor 0.2.1.13-alpha includes another big pile of minor bugfixes and<br />
cleanups. We're finally getting close to a release candidate.</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p>Changes in version 0.2.1.13-alpha - 2009-03-09<br />
<strong>Major bugfixes:</strong></p>

<ul>
<li>Correctly update the list of which countries we exclude as<br />
      exits, when the GeoIP file is loaded or reloaded. Diagnosed by<br />
      lark. Bugfix on 0.2.1.6-alpha.</li>
</ul>

<p><strong>Minor bugfixes (on 0.2.0.x and earlier):</strong></p>

<ul>
<li>Automatically detect MacOSX versions earlier than 10.4.0, and<br />
      disable kqueue from inside Tor when running with these versions.<br />
      We previously did this from the startup script, but that was no<br />
      help to people who didn't use the startup script. Resolves bug 863.</li>
<li>When we had picked an exit node for a connection, but marked it as<br />
      "optional", and it turned out we had no onion key for the exit,<br />
      stop wanting that exit and try again. This situation may not<br />
      be possible now, but will probably become feasible with proposal<br />
      158. Spotted by rovv. Fixes another case of bug 752.</li>
<li>Clients no longer cache certificates for authorities they do not<br />
      recognize. Bugfix on 0.2.0.9-alpha.</li>
<li>When we can't transmit a DNS request due to a network error, retry<br />
      it after a while, and eventually transmit a failing response to<br />
      the RESOLVED cell. Bugfix on 0.1.2.5-alpha.</li>
<li>If the controller claimed responsibility for a stream, but that<br />
      stream never finished making its connection, it would live<br />
      forever in circuit_wait state. Now we close it after SocksTimeout<br />
      seconds. Bugfix on 0.1.2.7-alpha; reported by Mike Perry.</li>
<li>Drop begin cells to a hidden service if they come from the middle<br />
      of a circuit. Patch from lark.</li>
<li>When we erroneously receive two EXTEND cells for the same circuit<br />
      ID on the same connection, drop the second. Patch from lark.</li>
<li>Fix a crash that occurs on exit nodes when a nameserver request<br />
      timed out. Bugfix on 0.1.2.1-alpha; our CLEAR debugging code had<br />
      been suppressing the bug since 0.1.2.10-alpha. Partial fix for<br />
      bug 929.</li>
<li>Do not assume that a stack-allocated character array will be<br />
      64-bit aligned on platforms that demand that uint64_t access is<br />
      aligned. Possible fix for bug 604.</li>
<li>Parse dates and IPv4 addresses in a locale- and libc-independent<br />
      manner, to avoid platform-dependent behavior on malformed input.</li>
<li>Build correctly when configured to build outside the main source<br />
      path. Patch from Michael Gold.</li>
<li>We were already rejecting relay begin cells with destination port<br />
      of 0. Now also reject extend cells with destination port or address<br />
      of 0. Suggested by lark.</li>
</ul>

<p><strong>Minor bugfixes (on 0.2.1.x):</strong></p>

<ul>
<li>Don't re-extend introduction circuits if we ran out of RELAY_EARLY<br />
      cells. Bugfix on 0.2.1.3-alpha. Fixes more of bug 878.</li>
<li>If we're an exit node, scrub the IP address to which we are exiting<br />
      in the logs. Bugfix on 0.2.1.8-alpha.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>On Linux, use the prctl call to re-enable core dumps when the user<br />
      is option is set.</li>
<li>New controller event NEWCONSENSUS that lists the networkstatus<br />
      lines for every recommended relay. Now controllers like Torflow<br />
can keep up-to-date on which relays they should be using.</li>
<li>Update to the "February 26 2009" ip-to-country file.</li>
</ul>

<p>The original notice can be found at <a href="http://archives.seul.org/or/talk/Mar-2009/msg00047.html" rel="nofollow">http://archives.seul.org/or/talk/Mar-2009/msg00047.html</a></p>

