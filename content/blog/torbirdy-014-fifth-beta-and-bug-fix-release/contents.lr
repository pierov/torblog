title: TorBirdy 0.1.4: Fifth Beta and Bug Fix Release
---
pub_date: 2015-03-12
---
author: sukhbir
---
tags:

TorBirdy
Thunderbird
icedove
email
---
_html_body:

<p>We are happy to announce the release of TorBirdy 0.1.4, our fifth beta release. This is a bug-fix release, which fixes an issue with TorBirdy 0.1.3 that prevents Thunderbird from starting if three or more than three IMAP accounts are configured. This was reported by users in several tickets (<a href="https://trac.torproject.org/projects/tor/ticket/14099" rel="nofollow">#14099</a>, <a href="https://trac.torproject.org/projects/tor/ticket/13982" rel="nofollow">#13982</a>, <a href="https://trac.torproject.org/projects/tor/ticket/13722" rel="nofollow">#13722</a>, <a href="https://trac.torproject.org/projects/tor/ticket/14007" rel="nofollow">#14007</a>, <a href="https://trac.torproject.org/projects/tor/ticket/14130" rel="nofollow">#14130</a>) and affects all platforms.</p>

<p><strong>Changes in TorBirdy 0.1.4</strong></p>

<blockquote><p>
    0.1.4, 09 March 2015<br />
      * Fix bug that prevented Thunderbird with TorBirdy 0.1.3 from starting<br />
      in profiles with more than three IMAP accounts (closes #14099, #13982,<br />
      #13722, #14007, #14130)
</p></blockquote>

<p><strong>Technical Explanation</strong></p>

<p>This bug was due to a variable in a <i>for</i> lop that was declared twice and was affecting the enumeration of an outer loop (lines 521 and 531 in <i>components/torbirdy.js</i>) used to iterate over IMAP accounts. Please see commit <a href="https://gitweb.torproject.org/torbirdy.git/commit/?id=625f80e1f656f38784c3928cc5e62f1407d1c400" rel="nofollow">625f80e</a> in the TorBirdy repository for the fix.</p>

<p>Users who are not affected by this issue (less than three IMAP accounts configured) may also upgrade but note that this release does not introduce any new features.</p>

<p>We offer two ways of installing TorBirdy -- either by visiting our <a href="https://dist.torproject.org/torbirdy/torbirdy-0.1.4.xpi" rel="nofollow">website</a> (<a href="https://dist.torproject.org/torbirdy/torbirdy-0.1.4.xpi.asc" rel="nofollow">GPG signature</a>) or by visiting the <a href="https://addons.mozilla.org/en-us/thunderbird/addon/torbirdy/" rel="nofollow">Mozilla Add-ons page for TorBirdy</a>. (TorBirdy 0.1.4 has been <a href="https://developer.mozilla.org/en-US/Add-ons/AMO/Policy/Reviews#Full_Review" rel="nofollow">fully reviewed</a> by Mozilla.)</p>

<p><strong>Using TorBirdy for the First Time?</strong></p>

<p>As a general anonymity and security note: we are still working on <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy#InfoLeaks" rel="nofollow">two known anonymity issues</a> with Mozilla. Please make sure that you read the <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy#BeforeusingTorBirdy" rel="nofollow">Before Using TorBirdy</a> and <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy#KnownTorBirdyIssues" rel="nofollow">Known TorBirdy Issues</a> sections on the wiki before using TorBirdy.</p>

<p>We had love help with getting our patches accepted, or anything that you think will help improve TorBirdy!</p>

<p>Feel free to follow along with the release on the <a href="https://lists.torproject.org/pipermail/tor-talk/2015-March/037246.html" rel="nofollow">tor-talk</a> mailing list.</p>

---
_comments:

<a id="comment-89793"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89793" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2015</p>
    </div>
    <a href="#comment-89793">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89793" class="permalink" rel="bookmark">connection failure</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>connection failure</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-89805"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89805" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2015</p>
    </div>
    <a href="#comment-89805">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89805" class="permalink" rel="bookmark">Error report:
Torbirdy 1.13</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Error report:<br />
Torbirdy 1.13 works but, using the newest 1.14;<br />
Icedove - Trisquel GNU/LINUX<br />
Send Message Error<br />
Sending of message failed.<br />
The message could not be sent because connecting to SMTP server mail.riseup.net failed. The server may be unavailable or is refusing SMTP connections. Please verify that your SMTP server settings are correct and try again, or contact the server administrator.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89806"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89806" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">March 12, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89805" class="permalink" rel="bookmark">Error report:
Torbirdy 1.13</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89806">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89806" class="permalink" rel="bookmark">You need Tor running,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You need Tor running, whether standalone Tor or the Tor Browser Bundle. Start the Tor Browser Bundle and then start Thunderbird with TorBirdy. If the issue still persists, let us know here or you can open a ticket on Trac (<a href="https://trac.torproject.org/projects/tor/newticket" rel="nofollow">https://trac.torproject.org/projects/tor/newticket</a>; choose the "TorBirdy" component.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-89837"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89837" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 13, 2015</p>
    </div>
    <a href="#comment-89837">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89837" class="permalink" rel="bookmark">erro de conexao</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>erro de conexao</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89979"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89979" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">March 17, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89837" class="permalink" rel="bookmark">erro de conexao</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89979">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89979" class="permalink" rel="bookmark">What is the exact error?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is the exact error? Note that you need Tor running. See: <a href="https://securityinabox.org/en/guide/thunderbird/windows" rel="nofollow">https://securityinabox.org/en/guide/thunderbird/windows</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-89852"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89852" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2015</p>
    </div>
    <a href="#comment-89852">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89852" class="permalink" rel="bookmark">how to download?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>how to download?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89978"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89978" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 17, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89852" class="permalink" rel="bookmark">how to download?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89978">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89978" class="permalink" rel="bookmark">You can download and install</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can download and install it from the Mozilla Add-ons website. <a href="https://addons.mozilla.org/en/thunderbird/addon/torbirdy/" rel="nofollow">https://addons.mozilla.org/en/thunderbird/addon/torbirdy/</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-89872"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89872" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2015</p>
    </div>
    <a href="#comment-89872">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89872" class="permalink" rel="bookmark">I`m using the add-on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I`m using the add-on 'ProfileSwitcher'  to change between different profiles in Thunderbird. <a href="https://freeshell.de/~kaosmos/profileswitcher-en.html" rel="nofollow">https://freeshell.de/~kaosmos/profileswitcher-en.html</a></p>
<p>Would I harm my anonymity when using TorBirdy and ProfileSwitcher in the same profile?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89980"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89980" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">March 17, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89872" class="permalink" rel="bookmark">I`m using the add-on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89980">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89980" class="permalink" rel="bookmark">It&#039;s difficult to say</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's difficult to say without actually auditing this. If the purpose of this extension is to just switch profiles, make sure that you have TorBirdy installed for all profiles you create. But then again, avoid using add-ons which we have not tested.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-90166"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-90166" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 18, 2015</p>
    </div>
    <a href="#comment-90166">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-90166" class="permalink" rel="bookmark">I can&#039;t use this to login my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't use this to login my gmail account. Gmail refused tor connection.<br />
And I am in China and Gmail had been banned here.<br />
Who would help me with it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-95333"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95333" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 17, 2015</p>
    </div>
    <a href="#comment-95333">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95333" class="permalink" rel="bookmark">Would you consider having an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would you consider having an option to receive and send HTML messages with TorBirdy on your next update?</p>
<p>It is true that HTML is not recommended for use, but on many occasion it is needed when a great level of anonymity is not needed, even if just to enter a hyperlink.</p>
<p>Would love to see it happen.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114066"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114066" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2015</p>
    </div>
    <a href="#comment-114066">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114066" class="permalink" rel="bookmark">Twitter fails to connect,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Twitter fails to connect, without any reported error.<br />
A third-party window asking for login loops after me manually inserting an alias and nickname on icedove chat's new account settings.</p>
<p>By clicking on "signup" in the twitter popup, quickly appears (for miliseconds) a number "14312312..." in the name formulary and then disappears to a blank signup.<br />
Is it a cookie issue? The software has no use for me even with TorBirdy enabled status on the bottom of the page.</p>
<p>Tor browser has no problems connecting to the twitter account.</p>
</div>
  </div>
</article>
<!-- Comment END -->
