title: Volunteer Spotlight: Damian Johnson
---
pub_date: 2017-11-16
---
author: tommy
---
tags:

volunteer spotlight
nyx
Google Summer of Code
arm
---
categories:

internships
network
---
summary: Tor is a labor of love, built by a small group of committed individuals, but we’re lucky to have the support of a dedicated volunteer base who help us make Tor the strongest anonymity tool out there. Today, we’re highlighting one volunteer in particular, Damian Johnson, aka atagar. (Tor mural painted by Ryan 'Henry' Ward.)
---
_html_body:

<p><em>Tor mural painted by <a href="http://www.ryanhenryward.com">Ryan 'Henry' Ward</a>.</em></p>
<p>Tor is a labor of love, built by a small group of committed individuals, but we’re lucky to have the support of a dedicated volunteer base who help us make Tor the strongest anonymity tool out there. Today, we’re highlighting one volunteer in particular, Damian Johnson, aka atagar.</p>
<h3 dir="ltr">Eight Years and Counting<img alt="Damian Johnson" src="/static/images/blog/inline-images/damian-johnson-atagar_0.png" class="align-right" /></h3>
<p dir="ltr">Damian has been contributing to Tor since 2009, when he was <a href="https://blog.torproject.org/summer-conclusion-arm-project">developing</a> a real-time relay which started as a Google’s Summer of Code application. “Though I'll need to pull back the time I contribute to Tor,” he wrote, as summer drew to a close, “I'm still hoping to stay active in the community.”</p>
<p dir="ltr">We’re so glad that Damian has done just that and become so central to Tor. In the past eight years, he has continued to develop arm (<a href="https://blog.torproject.org/meet-nyx-command-line-tor-relay-monitor">which just got a big overhaul, and is now called Nyx</a>) and <a href="https://stem.torproject.org">Stem</a>, a Python controller library to use with Tor. He also graduated to running Google’s Summer of Code for six years, runs <a href="https://gitweb.torproject.org/doctor.git">DocTor</a>, a tool which monitors relay consensus health, and helps us run Tor’s many <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo">mailing lists</a>, and helps onboard new core Tor members.</p>
<p dir="ltr">We’re grateful to Damian’s commitment to internet freedom, and for rolling up his sleeves and helping to improve Tor. Thank you, Damian!</p>
<p dir="ltr">Getting involved with Tor is easy. You can help us make the network faster and more decentralized by <a href="https://www.torproject.org/docs/tor-doc-relay.html.en">running a relay</a>, especially if you live in a <a href="https://twitter.com/TorAtlas/status/930817764671016960">part of the world</a> where we don’t have a lot of relays yet.</p>
<p dir="ltr">“The best defense for civil liberties is to deny centralized authority the power it needs to restrict personal freedoms,” Damian wrote in his GSOC application in 2009. “Privacy is a vital part of that.”</p>
<p dir="ltr">That’s still true today. Tor is a vital tool for protecting privacy and resisting repressive censorship and surveillance. If you can, please consider <a href="http://donate.torproject.org">making a donation</a>. Mozilla is generously matching every donation up to $500,000, so now’s your chance to double your impact.</p>

---
_comments:

<a id="comment-272673"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272673" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>kata (not verified)</span> said:</p>
      <p class="date-time">November 20, 2017</p>
    </div>
    <a href="#comment-272673">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272673" class="permalink" rel="bookmark">well , this Volunteer…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>well , this Volunteer Spotlight article is nice , hello atakar.<br />
( &gt; “The best .. today. "  is an american point of view , very fun)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272682"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272682" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2017</p>
    </div>
    <a href="#comment-272682">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272682" class="permalink" rel="bookmark">You can help us make the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>You can help us make the network faster and more decentralized by running a relay, especially if you live in a part of the world where we don’t have a lot of relays yet.</p></blockquote>
<p>Please provide documentation on trac about examples of typical places where new relays are sought and if possible with some providers and past experience. That would be more than enough for a lot of us to run relays in new places.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272686"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272686" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  tommy
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">t0mmy</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">t0mmy said:</p>
      <p class="date-time">November 21, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272682" class="permalink" rel="bookmark">You can help us make the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-272686">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272686" class="permalink" rel="bookmark">This would be an excellent…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This would be an excellent idea, I'll try to get this information put together. The short answer is that Tor's speed and capacity in the Global South is reduced because we don't have enough exit relays across the Global South.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272700"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272700" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to t0mmy</p>
    <a href="#comment-272700">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272700" class="permalink" rel="bookmark">Here are the only two…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here are the only two examples from the GoodBadISPs wiki on trac:</p>
<p><a href="https://trac.torproject.org/projects/tor/wiki/doc/GoodBadISPs#NewZealandandAustralia" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/doc/GoodBadISPs#NewZealan…</a><br />
<a href="https://trac.torproject.org/projects/tor/wiki/doc/GoodBadISPs#HongKong" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/doc/GoodBadISPs#HongKong</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-272684"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272684" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jack (not verified)</span> said:</p>
      <p class="date-time">November 21, 2017</p>
    </div>
    <a href="#comment-272684">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272684" class="permalink" rel="bookmark">Best</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Best</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272706"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272706" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>bob (not verified)</span> said:</p>
      <p class="date-time">November 22, 2017</p>
    </div>
    <a href="#comment-272706">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272706" class="permalink" rel="bookmark">I remember his timely and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I remember his timely and funny emails.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272725"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272725" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anthony  (not verified)</span> said:</p>
      <p class="date-time">November 24, 2017</p>
    </div>
    <a href="#comment-272725">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272725" class="permalink" rel="bookmark">I&#039;m new here</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm new here</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272728"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272728" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Damian&#039;s mother, Mamatagar">Damian&#039;s mothe… (not verified)</span> said:</p>
      <p class="date-time">November 25, 2017</p>
    </div>
    <a href="#comment-272728">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272728" class="permalink" rel="bookmark">When you were just my little…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When you were just my little son, how many times did I straighten out your shirt and jacket before we left for school, Damian?</p>
<p>Are you still putting on mismatched socks?</p>
<p>~ bad spoof, not even</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272747"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272747" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>杰姆 (not verified)</span> said:</p>
      <p class="date-time">November 28, 2017</p>
    </div>
    <a href="#comment-272747">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272747" class="permalink" rel="bookmark">很好很高兴</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>很好很高兴</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272767"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272767" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Respectfully yours (not verified)</span> said:</p>
      <p class="date-time">November 30, 2017</p>
    </div>
    <a href="#comment-272767">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272767" class="permalink" rel="bookmark">Hello, can I send cash to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello, can I send cash to the bank address listed in Germany? Will they actually deposit it? Thank you Tor and Tails :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
