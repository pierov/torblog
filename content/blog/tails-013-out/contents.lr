title: Tails 0.13 is out!
---
pub_date: 2012-09-18
---
author: tails
---
tags:

tails
anonymous operating system
---
categories: partners
---
_html_body:

<p>Tails 0.13 brings its lot of small but useful improvements and fixes a few security issues.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download it now.</a></p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li><strong>Use white-list</strong>/principle of least privilege <strong>approach for local services</strong>.<br />
Only users that need a certain local (i.e. hosted on loopback) service<br />
(according to our use cases) are granted access to it by our firewall;<br />
all other users are denied access.</li>
<li>Allow to modify language and layout in the "Advanced options" screen<br />
of the greeter.</li>
<li>Enable four workspaces in the Windows XP camouflage. This allows<br />
users to quickly switch to a more innocent looking workspace in case<br />
they are working on sensitive data and attract unwanted attention.<br />
The workspace switcher applet isn't there, though, since there's no<br />
such thing in Windows XP, so switching is only possible via keyboard<br />
shortcuts.</li>
<li>Claws Mail now saves local/POP emails in its dot-directory by default<br />
instead of the non-persistent <span class="geshifilter"><code class="php geshifilter-php">~<span style="color: #339933;">/</span><a href="http://www.php.net/mail"><span style="color: #990000;">Mail</span></a></code></span> directory. <em>Users who are already<br />
using persistence for Claws will have to perform this change manually.</em></li>
<li>Add support for wireless regulation.</li>
<li>Hide the <em>TailsData</em> partition in desktop applications.</li>
<li><strong>Tor</strong>
<ul>
<li>Upgrade to 0.2.2.39.</li>
</ul>
</li>
<li><strong>Iceweasel</strong>
<ul>
<li>Upgrade iceweasel to 10.0.7esr-2 (Extended Support Release).</li>
</ul>
</li>
<li><strong>Hardware support</strong>
<ul>
<li>Upgrade Linux to 3.2.23-1.</li>
</ul>
</li>
<li><strong>Software</strong>
<ul>
<li>Upgrade I2P to version 0.9.1.</li>
<li>Install GNOME System Monitor.</li>
<li>Upgrade WhisperBack to 1.6, with many UI improvements and new translations.</li>
</ul>
</li>
<li>Ship a <strong>first version of the incremental update system</strong>. Updates are not<br />
currently triggered automatically, but this will allow tests to be done<br />
on larger scales.</li>
</ul>

<p>Plus the usual bunch of minor bug reports and improvements.</p>

<p>See the <a href="http://git.immerda.ch/?p=amnesia.git;a=blob_plain;f=debian/changelog;hb=refs/tags/0.13" rel="nofollow">online Changelog</a> for technical details.</p>

<p>Don't hesitate to <a href="https://tails.boum.org/support/" rel="nofollow">get in touch with us</a>.</p>

