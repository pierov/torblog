title: Tor Weekly News — July 2nd, 2015
---
pub_date: 2015-07-02
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-sixth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Messenger Third Alpha is out</h1>

<p>Sukhbir Singh and Arlo Breault put out a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-June/008991.html" rel="nofollow">third alpha version</a> of Tor Messenger, the Instantbird-based instant messaging client with Tor and Off-the-Record encryption enabled by default.</p>

<p>This release comes with packages for Windows and Mac OS X, as well as 32-bit and 64-bit Linux. Major improvements include the ability to create XMPP accounts in-band (that is, by logging in with the desired account credentials, if the chat server supports this), meaning that users no longer have to create their accounts beforehand over a non-Tor connection; usability improvements to the Off-the-Record extension; an installable Arabic language pack, courtesy of Sherief Alaa (with more languages to follow); and other network- and application-related enhancements.</p>

<p>However, this is still an alpha release: “there may be serious privacy leaks and other issues”, so “please DO NOT recommend Tor Messenger to end users” just yet. If you’d like to test the software out, please see Sukhbir’s announcement for download links and installation instructions, then submit your feedback on Tor’s <a href="https://trac.torproject.org" rel="nofollow">bug tracker</a> with the “Tor Messenger” component, or on the tor-dev mailing list or IRC channel.</p>

<h1>OnionBalance 0.0.1 is out</h1>

<p>Donncha O’Cearbhaill, one of the students participating in the first-ever <a href="https://blog.torproject.org/blog/tor-summer-privacy-projects" rel="nofollow">Tor Summer of Privacy</a>, released the first alpha version of his <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038312.html" rel="nofollow">OnionBalance</a> tool. OnionBalance “provides load-balancing and redundancy for Tor hidden services by distributing client requests to multiple backend Tor instances”; if you run an onion service that handles a large number of client requests, or require automatic failover in the event that some of your hardware fails or is seized, OnionBalance will help to distribute the load evenly and efficiently.</p>

<p>The tool is currently under heavy development, and “there are likely bugs which cause the OnionBalance service to crash or not operate correctly”. “I would very much appreciate any feedback or bug reports. In particular I would like to improve the documentation and make the tool easier for operators to install and run”, writes Donncha. Please  see the release announcement for further information and installation instructions.</p>

<h1>Tor Weekly News turns two</h1>

<p>The <a href="https://lists.torproject.org/pipermail/tor-talk/2013-July/028770.html" rel="nofollow">first issue</a> of Tor Weekly News was sent out on July 3rd, 2013. Since the last anniversary, we’ve reported on many positive developments in the Tor community: work by Facebook security engineers to offer a <a href="https://lists.torproject.org/pipermail/tor-news/2015-February/000084.html" rel="nofollow">Tor onion service for the world’s largest social network</a>; the Library Freedom Project helping American public libraries to <a href="https://lists.torproject.org/pipermail/tor-news/2014-November/000070.html" rel="nofollow">protect their patrons’ right to free expression with Tor</a>; a significant <a href="https://lists.torproject.org/pipermail/tor-news/2015-March/000087.html" rel="nofollow">community-chosen donation by Reddit to the Tor Project</a>; credits for Tor and Tails in an <a href="https://lists.torproject.org/pipermail/tor-news/2015-February/000086.html" rel="nofollow">Academy Award-winning documentary film</a>; and of course the daily software development, research, and innovation that ensure security and anonymity for millions of Internet users around the world. Last month we were even able to lead our hundredth issue with the wonderful news that the United Nations Special Rapporteur on freedom of opinion and expression has endorsed the Tor Project’s work in his <a href="https://lists.torproject.org/pipermail/tor-news/2015-June/000100.html" rel="nofollow">first report to the UN Human Rights Council</a>.</p>

<p>We’re always grateful for help and suggestions; if you’d like to get involved, see the information below for more details. Many thanks to everyone who has helped to write and proofread this newsletter over the past two years. </p>

<h1>Monthly status reports for June 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of June has begun. Damian Johnson released <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000858.html" rel="nofollow">his report</a> first (with an update on Nyx development), followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000860.html" rel="nofollow">Karsten Loesing</a> (on project management and Tor network tools), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000861.html" rel="nofollow">Jacob Appelbaum</a> (on outreach and advocacy), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000863.html" rel="nofollow">David Goulet</a> (on onion service development), and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000864.html" rel="nofollow">Pearl Crescent</a> (on development of Tor Browser and related software).</p>

<p>Mike Perry sent out the report for the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000859.html" rel="nofollow">Tor Browser team</a>.</p>

<h1>Miscellaneous news</h1>

<p>Griffin Boyce offered an <a href="https://lists.torproject.org/pipermail/tor-dev/2015-July/008994.html" rel="nofollow">update</a> on the development status of Stormy, the one-click onion service setup tool: “Right now, the scripts are undergoing third-party testing to identify any obvious bugs before sending them to security auditors”.</p>

<p>Chloe posted details of an <a href="https://chloe.re/2015/06/20/a-month-with-badonions/" rel="nofollow">experiment</a> to detect malicious Tor relays that might be stealing usernames and passwords that are not protected by HTTPS connections.</p>

<p>Juha Nurmi <a href="https://lists.torproject.org/pipermail/tor-talk/2015-June/038295.html" rel="nofollow">warned</a> that an attacker is creating fake onion addresses that resemble those of popular onion services, including ahmia.fi, and using them to interfere with the content of onion pages as clients request them. Another <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038318.html" rel="nofollow">update</a> gives more information about the details of the attack.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

