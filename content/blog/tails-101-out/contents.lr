title: Tails 1.0.1 is out
---
pub_date: 2014-06-10
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.0.1, is out.</p>

<p>All users must <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> as soon as possible: this release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.0/" rel="nofollow">numerous security issues</a>.</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Security fixes
<ul>
<li>Upgrade the web browser to 24.6.0esr-0+tails1~bpo60+1 (Firefox 24.6.0esr + Iceweasel patches + Torbrowser patches).</li>
<li>Install Linux 3.14 from Debian unstable (fixes CVE-2014-3153 and others).</li>
<li>Install openssl from Squeeze LTS (fixes CVE-2014-0076, CVE-2014-0195, CVE-2014-0221, CVE-2014-3470 and CVE-2014-0224).</li>
<li>Install GnuTLS from Squeeze LTS (fixes CVE-2014-3466).</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Upgrade Tor to 0.2.4.22-1~d60.squeeze+1.</li>
<li>Upgrade I2P to 0.9.13-1~deb6u+1.</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<ul>
<li><a href="https://tails.boum.org/support/known_issues/" rel="nofollow">Longstanding</a> known issues.</li>
</ul>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for July 22.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

<p><strong>Support and feedback</strong><br />
For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

