title: Tor Weekly News — November 26th, 2014
---
pub_date: 2014-11-26
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the forty-seventh issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>A new Tor directory authority</h1>

<p>Tor, being free software, can be used by anyone to set up their own anonymity network, as Tom Ritter <a href="https://lists.torproject.org/pipermail/tor-dev/2014-October/007613.html" rel="nofollow">demonstrated last month</a>; but “the Tor network” as we know it today consists of the 6500+ relays voted on by <a href="https://gitweb.torproject.org/tor.git/blob/HEAD:/src/or/config.c#l823" rel="nofollow">nine</a> <a href="https://www.torproject.org/docs/faq#KeyManagement" rel="nofollow">“directory authorities”</a> (or “dirauths”), operated by trusted members of the Tor development team and community.</p>

<p>As Mike Perry, a longtime directory authority operator, wished to retire his machine, “turtles”, without unbalancing the number of authorities producing the <a href="https://metrics.torproject.org/about.html#consensus" rel="nofollow">consensus</a>, a new authority named “<a href="https://en.wikipedia.org/wiki/Longclaw" rel="nofollow">longclaw</a>” was brought online by the autonomous tech collective <a href="https://help.riseup.net/" rel="nofollow">Riseup</a>, which has been offering free and secure methods of communication (most of them now <a href="https://help.riseup.net/en/security/network-security/tor#riseups-tor-hidden-services" rel="nofollow">available as hidden services</a>) since 1999.</p>

<p>Thanks to Riseup for playing this key role in the operation of the Tor network!</p>

<h1>Miscellaneous news</h1>

<p>Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2014-November/004068.html" rel="nofollow">announced</a> the release of Orbot 14.1.3, which includes improved handling of background processes; it builds on the earlier <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2014-November/004036.html" rel="nofollow">14.1.0</a>, which brought with it support for Android 5.0 Lollipop, as well as stability fixes. Orweb was brought up to version 0.7, also introducing support for the new Android release.</p>

<p>George Kadianakis <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007863.html" rel="nofollow">sent out</a> a co-authored draft of a proposal for statistics concerning hidden service activity that relays could collect and publish without harming the anonymity or security of users and hidden services, and which might “be useful to Tor developers and to people who want to understand hidden services and the onionspace better.”</p>

<p>Tom Ritter drafted a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007853.html" rel="nofollow">proposal</a> exploring methods a hidden service operator might use to prove to certificate authorities that they control the service’s private key when requesting SSL certificates.</p>

<p>Karsten Loesing <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007834.html" rel="nofollow">spruced up</a> the documentation on the <a href="https://metrics.torproject.org/" rel="nofollow">Tor Metrics portal</a>, including a handy glossary of <a href="https://metrics.torproject.org/about.html" rel="nofollow">frequently-used Tor-specific terms</a>.</p>

<p>Damian Johnson sketched out a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007831.html" rel="nofollow">roadmap</a> for further development of <a href="https://stem.torproject.org/" rel="nofollow">Stem</a>, the Tor controller library in Python, welcoming “more general ideas on directions to take Stem, the tor-prompt, and this whole space”.</p>

<p>Andrew Lewman <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-November/000781.html" rel="nofollow">reported</a> on his experiments in mirroring the Tor Project website using the Fastly CDN as well as the BitTorrent Sync application.</p>

<p>Following a <a href="https://bugs.torproject.org/13703" rel="nofollow">suggestion</a> that a guide to server hardening should be distributed with the tor software package, Libertas <a href="https://lists.torproject.org/pipermail/tor-relays/2014-November/005846.html" rel="nofollow">drafted</a> a sample document and asked for reviews. “Please share any opinions or contributions you have. This was written in a little more than an hour, so it’s still a work in progress.”</p>

<p>Libertas also <a href="https://lists.torproject.org/pipermail/tor-relays/2014-November/005759.html" rel="nofollow">scanned</a> a large number of currently-running Tor relays to check which ssh access authentication methods their servers supported, finding 2051 relays that still permitted password-based ssh authentication. “Generally, it is far more secure to allow only public key auth. The Ubuntu help pages have a good <a href="https://help.ubuntu.com/community/SSH/OpenSSH/Keys" rel="nofollow">guide</a> on setting up key-based auth”.</p>

<p>SiNA Rabbani <a href="https://lists.torproject.org/pipermail/tor-relays/2014-November/005806.html" rel="nofollow">noted</a> that a large proportion of Tor exit relays are located in Europe, and called for relay operators to consider running nodes with US hosts. “I am not sure if the reason is lack of Tor-friendly ISPs or people are just too freaked out about the summer of Snowden. I think it’s very wrong to assume that EU countries are not part of the world-wide-wiretap, packets are going through a few internet exchanges anyways.”</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-October/000738.html" rel="nofollow">Andy Weber</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-October/000741.html" rel="nofollow">Matt Kraai</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-October/000746.html" rel="nofollow">Alexander Dietrich</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-October/000751.html" rel="nofollow">James Murphy</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-November/000763.html" rel="nofollow">Jesse Victors</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-November/000783.html" rel="nofollow">Lucid Networks</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-November/000784.html" rel="nofollow">mirror-server.de</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-November/000762.html" rel="nofollow">NTU Open Source Society</a>, and <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-November/000764.html" rel="nofollow">Justaguy</a> for running mirrors of the Tor Project’s website and software!</p>

<h1>Tor help desk roundup</h1>

<p>The help desk commonly sees questions from users who get error messages when using Vidalia, the graphical Tor controller. Vidalia is unmaintained and many of its features simply do not work any more, so it has been <a href="https://www.torproject.org/docs/faq.html#WhereDidVidaliaGo" rel="nofollow">deprecated</a>. For web browsing, only the latest version of <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">Tor Browser</a> should be used. If you were trying to use the (now also defunct) Vidalia Bridge or Relay Bundles, documentation for how to set up <a href="https://www.torproject.org/projects/obfsproxy-instructions" rel="nofollow">bridges</a> and regular <a href="https://www.torproject.org/docs/tor-relay-debian" rel="nofollow">relays</a> more effectively without Vidalia can be found on the website.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, Matt Pagan, Roger Dingledine, and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

