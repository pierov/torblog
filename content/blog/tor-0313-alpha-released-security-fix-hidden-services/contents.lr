title: Tor 0.3.1.3-alpha is released, with a security fix for hidden services.
---
pub_date: 2017-06-08
---
author: nickm
---
tags:

alpha
tor releases
---
categories: releases
---
_html_body:

<p>Hello again! This post announces the third alpha in the 0.3.1.x series, which I just released today. There were stable releases too; I'll go over them in the next post.</p>
<p>Tor 0.3.1.3-alpha fixes a pair of bugs that would allow an attacker to remotely crash a hidden service with an assertion failure. Anyone running a hidden service should upgrade to this version, or to some other version with fixes for TROVE-2017-004 and TROVE-2017-005.</p>
<p>Tor 0.3.1.3-alpha also includes fixes for several key management bugs that sometimes made relays unreliable, as well as several other bugfixes described below.</p>
<p>Since this is an alpha release, you can expect more bugs than usual. If you'd rather have a more stable experience, stick to the stable releases.</p>
<p>If you build Tor from source, you can find Tor 0.3.1.2-alpha at the usual place (at the Download page on our website). Otherwise, you'll probably want to wait until packages are available.</p>
<h2>Changes in version 0.3.1.3-alpha - 2017-06-08</h2>
<ul>
<li>Major bugfixes (hidden service, relay, security):
<ul>
<li>Fix a remotely triggerable assertion failure when a hidden service handles a malformed BEGIN cell. Fixes bug <a href="https://bugs.torproject.org/22493">22493</a>, tracked as TROVE-2017-004 and as CVE-2017-0375; bugfix on 0.3.0.1-alpha.</li>
<li>Fix a remotely triggerable assertion failure caused by receiving a BEGIN_DIR cell on a hidden service rendezvous circuit. Fixes bug <a href="https://bugs.torproject.org/22494">22494</a>, tracked as TROVE-2017-005 and CVE-2017-0376; bugfix on 0.2.2.1-alpha.</li>
</ul>
</li>
<li>Major bugfixes (relay, link handshake):
<ul>
<li>When performing the v3 link handshake on a TLS connection, report that we have the x509 certificate that we actually used on that connection, even if we have changed certificates since that connection was first opened. Previously, we would claim to have used our most recent x509 link certificate, which would sometimes make the link handshake fail. Fixes one case of bug <a href="https://bugs.torproject.org/22460">22460</a>; bugfix on 0.2.3.6-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (relays, key management):
<ul>
<li>Regenerate link and authentication certificates whenever the key that signs them changes; also, regenerate link certificates whenever the signed key changes. Previously, these processes were only weakly coupled, and we relays could (for minutes to hours) wind up with an inconsistent set of keys and certificates, which other relays would not accept. Fixes two cases of bug <a href="https://bugs.torproject.org/22460">22460</a>; bugfix on 0.3.0.1-alpha.</li>
<li>When sending an Ed25519 signing-&gt;link certificate in a CERTS cell, send the certificate that matches the x509 certificate that we used on the TLS connection. Previously, there was a race condition if the TLS context rotated after we began the TLS handshake but before we sent the CERTS cell. Fixes a case of bug <a href="https://bugs.torproject.org/22460">22460</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Major bugfixes (torrc, crash):
<ul>
<li>Fix a crash bug when using %include in torrc. Fixes bug <a href="https://bugs.torproject.org/22417">22417</a>; bugfix on 0.3.1.1-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor features (code style):
<ul>
<li>Add "Falls through" comments to our codebase, in order to silence GCC 7's -Wimplicit-fallthrough warnings. Patch from Andreas Stieger. Closes ticket <a href="https://bugs.torproject.org/22446">22446</a>.</li>
</ul>
</li>
<li>Minor features (diagnostic):
<ul>
<li>Add logging messages to try to diagnose a rare bug that seems to generate RSA-&gt;Ed25519 cross-certificates dated in the 1970s. We think this is happening because of incorrect system clocks, but we'd like to know for certain. Diagnostic for bug <a href="https://bugs.torproject.org/22466">22466</a>.</li>
</ul>
</li>
<li>Minor bugfixes (correctness):
<ul>
<li>Avoid undefined behavior when parsing IPv6 entries from the geoip6 file. Fixes bug <a href="https://bugs.torproject.org/22490">22490</a>; bugfix on 0.2.4.6-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory protocol):
<ul>
<li>Check for libzstd &gt;= 1.1, because older versions lack the necessary streaming API. Fixes bug <a href="https://bugs.torproject.org/22413">22413</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (link handshake):
<ul>
<li>Lower the lifetime of the RSA-&gt;Ed25519 cross-certificate to six months, and regenerate it when it is within one month of expiring. Previously, we had generated this certificate at startup with a ten-year lifetime, but that could lead to weird behavior when Tor was started with a grossly inaccurate clock. Mitigates bug <a href="https://bugs.torproject.org/22466">22466</a>; mitigation on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (storage directories):
<ul>
<li>Always check for underflows in the cached storage directory usage. If the usage does underflow, re-calculate it. Also, avoid a separate underflow when the usage is not known. Fixes bug <a href="https://bugs.torproject.org/22424">22424</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (unit tests):
<ul>
<li>The unit tests now pass on systems where localhost is misconfigured to some IPv4 address other than 127.0.0.1. Fixes bug <a href="https://bugs.torproject.org/6298">6298</a>; bugfix on 0.0.9pre2.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Clarify the manpage for the (deprecated) torify script. Closes ticket <a href="https://bugs.torproject.org/6892">6892</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-268952"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268952" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 08, 2017</p>
    </div>
    <a href="#comment-268952">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268952" class="permalink" rel="bookmark">localhost is misconfigured…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>localhost is misconfigured to some IPv4 address other than 127.0.0.1</p></blockquote>
<p>Nice joke!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268960"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268960" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 08, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268952" class="permalink" rel="bookmark">localhost is misconfigured…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-268960">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268960" class="permalink" rel="bookmark">Things can be funny and true…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Things can be funny and true at the same time. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-269520"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269520" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>simon (not verified)</span> said:</p>
      <p class="date-time">June 30, 2017</p>
    </div>
    <a href="#comment-269520">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269520" class="permalink" rel="bookmark">the new update continuously…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the new update continuously crashed my browser, so had to reinstall again...now aking me to restart browser for updates again...dont really want to but no choice. gutted</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269532"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269532" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">June 30, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269520" class="permalink" rel="bookmark">the new update continuously…</a> by <span>simon (not verified)</span></p>
    <a href="#comment-269532">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269532" class="permalink" rel="bookmark">Are you commenting about an…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are you commenting about an issue on the wrong blog post? This is a blog post about little-t tor. It has no GUI and runs in the background for most people's setup. It sounds like you are describing a problem with Tor Browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
