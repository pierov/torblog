title: New Release: Tor 0.3.4.3-alpha
---
pub_date: 2018-06-26
---
author: nickm
---
tags: alpha release
---
categories: releases
---
summary: There's a new alpha release available for download.  If you build Tor from source, you can download the source code for 0.3.4.3-alpha from the usual place on the website.  Packages should be available over the coming weeks, with a new alpha Tor Browser release over the coming weeks.
---
_html_body:

<p>Hi!  There's a new alpha release available for download.  If you build Tor from source, you can download the source code for 0.3.4.3-alpha from the usual place on the website.  Packages should be available over the coming weeks, with a new alpha Tor Browser release over the coming weeks. (There's Tor Browser alpha release planned for tomorrow, but it will probably still have 0.3.4.2-alpha.)</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.3.4.3-alpha fixes several bugs in earlier versions, including one that was causing stability issues on directory authorities.</p>
<h2>Changes in version 0.3.4.3-alpha - 2018-06-26</h2>
<ul>
<li>Major bugfixes (directory authority):
<ul>
<li>Stop leaking memory on directory authorities when planning to vote. This bug was crashing authorities by exhausting their memory. Fixes bug <a href="https://bugs.torproject.org/26435">26435</a>; bugfix on 0.3.3.6.</li>
</ul>
</li>
<li>Major bugfixes (rust, testing):
<ul>
<li>Make sure that failing tests in Rust will actually cause the build to fail: previously, they were ignored. Fixes bug <a href="https://bugs.torproject.org/26258">26258</a>; bugfix on 0.3.3.4-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor feature (directory authorities):
<ul>
<li>Stop warning about incomplete bw lines before the first complete bw line has been found, so that additional header lines can be ignored. Fixes bug <a href="https://bugs.torproject.org/25960">25960</a>; bugfix on 0.2.2.1-alpha</li>
</ul>
</li>
<li>Minor features (relay, diagnostic):
<ul>
<li>Add several checks to detect whether Tor relays are uploading their descriptors without specifying why they regenerated them. Diagnostic for ticket <a href="https://bugs.torproject.org/25686">25686</a>.</li>
</ul>
</li>
<li>Minor features (unit tests):
<ul>
<li>Test complete bandwidth measurements files, and test that incomplete bandwidth lines only give warnings when the end of the header has not been detected. Fixes bug <a href="https://bugs.torproject.org/25947">25947</a>; bugfix on 0.2.2.1-alpha</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Refrain from compiling unit testing related object files when --disable-unittests is set to configure script. Fixes bug <a href="https://bugs.torproject.org/24891">24891</a>; bugfix on 0.2.5.1-alpha.</li>
<li>When linking the libtor_testing.a library, only include the dirauth object files once. Previously, they were getting added twice. Fixes bug <a href="https://bugs.torproject.org/26402">26402</a>; bugfix on 0.3.4.1-alpha.</li>
<li>The --enable-fatal-warnings flag now affects Rust code as well. Closes ticket <a href="https://bugs.torproject.org/26245">26245</a>.</li>
</ul>
</li>
<li>Minor bugfixes (onion services):
<ul>
<li>Recompute some consensus information after detecting a clock jump, or after transitioning from a non-live consensus to a live consensus. We do this to avoid having an outdated state, and miscalculating the index for next-generation onion services. Fixes bug <a href="https://bugs.torproject.org/24977">24977</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Relays now correctly block attempts to re-extend to the previous relay by Ed25519 identity. Previously they would warn in this case, but not actually reject the attempt. Fixes bug <a href="https://bugs.torproject.org/26158">26158</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Fix compilation of the doctests in the Rust crypto crate. Fixes bug <a href="https://bugs.torproject.org/26415">26415</a>; bugfix on 0.3.4.1-alpha.</li>
<li>Instead of trying to read the geoip configuration files from within the unit tests, instead create our own ersatz files with just enough geoip data in the format we expect. Trying to read from the source directory created problems on Windows with mingw, where the build system's paths are not the same as the platform's paths. Fixes bug <a href="https://bugs.torproject.org/25787">25787</a>; bugfix on 0.3.4.1-alpha.</li>
<li>Refrain from trying to get an item from an empty smartlist in test_bridges_clear_bridge_list. Set DEBUG_SMARTLIST in unit tests to catch improper smartlist usage. Furthermore, enable DEBUG_SMARTLIST globally when build is configured with fragile hardening. Fixes bug <a href="https://bugs.torproject.org/26196">26196</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
</ul>

