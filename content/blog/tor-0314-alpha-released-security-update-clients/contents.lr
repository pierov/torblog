title: Tor 0.3.1.4-alpha is released (with security update for clients)
---
pub_date: 2017-06-29
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>Hello again! This post announces the fourth alpha in the 0.3.1.x series, which we just released today. There's a stable release too; I'll mention that in the next post.</p>
<p>Tor 0.3.1.4-alpha fixes a path selection bug that would allow a client to use a guard that was in the same network family as a chosen exit relay. This is a security regression; all clients running earlier versions of 0.3.0.x or 0.3.1.x should upgrade to 0.3.0.9 or 0.3.1.4-alpha.</p>
<p>This release also fixes several other bugs introduced in 0.3.0.x and 0.3.1.x, including others that can affect bandwidth usage and correctness.</p>
<p>Since this is an alpha release, you can expect more bugs than usual. If you'd rather have a more stable experience, stick to the stable releases.</p>
<p>If you build Tor from source, you can find Tor 0.3.1.4-alpha at the usual place (at the Download page on our website). Otherwise, you'll probably want to wait until packages are available. There should be a new Tor Browser release early next week.</p>
<h2>Changes in version 0.3.1.4-alpha - 2017-06-29</h2>
<ul>
<li>New dependencies:
<ul>
<li>To build with zstd and lzma support, Tor now requires the pkg-config tool at build time. (This requirement was new in 0.3.1.1-alpha, but was not noted at the time. Noting it here to close ticket <a href="https://bugs.torproject.org/22623">22623</a>.)</li>
</ul>
</li>
<li>Major bugfixes (path selection, security):
<ul>
<li>When choosing which guard to use for a circuit, avoid the exit's family along with the exit itself. Previously, the new guard selection logic avoided the exit, but did not consider its family. Fixes bug <a href="https://bugs.torproject.org/22753">22753</a>; bugfix on 0.3.0.1-alpha. Tracked as TROVE-2016- 006 and CVE-2017-0377.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (compression, zstd):
<ul>
<li>Correctly detect a full buffer when decompressing a large zstd- compressed input. Previously, we would sometimes treat a full buffer as an error. Fixes bug <a href="https://bugs.torproject.org/22628">22628</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Major bugfixes (directory protocol):
<ul>
<li>Ensure that we send "304 Not modified" as HTTP status code when a client is attempting to fetch a consensus or consensus diff, and the best one we can send them is one they already have. Fixes bug <a href="https://bugs.torproject.org/22702">22702</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Major bugfixes (entry guards):
<ul>
<li>When starting with an old consensus, do not add new entry guards unless the consensus is "reasonably live" (under 1 day old). Fixes one root cause of bug <a href="https://bugs.torproject.org/22400">22400</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor features (bug mitigation, diagnostics, logging):
<ul>
<li>Avoid an assertion failure, and log a better error message, when unable to remove a file from the consensus cache on Windows. Attempts to mitigate and diagnose bug <a href="https://bugs.torproject.org/22752">22752</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the June 8 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor bugfixes (compression):
<ul>
<li>When compressing or decompressing a buffer, check for a failure to create a compression object. Fixes bug <a href="https://bugs.torproject.org/22626">22626</a>; bugfix on 0.3.1.1-alpha.</li>
<li>When decompressing a buffer, check for extra data after the end of the compressed data. Fixes bug <a href="https://bugs.torproject.org/22629">22629</a>; bugfix on 0.3.1.1-alpha.</li>
<li>When decompressing an object received over an anonymous directory connection, if we have already decompressed it using an acceptable compression method, do not reject it for looking like an unacceptable compression method. Fixes part of bug <a href="https://bugs.torproject.org/22670">22670</a>; bugfix on 0.3.1.1-alpha.</li>
<li>When serving directory votes compressed with zlib, do not claim to have compressed them with zstd. Fixes bug <a href="https://bugs.torproject.org/22669">22669</a>; bugfix on 0.3.1.1-alpha.</li>
<li>When spooling compressed data to an output buffer, don't try to spool more data when there is no more data to spool and we are not trying to flush the input. Previously, we would sometimes launch compression requests with nothing to do, which interferes with our 22672 checks. Fixes bug <a href="https://bugs.torproject.org/22719">22719</a>; bugfix on 0.2.0.16-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (defensive programming):
<ul>
<li>Detect and break out of infinite loops in our compression code. We don't think that any such loops exist now, but it's best to be safe. Closes ticket <a href="https://bugs.torproject.org/22672">22672</a>.</li>
<li>Fix a memset() off the end of an array when packing cells. This bug should be harmless in practice, since the corrupted bytes are still in the same structure, and are always padding bytes, ignored, or immediately overwritten, depending on compiler behavior. Nevertheless, because the memset()'s purpose is to make sure that any other cell-handling bugs can't expose bytes to the network, we need to fix it. Fixes bug <a href="https://bugs.torproject.org/22737">22737</a>; bugfix on 0.2.4.11-alpha. Fixes CID 1401591.</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp2 sandbox):
<ul>
<li>Permit the fchmod system call, to avoid crashing on startup when starting with the seccomp2 sandbox and an unexpected set of permissions on the data directory or its contents. Fixes bug <a href="https://bugs.torproject.org/22516">22516</a>; bugfix on 0.2.5.4-alpha.</li>
<li>Fix a crash in the LZMA module, when the sandbox was enabled, and liblzma would allocate more than 16 MB of memory. We solve this by bumping the mprotect() limit in the sandbox module from 16 MB to 20 MB. Fixes bug <a href="https://bugs.torproject.org/22751">22751</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>When decompressing, do not warn if we fail to decompress using a compression method that we merely guessed. Fixes part of bug <a href="https://bugs.torproject.org/22670">22670</a>; bugfix on 0.1.1.14-alpha.</li>
<li>When decompressing, treat mismatch between content-encoding and actual compression type as a protocol warning. Fixes part of bug <a href="https://bugs.torproject.org/22670">22670</a>; bugfix on 0.1.1.9-alpha.</li>
<li>Downgrade "assigned_to_cpuworker failed" message to info-level severity. In every case that can reach it, either a better warning has already been logged, or no warning is warranted. Fixes bug <a href="https://bugs.torproject.org/22356">22356</a>; bugfix on 0.2.6.3-alpha.</li>
<li>Demote a warn that was caused by libevent delays to info if netflow padding is less than 4.5 seconds late, or to notice if it is more (4.5 seconds is the amount of time that a netflow record might be emitted after, if we chose the maximum timeout). Fixes bug <a href="https://bugs.torproject.org/22212">22212</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (process behavior):
<ul>
<li>When exiting because of an error, always exit with a nonzero exit status. Previously, we would fail to report an error in our exit status in cases related to __OwningControllerProcess failure, lockfile contention, and Ed25519 key initialization. Fixes bug <a href="https://bugs.torproject.org/22720">22720</a>; bugfix on versions 0.2.1.6-alpha, 0.2.2.28-beta, and 0.2.7.2-alpha respectively. Reported by "f55jwk4f"; patch from "huyvq".</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Add a manpage description for the key-pinning-journal file. Closes ticket <a href="https://bugs.torproject.org/22347">22347</a>.</li>
<li>Correctly note that bandwidth accounting values are stored in the state file, and the bw_accounting file is now obsolete. Closes ticket <a href="https://bugs.torproject.org/16082">16082</a>.</li>
<li>Document more of the files in the Tor data directory, including cached-extrainfo, secret_onion_key{,_ntor}.old, hidserv-stats, approved-routers, sr-random, and diff-cache. Found while fixing ticket <a href="https://bugs.torproject.org/22347">22347</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-269776"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269776" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 08, 2017</p>
    </div>
    <a href="#comment-269776">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269776" class="permalink" rel="bookmark">[07-08 17:59:58] Torbutton…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[07-08 17:59:58] Torbutton INFO: controlPort &gt;&gt; 650 STREAM 2187 NEW 0 1.1.1.1.$AAA...EEE.exit:53 PURPOSE=DIR_FETCH<br />
[07-08 17:59:58] Torbutton INFO: controlPort &gt;&gt; 650 STREAM 2189 NEW 0 1.1.1.1.$AAA...EEE.exit:53 PURPOSE=DIR_FETCH<br />
[07-08 17:59:58] Torbutton INFO: controlPort &gt;&gt; 650 STREAM 2191 NEW 0 1.1.1.1.$AAA...EEE.exit:53 PURPOSE=DIR_FETCH<br />
Why the same node? Why 3 times in a row? Why STREAM numbers are odd only?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269798"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269798" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">July 10, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269776" class="permalink" rel="bookmark">[07-08 17:59:58] Torbutton…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-269798">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269798" class="permalink" rel="bookmark">Not sure, to be honest. Does…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not sure, to be honest. Does that issue go away with older tor versions? You could test that with extracting the expert bundle (you'll find the respective tor-win32*zip files in the Tor Browser bundles directories at <a href="https://archive.torproject.org/tor-package-archive/torbrowser/" rel="nofollow">https://archive.torproject.org/tor-package-archive/torbrowser/</a>) and copyinf the tor.exe file over the one in the Tor Browser you are using? Maybe we can first narrow down the problem to be either in the tor or the Tor Browser area that way.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269806"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269806" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 11, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-269806">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269806" class="permalink" rel="bookmark">Is this really an issue? Isn…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is this really an issue? Isn't it a Tor issue?<br />
Tor stable 0.3.0.9: three, to the same, even, i.e. STREAM numbers could be odd or even.<br />
The whole algorithm looks like this: STREAM 169 NODE. When DONE, STREAM 172,174,176 NODE. 2 streams (170,171) also disappeared.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269820"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269820" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 13, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269806" class="permalink" rel="bookmark">Is this really an issue? Isn…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-269820">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269820" class="permalink" rel="bookmark">Could somebody from the Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could somebody from the Tor Team explain is this an intended behavior or not?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-269835"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269835" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>liteshield (not verified)</span> said:</p>
      <p class="date-time">July 14, 2017</p>
    </div>
    <a href="#comment-269835">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269835" class="permalink" rel="bookmark">tor_bug_occurred_(): Bug:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>tor_bug_occurred_(): Bug: consdiffmgr.c:1289: store_multiple: Non-fatal assertion !(ent == NULL) failed. (on Tor 0.3.1.4-alpha fab91a290ded3e74)<br />
Bug: Non-fatal assertion !(ent == NULL) failed in store_multiple at consdiffmgr.c:1289. (Stack trace not available) (on Tor 0.3.1.4-alpha fab91a290ded3e74)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-269841"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269841" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 14, 2017</p>
    </div>
    <a href="#comment-269841">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269841" class="permalink" rel="bookmark">Unable to unlink &quot;.\\Data\…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unable to unlink ".\\Data\\Tor\\LocalState\\diff-cache/1001" while removing file: Permission denied<br />
tor_bug_occurred_(): Bug: consdiffmgr.c:1289: store_multiple: Non-fatal assertion !(ent == NULL) failed. (on Tor 0.3.1.4-alpha fab91a290ded3e74)<br />
Bug: Non-fatal assertion !(ent == NULL) failed in store_multiple at consdiffmgr.c:1289. (Stack trace not available) (on Tor 0.3.1.4-alpha fab91a290ded3e74)<br />
tor_bug_occurred_(): Bug: consdiffmgr.c:328: cdm_diff_ht_purge: Non-fatal assertion !((*diff)-&gt;entry == NULL) failed. (on Tor 0.3.1.4-alpha fab91a290ded3e74)<br />
Bug: Non-fatal assertion !((*diff)-&gt;entry == NULL) failed in cdm_diff_ht_purge at consdiffmgr.c:328. (Stack trace not available) (on Tor 0.3.1.4-alpha fab91a290ded3e74)<br />
eventdns: All nameservers have failed</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269860"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269860" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">July 17, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269841" class="permalink" rel="bookmark">Unable to unlink &quot;.\\Data\…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-269860">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269860" class="permalink" rel="bookmark">Thanks for reporting this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for reporting this bug. This is <a href="https://trac.torproject.org/projects/tor/ticket/22752" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/22752</a>. Do you have some more information that could help us tracking this problem down?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-269900"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269900" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Alan Mintaka (not verified)</span> said:</p>
      <p class="date-time">July 20, 2017</p>
    </div>
    <a href="#comment-269900">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269900" class="permalink" rel="bookmark">Plse disregard last request…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Plse disregard last request for help RE Bad Gateway 502 Error.  I added an "Inbound Rule" in Windows Firewall for the TOR executable for firefox.exe, defaulted everything, and that seems to have done the trick for the time being.  Anyone using Windows Firewall might want to check this out.</p>
<p>Also forgot to mention that I'm using Windows 7 "Home Premium."   Have no idea how this would play out in Windows 8 or 10, and don't want to know.</p>
<p>Response is on the slow side, but I kind of expected that for anonymous browsing.  I'll work through the other blog posts and community support to try to resolve that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
