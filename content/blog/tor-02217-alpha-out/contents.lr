title: Tor 0.2.2.17-alpha is out
---
pub_date: 2010-10-04
---
author: phobos
---
tags:

bug fixes
alpha release
bandwidth fixes
best tor ever
refuse unknown exits
---
categories:

network
relays
releases
---
_html_body:

<p>Tor 0.2.2.17-alpha introduces a feature to make it harder for clients to use one-hop circuits (which can put the exit relays at higher risk, plus unbalance the network); fixes a big bug in bandwidth accounting for relays that want to limit their monthly bandwidth use; fixes a big pile of bugs in how clients tolerate temporary network failure; and makes our adaptive circuit build timeout feature (which improves client performance if your network is fast while not breaking things if your network is slow) better handle bad networks.</p>

<p><a href="https://www.torproject.org/download.html.en" rel="nofollow">https://www.torproject.org/download.html.en</a></p>

<p>Packages will be appearing over the next few days.</p>

<p>The original announcement is <a href="http://archives.seul.org/or/talk/Oct-2010/msg00001.html" rel="nofollow">http://archives.seul.org/or/talk/Oct-2010/msg00001.html</a></p>

<blockquote><p>
Changes in version 0.2.2.17-alpha - 2010-09-30<br />
  o Major features:<br />
    - Exit relays now try harder to block exit attempts from unknown<br />
      relays, to make it harder for people to use them as one-hop proxies<br />
      a la tortunnel. Controlled by the refuseunknownexits consensus<br />
      parameter (currently enabled), or you can override it on your<br />
      relay with the RefuseUnknownExits torrc option. Resolves bug 1751.</p>
<p>  o Major bugfixes (0.2.1.x and earlier):<br />
    - Fix a bug in bandwidth accounting that could make us use twice<br />
      the intended bandwidth when our interval start changes due to<br />
      daylight saving time. Now we tolerate skew in stored vs computed<br />
      interval starts: if the start of the period changes by no more than<br />
      50% of the period's duration, we remember bytes that we transferred<br />
      in the old period. Fixes bug 1511; bugfix on 0.0.9pre5.<br />
    - Always search the Windows system directory for system DLLs, and<br />
      nowhere else. Bugfix on 0.1.1.23; fixes bug 1954.<br />
    - When you're using bridges and your network goes away and your<br />
      bridges get marked as down, recover when you attempt a new socks<br />
      connection (if the network is back), rather than waiting up to an<br />
      hour to try fetching new descriptors for your bridges. Bugfix on<br />
      0.2.0.3-alpha; fixes bug 1981.</p>
<p>  o Major bugfixes (on 0.2.2.x):<br />
    - Fix compilation on Windows. Bugfix on 0.2.2.16-alpha; related to<br />
      bug 1797.<br />
    - Fix a segfault that could happen when operating a bridge relay with<br />
      no GeoIP database set. Fixes bug 1964; bugfix on 0.2.2.15-alpha.<br />
    - The consensus bandwidth-weights (used by clients to choose fast<br />
      relays) entered an unexpected edge case in September where<br />
      Exits were much scarcer than Guards, resulting in bad weight<br />
      recommendations. Now we compute them using new constraints that<br />
      should succeed in all cases. Also alter directory authorities to<br />
      not include the bandwidth-weights line if they fail to produce<br />
      valid values. Fixes bug 1952; bugfix on 0.2.2.10-alpha.<br />
    - When weighting bridges during path selection, we used to trust<br />
      the bandwidths they provided in their descriptor, only capping them<br />
      at 10MB/s. This turned out to be problematic for two reasons:<br />
      Bridges could claim to handle a lot more traffic then they<br />
      actually would, thus making more clients pick them and have a<br />
      pretty effective DoS attack. The other issue is that new bridges<br />
      that might not have a good estimate for their bw capacity yet<br />
      would not get used at all unless no other bridges are available<br />
      to a client. Fixes bug 1912; bugfix on 0.2.2.7-alpha.</p>
<p>  o Major bugfixes (on the circuit build timeout feature, 0.2.2.x):<br />
    - Ignore cannibalized circuits when recording circuit build times.<br />
      This should provide for a minor performance improvement for hidden<br />
      service users using 0.2.2.14-alpha, and should remove two spurious<br />
      notice log messages. Bugfix on 0.2.2.14-alpha; fixes bug 1740.<br />
    - Simplify the logic that causes us to decide if the network is<br />
      unavailable for purposes of recording circuit build times. If we<br />
      receive no cells whatsoever for the entire duration of a circuit's<br />
      full measured lifetime, the network is probably down. Also ignore<br />
      one-hop directory fetching circuit timeouts when calculating our<br />
      circuit build times. These changes should hopefully reduce the<br />
      cases where we see ridiculous circuit build timeouts for people<br />
      with spotty wireless connections. Fixes part of bug 1772; bugfix<br />
      on 0.2.2.2-alpha.<br />
    - Prevent the circuit build timeout from becoming larger than<br />
      the maximum build time we have ever seen. Also, prevent the time<br />
      period for measurement circuits from becoming larger than twice that<br />
      value. Fixes the other part of bug 1772; bugfix on 0.2.2.2-alpha.</p>
<p>  o Minor features:<br />
    - When we run out of directory information such that we can't build<br />
      circuits, but then get enough that we can build circuits, log when<br />
      we actually construct a circuit, so the user has a better chance of<br />
      knowing what's going on. Fixes bug 1362.<br />
    - Be more generous with how much bandwidth we'd use up (with<br />
      accounting enabled) before entering "soft hibernation". Previously,<br />
      we'd refuse new connections and circuits once we'd used up 95% of<br />
      our allotment. Now, we use up 95% of our allotment, AND make sure<br />
      that we have no more than 500MB (or 3 hours of expected traffic,<br />
      whichever is lower) remaining before we enter soft hibernation.<br />
    - If we've configured EntryNodes and our network goes away and/or all<br />
      our entrynodes get marked down, optimistically retry them all when<br />
      a new socks application request appears. Fixes bug 1882.<br />
    - Add some more defensive programming for architectures that can't<br />
      handle unaligned integer accesses. We don't know of any actual bugs<br />
      right now, but that's the best time to fix them. Fixes bug 1943.<br />
    - Support line continuations in the torrc config file. If a line<br />
      ends with a single backslash character, the newline is ignored, and<br />
      the configuration value is treated as continuing on the next line.<br />
      Resolves bug 1929.</p>
<p>  o Minor bugfixes (on 0.2.1.x and earlier):<br />
    - For bandwidth accounting, calculate our expected bandwidth rate<br />
      based on the time during which we were active and not in<br />
      soft-hibernation during the last interval. Previously, we were<br />
      also considering the time spent in soft-hibernation. If this<br />
      was a long time, we would wind up underestimating our bandwidth<br />
      by a lot, and skewing our wakeup time towards the start of the<br />
      accounting interval. Fixes bug 1789. Bugfix on 0.0.9pre5.</p>
<p>  o Minor bugfixes (on 0.2.2.x):<br />
    - Resume generating CIRC FAILED REASON=TIMEOUT control port messages,<br />
      which were disabled by the circuit build timeout changes in<br />
      0.2.2.14-alpha. Bugfix on 0.2.2.14-alpha; fixes bug 1739.<br />
    - Make sure we don't warn about missing bandwidth weights when<br />
      choosing bridges or other relays not in the consensus. Bugfix on<br />
      0.2.2.10-alpha; fixes bug 1805.<br />
    - In our logs, do not double-report signatures from unrecognized<br />
      authorities both as "from unknown authority" and "not<br />
      present". Fixes bug 1956, bugfix on 0.2.2.16-alpha.
</p></blockquote>

---
_comments:

<a id="comment-7938"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7938" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 04, 2010</p>
    </div>
    <a href="#comment-7938">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7938" class="permalink" rel="bookmark">Hi phobos!!!!!!!!!!!!!
I&#039;ve</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi phobos!!!!!!!!!!!!!<br />
I've a question!!!!!!!!!!!!!!!!!!!!! I don't understand this!!!!!!!! Tor "<em>introduces a feature to make it harder for clients to use one-hop circuits (which can put the exit relays at higher risk</em>", so, i would like to know!!! one-hop circuits put the exit relays at a higher risk <em>of what</em>?!!!!!!!!!!!!! Because it doesn't make sense to me!!!!!!!!</p>
<p>bye!!!!!!!!<br />
~bee!!!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7943"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7943" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 05, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7938" class="permalink" rel="bookmark">Hi phobos!!!!!!!!!!!!!
I&#039;ve</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7943">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7943" class="permalink" rel="bookmark">Why ? !!!! The need !!!! for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why ? !!!! The need !!!! for all the exclamation marks !!!!!!!</p>
<p>:-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-7950"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7950" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 05, 2010</p>
    </div>
    <a href="#comment-7950">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7950" class="permalink" rel="bookmark">In this version did you fix</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In this version did you fix the 30MB limit bug? If not where can i get older releases of Vidalia+privoxy? Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7975"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7975" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 08, 2010</p>
    </div>
    <a href="#comment-7975">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7975" class="permalink" rel="bookmark">As far as I know this bug is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As far as I know this bug is unrelated to tor. It is a limitation within Polipo, but you can use privoxy instead. It has no limits.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7979"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7979" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 08, 2010</p>
    </div>
    <a href="#comment-7979">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7979" class="permalink" rel="bookmark">Love you guys and appreciate</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Love you guys and appreciate Tor very much but I just need a quick start up guide to get it configured and running good enough for basic troll protection on forums -- I really don't need to wade through all those fascinating geeky tidbits on the wiki front page.</p>
<p>You could call it "click here for the quick start up guide".  LOL  :)</p>
<p>For example, the first time I downloaded Tor/vidalia/firefox, it worked perfectly right out of the box without my doing anything at all (checked my ip with those reverse look up sites)  and browsing the net was only a tiny bit slower than normal.  But then I got the bright idea to disable Tor, and haven't been able to get it running again even after uninstalling Tor/vidalia/firefox and reinstalling.</p>
<p>I don't know where to START, and reading the Wiki novel just to find a simple set of instructions seems redundant.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7982"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7982" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 08, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7979" class="permalink" rel="bookmark">Love you guys and appreciate</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7982">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7982" class="permalink" rel="bookmark">this is why we push the tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>this is why we push the tor browser bundle, it's preconfigured and for 99.99% of people works every time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-7983"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7983" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 08, 2010</p>
    </div>
    <a href="#comment-7983">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7983" class="permalink" rel="bookmark">Oh gosh thanks for replying</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh gosh thanks for replying so quickly.  But I did download the Tor bundle!  lol  It worked perfectly right out of the box without my doing anything at all -- literally.  Which is why I'm so confused why it doesn't work now.</p>
<p>All I did was click on that button in the bottom right screen to disable Tor temporarily, and when I tried to enable it again, it wouldn't work no matter what I did.  </p>
<p>The error message which appears in the firefox window is:</p>
<p>The proxy server is refusing connections<br />
Firefox is configured to use a proxy server that is refusing connections.<br />
*   Check the proxy settings to make sure that they are correct.<br />
*   Contact your network administrator to make sure the proxy server is working.</p>
<p>All I really need to know (I think) is what to type into the Tor preferences screen.  </p>
<p>Anyway, you guys are awesome, seriously.  Hate to waste anyone's time... but a quick start up guide.... please?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7984"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7984" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 08, 2010</p>
    </div>
    <a href="#comment-7984">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7984" class="permalink" rel="bookmark">Tor 0.2.2.17-alpha is much</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor 0.2.2.17-alpha is much faster on my laptop than Tor 0.2.2.15-alpha. Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7990"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7990" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 09, 2010</p>
    </div>
    <a href="#comment-7990">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7990" class="permalink" rel="bookmark">in stable win bundle version</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>in stable win bundle version 0.2.1.26-0.2.10.exe the command<br />
"tor -?" shows v0.2.2.15-alpha!</p>
<p>where is 0.2.1.26?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7994"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7994" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2010</p>
    </div>
    <a href="#comment-7994">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7994" class="permalink" rel="bookmark">Anyone else experiencing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Anyone else experiencing server (as middleman) crashes on Debian?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7998"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7998" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2010</p>
    </div>
    <a href="#comment-7998">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7998" class="permalink" rel="bookmark">Hello,
Just to let you know</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,</p>
<p>Just to let you know that I have just tried using the TOR Browser Bundle with Vista/Firefox 3.6.10 and surprisingly, (this time) it actually worked. However there was no mention that all plugins are disabled so that you cannot watch even a video. It would have saved me a few hours if you had mentioned it upfront. </p>
<p>Even worse than that is the fact that all anonymity is lost should you  ever (gasp) watch a video or use any plugin ! What a joke !<br />
A little flaw in the TOR system if you ask me because it renders it useless. I mean, you have gone to extensive lengths to successfully stay anonymous, and congratulations on that, but one single flash file and it is all rendered useless. </p>
<p>By the way, for people who cannot provide an anonymous system I would not try and be patronising with the :<br />
“Then read "How to ask questions the smart way"<br />
It just make you look even more silly. </p>
<p>Your “contact us” section is just like TOR. You run around in rings getting nowhere. </p>
<p>Ta<br />
Super</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-8143"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8143" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 16, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7998" class="permalink" rel="bookmark">Hello,
Just to let you know</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-8143">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8143" class="permalink" rel="bookmark">The warning is on the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The warning is on the download page in a different color box, with a red warning icon next to it.  Flash is a binary blob that the user cannot control.  It can have access to system resources and do just about whatever it wants, and send data to wherever it wants.  Tor is forced to block it to protect you.  Tor protects you just fine.  Unfortunately operating systems are designed to share your data with anything that asks, regardless of what the user wants it to share.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-8169"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8169" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-8169">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8169" class="permalink" rel="bookmark">You know what. You could</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You know what. You could design a system where Flash is relatively safe to use. And for the user it would be a tool that takes allot more work to utilize if it works (it might work for 75-85% of the users or partly for 95%). I'm not going to pretend any non-free (as in freedom) system is safe though because you are right. You don't have the source code. It isn't. Although if you set it up just right... the flash plug-in wouldn't be able to get your external IP address and would be rendered quite harmless for all practical purposes. What it would be sending over Tor and revealing to any malicious party would be an internal IP address. Totally useless to identify you. It takes allot more work to get right though.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-7999"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7999" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2010</p>
    </div>
    <a href="#comment-7999">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7999" class="permalink" rel="bookmark">Do fix 30MB limit bug as</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do fix 30MB limit bug as soon as possible! Thank's in advance</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8131"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8131" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 13, 2010</p>
    </div>
    <a href="#comment-8131">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8131" class="permalink" rel="bookmark">Does anyone know...
When I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does anyone know...</p>
<p>When I compile on Windows under MinGW I get a Tor executable that is overt 6mb !!!<br />
Is there some optimisation flag I am missing ?<br />
The main developers manage to produce an executable that is only 2mb.</p>
<p>Scratching my head here...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-8142"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8142" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 16, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-8131" class="permalink" rel="bookmark">Does anyone know...
When I</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-8142">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8142" class="permalink" rel="bookmark">We compile in msys on winxp.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We compile in msys on winxp.  what options are you including and have you tried to strip the exe?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-8144"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8144" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 16, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-8144">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8144" class="permalink" rel="bookmark">Hi Phobos, thanks for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Phobos, thanks for getting back on this one.<br />
I hope they are paying you for all this support !</p>
<p>I dont include any options per se on the build.<br />
I effectively issue a make against the code and out pops the EXE.</p>
<p>I dont know how to strip an EXE so I guess mine is fully clothed ;-)</p>
<p>How do I strip it ? That sounds like a potential solution here since its new to me.</p>
<p>The flags I see in the makefile are:<br />
CFLAGS = -g -O2 -Wall -g -O2 -fno-strict-aliasing</p>
<p>Thats all i guess..</p>
<p>Thanks in advance !</p>
<p>Cav Edwards</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-8149"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8149" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 17, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-8144" class="permalink" rel="bookmark">Hi Phobos, thanks for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-8149">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8149" class="permalink" rel="bookmark">in msys, &quot;strip tor.exe&quot;</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>in msys, "strip tor.exe"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-8151"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8151" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 18, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-8151">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8151" class="permalink" rel="bookmark">Fantastic... thats done the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fantastic... thats done the trick !</p>
<p>Many thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-8132"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8132" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 13, 2010</p>
    </div>
    <a href="#comment-8132">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8132" class="permalink" rel="bookmark">OMG! even Tor 0.2.2.17-alpha</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OMG! even Tor 0.2.2.17-alpha  DOESN'T SUPPORT downloading files large than 30MB! JESUS CHRIST you are not able fix this primitive bug?! What are you waiting for? Christmass?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-8140"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8140" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 16, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-8132" class="permalink" rel="bookmark">OMG! even Tor 0.2.2.17-alpha</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-8140">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8140" class="permalink" rel="bookmark">It&#039;s not a tor problem, it&#039;s</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's not a tor problem, it's a polipo issue.  And no, we aren't able to fix it easily because 99% of the time there is no bug to report.  If you can reliably recreate the issue, open a ticket, <a href="https://trac.torproject.org/" rel="nofollow">https://trac.torproject.org/</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-8461"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8461" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 29, 2010</p>
    </div>
    <a href="#comment-8461">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8461" class="permalink" rel="bookmark">Why in Vidalia Bundle and in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why in Vidalia Bundle and in Tor Browser the tor.exe have different sizes?</p>
</div>
  </div>
</article>
<!-- Comment END -->
