title: New Release: Tor Browser 10.5a10 (Windows Only)
---
pub_date: 2021-02-07
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.5
---
categories: applications
---
_html_body:

<p>Tor Browser 10.5a10 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a10/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-10011">latest stable release</a> instead.</p>
<p>This version updates Firefox to 78.7.1esr for Windows. This release includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-06/">security updates</a> to Firefox.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a8 is:</p>
<ul>
<li>Windows
<ul>
<li>Update Firefox to 78.7.1esr</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291115"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291115" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Patricia Jaramillo (not verified)</span> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
    <a href="#comment-291115">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291115" class="permalink" rel="bookmark">Last localization to spanish…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Last localization to spanish is not totally good. It uses a regional and not generic terms or words in some texts, I.e. "Doná" or "Explorá" instead of "Dona" or "Explora".</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291177"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291177" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 17, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291115" class="permalink" rel="bookmark">Last localization to spanish…</a> by <span>Patricia Jaramillo (not verified)</span></p>
    <a href="#comment-291177">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291177" class="permalink" rel="bookmark">Thank you! Please help us!…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you! Please help us! There are more details on <a href="https://community.torproject.org/localization/" rel="nofollow">https://community.torproject.org/localization/</a></p>
<p>For registration, following <a href="https://community.torproject.org/localization/becoming-tor-translator/" rel="nofollow">https://community.torproject.org/localization/becoming-tor-translator/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291125"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291125" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Bobby (not verified)</span> said:</p>
      <p class="date-time">February 10, 2021</p>
    </div>
    <a href="#comment-291125">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291125" class="permalink" rel="bookmark">trees</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>green</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291130"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291130" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>David Gray (not verified)</span> said:</p>
      <p class="date-time">February 11, 2021</p>
    </div>
    <a href="#comment-291130">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291130" class="permalink" rel="bookmark">Thank you for providing this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for providing this service.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291143"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291143" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>shrooming q (not verified)</span> said:</p>
      <p class="date-time">February 12, 2021</p>
    </div>
    <a href="#comment-291143">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291143" class="permalink" rel="bookmark">All good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>All good</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291147"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291147" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Nana (not verified)</span> said:</p>
      <p class="date-time">February 13, 2021</p>
    </div>
    <a href="#comment-291147">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291147" class="permalink" rel="bookmark">good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>good</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291163"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291163" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2021</p>
    </div>
    <a href="#comment-291163">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291163" class="permalink" rel="bookmark">Reddit is blocking Tor for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"503 Service Unavailable" on Reddit for weeks or months. They're blocking Tor.  Twitter sometimes blocks Tor all of a sudden. "Something went wrong" YouTube started doing less sudden blocking redirects to google.com and more captchas. Teddit, Nitter, Invidious, Bibliogram... Front ends like those are sometimes the only way to read or watch them. To reach Reddit, it **is the only way** anymore. <strong>**Can we please have Tor Browser come with Privacy Redirect or something like it??**</strong></p>
<p><a href="https://github.com/SimonBrazell/privacy-redirect" rel="nofollow">https://github.com/SimonBrazell/privacy-redirect</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291266"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291266" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 26, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291163" class="permalink" rel="bookmark">Reddit is blocking Tor for…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291266">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291266" class="permalink" rel="bookmark">Old reddit works fine. Just…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Old reddit works fine. Don't know why the new one is useless. Just add old instead of www on reddit and it should work. Like this old.reddit.com/r/TOR/ opens the tor subreddit</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291192"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291192" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 18, 2021</p>
    </div>
    <a href="#comment-291192">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291192" class="permalink" rel="bookmark">Tor Browser still corrupts…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser still corrupts headers: Invalid header array...<br />
And now it breaks DDG:<br />
We've detected that you have connected over Tor. There appears to be an issue with the Tor Exit Node you are currently using. Please recreate your Tor circuit or restart your Tor browser in order to fix this.</p>
<p>If this error persists, please let us know: <a href="mailto:error-tor@duckduckgo.com" rel="nofollow">error-tor@duckduckgo.com</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291202"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291202" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 23, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291192" class="permalink" rel="bookmark">Tor Browser still corrupts…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291202">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291202" class="permalink" rel="bookmark">Are you certain that message…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are you certain that message from DDG is related to corrupt headers?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291200"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291200" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>super cookie (not verified)</span> said:</p>
      <p class="date-time">February 23, 2021</p>
    </div>
    <a href="#comment-291200">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291200" class="permalink" rel="bookmark">... and what about favicons…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>... and what about favicons super cookies ...<br />
do i reinstall torbrowser always after using?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291203"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291203" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 23, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291200" class="permalink" rel="bookmark">... and what about favicons…</a> by <span>super cookie (not verified)</span></p>
    <a href="#comment-291203">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291203" class="permalink" rel="bookmark">No, Firefox was not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, Firefox was not vulnerable, and Tor Browser doesn't save FavIcons after restarting.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
