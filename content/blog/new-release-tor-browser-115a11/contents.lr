title: New Alpha Release: Tor Browser 11.5a11 (Windows/macOS/Linux)
---
pub_date: 2022-05-20
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5a11 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a11 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a11/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-17/) to Firefox.

Tor Browser 11.5a11 updates Firefox on Windows, macOS, and Linux to 91.9.0esr.

We use the opportunity as well to update various other components of Tor Browser:
- NoScript 11.4.5
- Tor 0.4.7.7
- OpenSSL to 1.1.1o
- Tor Launcher 0.2.35

The full changelog since [Tor Browser 11.5a9](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Windows + OS X + Linux
 - Update Firefox to 91.9.0esr
 - Update NoScript to 11.4.5
 - Update Tor to 0.4.7.7
 - Update OpenSSL to 1.1.1o
 - Update Tor Launcher to 0.2.35
 - [Bug tor-browser#21484](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/21484): Remove or hide "What's New" link from About dialog
 - [Bug tor-browser-build#40482](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40482): Remove smallerrichard builtin bridge
 - [Bug tor-browser#40886](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40886): Amend about:tor on Nightly and Alpha to encourage testing
 - [Bug tor-browser#40887](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40887): Implement amends to torconnect and Connection Settings following 11.5a9
- Build System
 - All Platforms
   - [Bug tor-browser-build#40319](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40319): Add build tag to downloads.json
