title: Tor Weekly News — May 28th, 2015
---
pub_date: 2015-05-27
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-first issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>New faces in the Tor community</h1>

<p>Since the beginning of the year, the Tor Project, Inc. has made two exciting new additions to its core team. Kate Krauss is Tor’s new Director of Communications, working with journalists and activists to make sure information about the Tor Project’s work gets to groups in need and to the public at large. She has two decades’ worth of experience in non-profit communications and outreach, earned at pioneering AIDS advocacy groups ACT UP and the AIDS Policy Project, and has also contributed to projects working against censorship and surveillance.</p>

<p>Isabela Bagueros is Tor’s new Project Manager, coordinating the numerous moving parts of Tor’s research, development, and outreach activities, and ensuring smooth interaction between funders, developers, users, and other communities. An active member of the free software community both in Brazil and internationally, she contributed to the migration of Brazilian government IT systems to free software, and has spent the last four years working on growth and internationalization at Twitter. See the <a href="https://blog.torproject.org/blog/new-project-manager-and-director-communications-tor" rel="nofollow">press release</a> on the Tor blog to learn more about Kate and Isabela, and the experience they bring to Tor.</p>

<p>Another newly-launched collaboration is that between the Tor Project and Sue Gardner, former executive director of the Wikimedia Foundation and award-winning activist for Internet freedom, transparency, and investigative journalism. Sue will be expanding on her recent informal advisory role to help develop a long-term organizational strategy for Tor; this will involve public consultations with members of the Tor Project and the wider Tor community “to develop a plan for making Tor as effective and sustainable as it can be”, as Roger Dingledine wrote on the <a href="https://blog.torproject.org/blog/sue-gardner-and-tor-strategy-project" rel="nofollow">Tor blog</a>. Thanks to <a href="https://firstlook.org/about/" rel="nofollow">First Look Media</a> for making this project possible!</p>

<p>These new relationships are already bearing fruit in many areas of Tor’s day-to-day operation. More new arrivals are expected in the near future, too: while the board of directors handles the process of transitioning to a new Executive Director following <a href="https://blog.torproject.org/blog/roger-dingledine-becomes-interim-executive-director-tor-project" rel="nofollow">the departure of Andrew Lewman</a>, the Tor Project is <a href="https://www.torproject.org/about/jobs-execadmin" rel="nofollow">looking for an Executive Administrator</a> to help meet its administrative and organizational needs in a time of rapid growth and development. If this sounds like something you want to be in on, please see the job description for the full details and instructions for applying.</p>

<h1>Miscellaneous news</h1>

<p>Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2015-May/008868.html" rel="nofollow">explained</a> what the recently-disclosed “<a href="https://weakdh.org/" rel="nofollow">Logjam”/“weakdh</a>” attack against SSL might mean for Tor. In short: not a lot, but you should take it as an opportunity to update Tor and OpenSSL if you haven’t done so already. See Nick’s post for the technical explanation.</p>

<p>Damian Johnson published a <a href="https://stem.torproject.org/tutorials/to_russia_with_love.html#custom-path-selection" rel="nofollow">tutorial</a> covering techniques for fine-grained handling of Tor circuits and streams using the Stem controller library.</p>

<p>Donncha O’Cearbhaill, one of the students in Tor’s first-ever <a href="https://trac.torproject.org/projects/tor/wiki/org/TorSoP" rel="nofollow">Summer of Privacy program</a>, <a href="https://lists.torproject.org/pipermail/tor-talk/2015-May/037966.html" rel="nofollow">introduced</a> his project for the summer — improving the resilience and scalability of Tor onion services — and asked for feedback from onion service operators concerning “the use-cases, priorities and limitations for people who are experiencing the current limitations of the onion service subsystem”.</p>

<p>Arturo Filastò sent out status reports for the OONI team, covering its activities in <a href="https://lists.torproject.org/pipermail/ooni-dev/2015-May/000282.html" rel="nofollow">March</a> and <a href="https://lists.torproject.org/pipermail/ooni-dev/2015-May/000283.html" rel="nofollow">April</a>.</p>

<h1>This week in Tor history</h1>

<p><a href="https://lists.torproject.org/pipermail/tor-news/2014-May/000047.html" rel="nofollow">A year ago this week</a>, Yawning Angel <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006897.html" rel="nofollow">announced</a> the development of obfs4, a next-generation censorship-circumvention system based on ScrambleSuit but using djb crypto, which (since you asked) means “a combination of Curve25519, Elligator2, HMAC-SHA256, XSalsa20/Poly1305 and SipHash-2-4”. The now-mature obfs4 was recently made the default pluggable transport offered to users requesting bridge relay addresses from the <a href="https://bridges.torproject.org" rel="nofollow">BridgeDB service</a>, and has gained <a href="https://metrics.torproject.org/userstats-bridge-transport.html?graph=userstats-bridge-transport&amp;start=2015-02-25&amp;end=2015-05-26&amp;transport=obfs4" rel="nofollow">over a thousand regular users</a> since its inclusion in the stable Tor Browser series this time last month.</p>

<p>Also celebrating its one-year anniversary is Micah Lee’s simple onion service-based filesharing tool, <a href="https://onionshare.org/" rel="nofollow">OnionShare</a>, whose fans include <a href="http://gizmodo.com/meet-onionshare-the-file-sharing-app-the-next-snowden-1597056567" rel="nofollow">national security journalists</a> and <a href="https://twitter.com/torproject/status/601486285639983105" rel="nofollow">unnamed Tor Project members</a>…</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

