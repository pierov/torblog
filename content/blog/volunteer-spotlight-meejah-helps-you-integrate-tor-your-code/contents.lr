title: Volunteer Spotlight: Meejah Helps You Integrate Tor into Your Code
---
pub_date: 2018-02-20
---
author: tommy
---
tags:

volunteer spotlight
privacy
---
summary:

Tor is a labor of love built by a small group of committed individuals such as Meejah who runs Tor command-line tools, helps people integrate Tor into their code, and scans the Tor network for bad relays. 

---
_html_body:

<p> </p>
<p>Tor is a labor of love built by a small group of committed individuals, but we’re lucky to have the support of a dedicated volunteer base who help us make Tor the strongest anonymity tool out there. The <a href="https://blog.torproject.org/aggregation-feed-types/volunteer-spotlight">volunteer spotlight</a> is a regular feature here on the Tor Blog, and today, we’re highlighting Meejah, who runs Tor command-line tools, helps people integrate Tor into their code, and scans the Tor network for bad relays. </p>
<p>Meejah started out at Tor hacking together some basic things with Twisted, an event-based networking library for Python. This led him to <a href="https://blog.torproject.org/volunteer-spotlight-damian-johnson">Damian’s</a> Python libraries to use with Tor, which led him to develop release txtorcon, an implementation of the control-spec for Tor using Twisted.</p>
<p>Meejah has been involved with with programming and open-source communities for over two decades. As the internet grew in size and popularity, he watched in alarm as it became more hostile.</p>
<p>“Seeing what I saw as a really friendly and mostly-helpful online community start to morph into a more hostile environment pushed me towards privacy/security things,” he says. And so he decided to help secure the net.</p>
<p>Privacy, he maintains, isn’t just about secrecy. “It's about empowering individuals to decide what to reveal,” he tells us. “Sometimes I WANT to share a thing with the world, but that should be up to me. Security is kind of a prerequisite for privacy; any kind of privacy control is meaningless if the underlying software can be easily subverted (‘isn't secure’). So this kind of work is probably less ‘exciting’ in some ways, but absolutely vital.”</p>
<p>Looking forward, Meejah would like to see more developers release software that’s both secure and easy to use (that’s something Tor has been hard at work on; we just released a <a href="https://blog.torproject.org/tor-browser-75-released">new Tor Browser</a> with a redesigned launch experience for better usability). </p>
<p>He would also like to continue seeing decentralized systems gain more traction, lessening the control big internet service companies have over information flow. “We definitely need a lot more people to act a lot differently before the internet looks decentralized. I'm trying to help in my own little corners, and volunteering with Tor is part of that.”</p>
<p>We’re grateful for Meejah’s work. Thanks to him, it’s becoming easier for people to onionize the internet of things, securing the web one step at a time.</p>
<h3><b>Join Our Community</b></h3>
<p>Getting involved with Tor is easy: you can help us make the network faster and more decentralized by <a href="https://www.torproject.org/docs/tor-doc-relay.html.en">running a relay</a>, especially if you live in a <a href="https://twitter.com/TorAtlas/status/955675358442983429">part of the world</a> where we don’t have a lot of relays yet. You can read all of our volunteer spotlights <a href="https://blog.torproject.org/aggregation-feed-types/volunteer-spotlight">here</a>.</p>
<p>Tor is a vital tool for protecting privacy and resisting repressive censorship and surveillance. If you want to make a contribution but don’t have the time to volunteer, <a href="http://donate.torproject.org/">your donation</a> helps keep Tor fast, strong, and secure.</p>

---
_comments:

<a id="comment-274024"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274024" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>else (not verified)</span> said:</p>
      <p class="date-time">February 20, 2018</p>
    </div>
    <a href="#comment-274024">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274024" class="permalink" rel="bookmark">as users , we have 2 big…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>as users , we have 2 big problems :<br />
    - find company<br />
    <a href="https://www.torproject.org/download/download.html.en#warning" rel="nofollow">https://www.torproject.org/download/download.html.en#warning</a><br />
    #Want Tor to really work?<br />
    - securely contact our friends /connect to the web/onions<br />
    web-mail/ricochet<br />
    <a href="http://mail2tor2zyjdctd.onion" rel="nofollow">http://mail2tor2zyjdctd.onion</a><br />
    <a href="https://ricochet.im/" rel="nofollow">https://ricochet.im/</a></p>
<p>Secure the net could begin by (one step at a time) :<br />
    - by alerting about fake/false 'onion_secure_service' ,<br />
    - publishing which errors are fatal (testimonies of user who were 'catched/targeted/attacked') ,<br />
    - by updating TorBrowser at a better level of security/anonymity ,<br />
    - by making a pressure , an interview of the independent dev (ricochet is not updated e.g , elude.in is down e.g) _ even if they are not part of the tor project ,<br />
    - by opening new onions (censored contents/taboo in most countries) about euthanasia ; the price of rare flower, tree, animal ; a counter-privacy news (you are under survey ? publish the names, address, photo of your spies) ,<br />
    - by promoting &amp; installing a 'tor service' near the airport, harbor, railways station (or publishing the list of hotel/coffee shop nearest of these places which are running Tor).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274083"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274083" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>K H (not verified)</span> said:</p>
      <p class="date-time">February 24, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274024" class="permalink" rel="bookmark">as users , we have 2 big…</a> by <span>else (not verified)</span></p>
    <a href="#comment-274083">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274083" class="permalink" rel="bookmark">#Want Tor to really work?…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>
#Want Tor to really work?<br />
- securely contact our friends /connect to the web/onions<br />
web-mail/ricochet<br />
<a href="http://mail2tor2zyjdctd.onion" rel="nofollow">http://mail2tor2zyjdctd.onion</a>
</p></blockquote>
<p>Blah, it's MX record is incorrect (MX record must point to hostname, not IP address).</p>
<p><div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">$ host <span style="color: #339933;">-</span>vt mx mail2tor<span style="color: #339933;">.</span>com<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Trying <span style="color: #0000ff;">&quot;mail2tor.com&quot;</span><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">;;</span> <span style="color: #339933;">-&gt;&gt;</span>HEADER<span style="color: #339933;">&lt;&lt;-</span> opcode<span style="color: #339933;">:</span> QUERY<span style="color: #339933;">,</span> status<span style="color: #339933;">:</span> NOERROR<span style="color: #339933;">,</span> id<span style="color: #339933;">:</span> <span style="color: #cc66cc;">41355</span><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">;;</span> flags<span style="color: #339933;">:</span> qr rd ra<span style="color: #339933;">;</span> QUERY<span style="color: #339933;">:</span> <span style="color: #cc66cc;">1</span><span style="color: #339933;">,</span> ANSWER<span style="color: #339933;">:</span> <span style="color: #cc66cc;">1</span><span style="color: #339933;">,</span> AUTHORITY<span style="color: #339933;">:</span> <span style="color: #cc66cc;">0</span><span style="color: #339933;">,</span> ADDITIONAL<span style="color: #339933;">:</span> <span style="color: #cc66cc;">0</span><span style="color: #339933;">&lt;/</span>p<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>p<span style="color: #339933;">&gt;;;</span> QUESTION SECTION<span style="color: #339933;">:&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">;</span>mail2tor<span style="color: #339933;">.</span>com<span style="color: #339933;">.</span>          IN  MX<span style="color: #339933;">&lt;/</span>p<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>p<span style="color: #339933;">&gt;;;</span> ANSWER SECTION<span style="color: #339933;">:&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">mail2tor<span style="color: #339933;">.</span>com<span style="color: #339933;">.</span>      <span style="color: #cc66cc;">140</span>    IN  MX  <span style="color: #cc66cc;">10</span> 91<span style="color: #339933;">.</span>234<span style="color: #339933;">.</span>99<span style="color: #339933;">.</span>184<span style="color: #339933;">.&lt;/</span>p<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>p<span style="color: #339933;">&gt;</span>Received <span style="color: #cc66cc;">59</span> bytes from 127<span style="color: #339933;">.</span>0<span style="color: #339933;">.</span>1<span style="color: #339933;">.</span>1<span style="color: #666666; font-style: italic;">#53 in 15 ms&lt;br /&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">$<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li></ol></pre></div></p>
<p>Registration page <a href="http://mail2tor2zyjdctd.onion/register.php" rel="nofollow">http://mail2tor2zyjdctd.onion/register.php</a> indicates that mail domain is  @mail2tor.com</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274026"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274026" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 20, 2018</p>
    </div>
    <a href="#comment-274026">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274026" class="permalink" rel="bookmark">&gt; Privacy, he maintains, isn…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Privacy, he maintains, isn’t just about secrecy. “It's about empowering individuals to decide what to reveal,” he tells us. “Sometimes I WANT to share a thing with the world, but that should be up to me. Security is kind of a prerequisite for privacy; any kind of privacy control is meaningless if the underlying software can be easily subverted (‘isn't secure’). So this kind of work is probably less ‘exciting’ in some ways, but absolutely vital.”</p>
<p>Exactly.  This is one of those important points which is often overlooking: strong civilian encryption makes sharing selected information safer.  For example, I might want to be able to vote by establishing my eligibility without revealing my name/address, so that I need not fear retaliation for voting against an incumbent authoritarian, say.  I might want to send money to a worthy cause by proving I have enough to pay without revealing my name/address to government spooks or other nosies.  I might want to log into a social media site using an established identity not tied to any name/address in any (neccessarily leaky) database.</p>
<p>Further, encryption can help restore one of the most important facts which ought to be self-evident: the proper owner of data about a person is that person.  Not a government, not a bank, not a politician, not Facebook or Google.</p>
<p>@ Meejah and all coders for justice: keep up the good work!  And stay safe: We the People need you more than ever.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274038"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274038" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  meejah
  </article>
    <div class="comment-header">
      <p class="comment__submitted">meejah said:</p>
      <p class="date-time">February 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274026" class="permalink" rel="bookmark">&gt; Privacy, he maintains, isn…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274038">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274038" class="permalink" rel="bookmark">Thanks!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274050"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274050" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to meejah</p>
    <a href="#comment-274050">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274050" class="permalink" rel="bookmark">&gt; scans the Tor network for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; scans the Tor network for bad relays</p>
<p>Question (about exit nodes): while reading an https news site, I clicked on another story and unexpectedly, after a delay, saw the "encrypted connection failed" warning from Tor Browser.  Should I be concerned that the exit browser was misbehaving?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274302"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274302" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  meejah
  </article>
    <div class="comment-header">
      <p class="comment__submitted">meejah said:</p>
      <p class="date-time">March 13, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274050" class="permalink" rel="bookmark">&gt; scans the Tor network for…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274302">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274302" class="permalink" rel="bookmark">You should always honour the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You should always honour the warnings about TLS certificate failures from your browser.</p>
<p>There are many things that can cause such failures, and yes a misbehaving exit node is one of them. If you believe it's a particular exit which causes the behavior you can report it to the bad-relays at torproject.org mailing list and someone will take a look.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-274035"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274035" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 21, 2018</p>
    </div>
    <a href="#comment-274035">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274035" class="permalink" rel="bookmark">Thanks #meejah for all of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks #meejah for all of your work! ~ signed: your random tor user :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274039"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274039" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  meejah
  </article>
    <div class="comment-header">
      <p class="comment__submitted">meejah said:</p>
      <p class="date-time">February 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274035" class="permalink" rel="bookmark">Thanks #meejah for all of…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274039">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274039" class="permalink" rel="bookmark">Thank you! :)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you! :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274040"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274040" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>TorLover (not verified)</span> said:</p>
      <p class="date-time">February 21, 2018</p>
    </div>
    <a href="#comment-274040">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274040" class="permalink" rel="bookmark">I know it’s hard. Even…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I know it’s hard. Even people are not interesting in privacy and freedom, I only can say: Thank you meejah! Keep pushing. We love c0d3r like you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274052"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274052" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274040" class="permalink" rel="bookmark">I know it’s hard. Even…</a> by <span>TorLover (not verified)</span></p>
    <a href="#comment-274052">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274052" class="permalink" rel="bookmark">unfortunately , you are…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>unfortunately , you are right ; few people are interested/concerned in privacy &amp; freedom : it must be a luxury ring that one can offered when one becomes famous, when one does not need it ...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274303"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274303" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  meejah
  </article>
    <div class="comment-header">
      <p class="comment__submitted">meejah said:</p>
      <p class="date-time">March 13, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274040" class="permalink" rel="bookmark">I know it’s hard. Even…</a> by <span>TorLover (not verified)</span></p>
    <a href="#comment-274303">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274303" class="permalink" rel="bookmark">thanks :)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274057"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274057" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>code (not verified)</span> said:</p>
      <p class="date-time">February 23, 2018</p>
    </div>
    <a href="#comment-274057">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274057" class="permalink" rel="bookmark">backdoor : i use the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>backdoor : i use the official stable linux version 7.0.5 _x64</p>
<p><a href="https://www.wired.com/2013/09/nsa-backdoor/" rel="nofollow">https://www.wired.com/2013/09/nsa-backdoor/</a><br />
<a href="https://en.wikipedia.org/wiki/NSAKEY" rel="nofollow">https://en.wikipedia.org/wiki/NSAKEY</a></p>
<p>1995 : NSA &amp; backdoor : see Swiss company Crypto AG<br />
1999 : Windows NT &amp; _NSAKEY<br />
2001 : Backdoors and Key Escrow<br />
<a href="https://www.rossde.com/PGP/pgp_backdoor.html" rel="nofollow">https://www.rossde.com/PGP/pgp_backdoor.html</a><br />
2004/2005 standard &amp; FIPS in 2006<br />
2006 : random number generator &amp; private key<br />
2007 : an encryption standard compromised by design<br />
2010 : google/gmail<br />
<a href="https://www.cnn.com/2010/OPINION/01/23/schneier.google.hacking/index.html" rel="nofollow">https://www.cnn.com/2010/OPINION/01/23/schneier.google.hacking/index.ht…</a><br />
2012 : cisco<br />
2013 : Dual_EC_DRBG algorithm was indeed a backdoor (NIST)</p>
<p>2013 : <a href="https://www.eff.org/deeplinks/2013/05/caleatwo" rel="nofollow">https://www.eff.org/deeplinks/2013/05/caleatwo</a></p>
<p>2013 : <a href="https://www.csis.org/analysis/backdoors-and-encryption" rel="nofollow">https://www.csis.org/analysis/backdoors-and-encryption</a><br />
2015 u.k : <a href="https://www.extremetech.com/internet/217798-uk-law-mandates-software-backdoors-jail-for-disclosing-vulnerability" rel="nofollow">https://www.extremetech.com/internet/217798-uk-law-mandates-software-ba…</a><br />
2015 : china : <a href="https://thehackernews.com/2015/02/iphone-china-backdoor.html" rel="nofollow">https://thehackernews.com/2015/02/iphone-china-backdoor.html</a><br />
2015 usa : <a href="https://www.theguardian.com/technology/2015/jul/08/fbi-chief-backdoor-access-encryption-isis" rel="nofollow">https://www.theguardian.com/technology/2015/jul/08/fbi-chief-backdoor-a…</a><br />
2016 : Back Door in Some U.S. Phones<br />
2016 : iphone<br />
<a href="https://www.dailydot.com/layer8/what-is-all-writs-act/" rel="nofollow">https://www.dailydot.com/layer8/what-is-all-writs-act/</a><br />
2016 : We will never backdoor our software<br />
<a href="https://iicybersecurity.wordpress.com/2016/03/22/tor-project-says-it-can-quickly-catch-spying-code/" rel="nofollow">https://iicybersecurity.wordpress.com/2016/03/22/tor-project-says-it-ca…</a><br />
2016 : yahoo<br />
<a href="https://www.reuters.com/article/us-yahoo-nsa-exclusive/yahoo-secretly-scanned-customer-emails-for-u-s-intelligence-sources-idUSKCN1241YT" rel="nofollow">https://www.reuters.com/article/us-yahoo-nsa-exclusive/yahoo-secretly-s…</a><br />
2016 : china<br />
<a href="https://www.nytimes.com/2016/11/16/us/politics/china-phones-software-security.html" rel="nofollow">https://www.nytimes.com/2016/11/16/us/politics/china-phones-software-se…</a><br />
2016 : art 41<br />
<a href="https://www.techworm.net/2016/05/tor-vpn-users-labeled-criminals-hacked-spied-fbi-new-law.html" rel="nofollow">https://www.techworm.net/2016/05/tor-vpn-users-labeled-criminals-hacked…</a><br />
2017 germany : <a href="https://yro.slashdot.org/story/17/12/05/1852249/germany-preparing-law-for-backdoors-in-any-type-of-modern-device" rel="nofollow">https://yro.slashdot.org/story/17/12/05/1852249/germany-preparing-law-f…</a></p>
<p>2017 : Nation-State Hacking<br />
<a href="https://www.eff.org/deeplinks/2017/12/2017-year-nation-state-hacking" rel="nofollow">https://www.eff.org/deeplinks/2017/12/2017-year-nation-state-hacking</a><br />
2017 : NSA<br />
<a href="https://truepundit.com/exclusive-six-u-s-agencies-conspired-to-illegally-wiretap-trump-british-intel-used-as-front-to-spy-on-campaign-for-nsa/" rel="nofollow">https://truepundit.com/exclusive-six-u-s-agencies-conspired-to-illegall…</a><br />
2017 : tor<br />
<a href="https://www.reddit.com/r/privacy/comments/5quzco/is_tor_legal_in_the_usa/" rel="nofollow">https://www.reddit.com/r/privacy/comments/5quzco/is_tor_legal_in_the_us…</a><br />
2017 : tor<br />
<a href="https://www.securityweek.com/bug-tor-browser-exposed-ip-addresses-macos-and-linux-users" rel="nofollow">https://www.securityweek.com/bug-tor-browser-exposed-ip-addresses-macos…</a><br />
2017 : tor version 7.09<br />
<a href="https://www.helpnetsecurity.com/2017/11/06/tor-browser-ip-leak/" rel="nofollow">https://www.helpnetsecurity.com/2017/11/06/tor-browser-ip-leak/</a><br />
2018 : encryption is free expression.<br />
<a href="https://www.eff.org/deeplinks/2018/02/new-national-academy-sciences-report-encryption-asks-wrong-questions" rel="nofollow">https://www.eff.org/deeplinks/2018/02/new-national-academy-sciences-rep…</a><br />
2018 : Malware Espionage Campaign<br />
<a href="https://www.eff.org/press/releases/eff-and-lookout-uncover-new-malware-espionage-campaign-infecting-thousands-around" rel="nofollow">https://www.eff.org/press/releases/eff-and-lookout-uncover-new-malware-…</a><br />
2018 : apple<br />
<a href="https://www.wsws.org/en/articles/2016/02/18/appl-f18.html" rel="nofollow">https://www.wsws.org/en/articles/2016/02/18/appl-f18.html</a></p>
<p>The laws in the u.s.a apply on the service providing access &amp; security (e.g CGHQ runs as a nsa service) everywhere a u.s product is used / an agreement was signed /a joint-venture is made.</p>
<p>Tor is vulnerable until the version 7.0.9</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274114"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274114" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">February 26, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274057" class="permalink" rel="bookmark">backdoor : i use the…</a> by <span>code (not verified)</span></p>
    <a href="#comment-274114">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274114" class="permalink" rel="bookmark">There is no official version…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is no official and up-to-date version 7.0.5 right now. That one is severely outdated and contains unfixed security vulnerabilities. Please use Tor Browser 7.5.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274136"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274136" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 26, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274057" class="permalink" rel="bookmark">backdoor : i use the…</a> by <span>code (not verified)</span></p>
    <a href="#comment-274136">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274136" class="permalink" rel="bookmark">Interesting collection of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Interesting collection of links, but you didn't say what you think it the point.  I'd suggest that the takeaway is this:</p>
<p>o the technology of cyberwar currently favors the attacker; Tor is one of the few tools available to defenders,<br />
o We the People are in an arms race with all the government and hate group attackers</p>
<p>Crypto AG: for the benefit of readers who don't know the story: Crypto Aktiengesellschaft was the cyper machine company founded by Boris Hagelin in 1959 in Zug, CH.  This company sold improved versions of a cypher machine originally invented by Arvid Damm in 1916 (and later modified by Hagelin) to many corporations and governments.  The device was widely used before digital computers enabled digital encryption systems.</p>
<p>In 1995 it was revealed that NSA had throughly broken the Crypto AG cypher machines and had been reading diplomatic traffic among "small nations" for years.</p>
<p>As mentioned recently in this blog, Meltdown/Spectre attacks share in common with cryptanalysis a key feature: the fact that a successful attack has occurred is invisible to the legitimate user.  This point has too often been forgotten after NSA largely gave up on cryptanalysis in favor of evading (strong) cryptographic protections entirely by attacking (weak) computer network defenses.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-275026"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275026" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="dfsljkkfsjdiowercnmcxlk">dfsljkkfsjdiow… (not verified)</span> said:</p>
      <p class="date-time">April 25, 2018</p>
    </div>
    <a href="#comment-275026">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275026" class="permalink" rel="bookmark">Those people look like they…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Those people look like they are in a stomach.</p>
</div>
  </div>
</article>
<!-- Comment END -->
