title: Hack With Us in Mexico City / Hackeá con Tor en México
---
pub_date: 2018-09-07
---
author: tommy
---
tags: dev meeting
---
categories: community
---
summary: At our upcoming Tor meeting in Mexico City, we're having two open hack days that everyone is welcome to join.
---
_html_body:

<p><em>Original Photo By Edmund Garman (Flickr: Mexico City) [CC BY 2.0], via Wikimedia Commons</em></p>
<p><em>English version below. </em></p>
<div id="magicdomid4">
<p>A finales de septiembre, personas que colaboran con Tor en todo el mundo se reunirán en la Ciudad de México por uno de nuestros encuentros bianuales. Hablaremos sobre el futuro de Tor como organización y decidiremos en qué protocolos y características enfocaremos nuestros esfuerzos. Es una oportunidad para que los equipos distribuidos en Tor pasen tiempo juntos y definan en qué trabajar en los próximos meses y años.</p>
</div>
<div id="magicdomid6">Como parte de este encuentro, también tendremos dos días de hackatón abierto para que todos pueden sumarse. Los días de puertas abiertas durante el encuentro en Ciudad de México serán el martes 2 de Octubre y el miércoles 3 de Octubre en el<span> <a href="http://www.sheratonmexicocitymariaisabel.com/">Sheraton María Isabel</a>. Para obtener más información, <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2018MexicoCity">consultá la wiki de la reunión</a>.</span></div>
<h2><span>¿Qué son los días abiertos?</span></h2>
<div id="magicdomid10">
<p><span>Estos días son oportunidades para desarrolladores y personas sin experiencia técnica para venir y pasar el rato con algunos miembros del equipo de Tor. Aquí se puede obtener más información acerca de Tor, hackear algo con nosotros o simplemente conocer a gente interesante que también esté interesada en el software libre y de código abierto, la privacidad en línea y la libertad de expresión.</span></p>
<p><span>Estos días están abiertos a todos, independientemente de su nivel de habilidad técnica. Si estás interesado en contribuir con Tor </span>(ya sea <a href="https://www.torproject.org/getinvolved/volunteer.html">como voluntario</a> o por un <a href="https://www.torproject.org/about/jobs">puesto pagado</a>), este es también un buen momento para aprender más sobre lo que estamos haciendo y unirte a nuestra comunidad.</p>
</div>
<div id="magicdomid14"><span>Este año, tendremos varias sesiones diseñadas para los recién llegados. Estas incluyen Introducción a Tor para principiantes (en inglés y en español) y "State of Onion" una charla sobre dónde estamos y hacia dónde vamos. Estaremos desarrollando el cronograma hasta los mismos días del evento, <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2018MexicoCity/PublicDays">pero puedes ver todo lo que tenemos planeado hasta ahora</a>.</span></div>
<h2 id="magicdomid16"><span>¡Ven a contribuir con Tor!</span></h2>
<div id="magicdomid18"><span>Los días de puertas abiertas durante la </span>encuentro<span> de Tor, al igual que todos los eventos de Tor, se ejecutan según <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2017Amsterdam/ParticipantGuidelines">las pautas</a> y el <a href="https://gitweb.torproject.org/community/policies.git/tree/code_of_conduct.txt">código de conducta</a> de nuestros participantes.  ¡Traé tus ideas y preguntas, vamos a juntarnos y hackear! La mayoría de las sesiones se realizarán en inglés, pero habrá algunas en español. Recuerda que habrá oradores de múltiples idiomas asistiendo.</span></div>
<h2 id="magicdomid20"><span>¿Que más sucede?</span></h2>
<h3 id="magicdomid23"><span>Relay Meetup</span></h3>
<div><span>2 de octubre, 6:30 p.m. Sheraton María Isabel. En inglés.</span></div>
<div id="magicdomid24"> </div>
<div id="magicdomid25"><span>La red Tor está compuesta por miles de voluntarios que dan su tiempo y ancho de banda para hacer del mundo un lugar mejor ejecutando nodos o relays. Los días de puertas abiertas de esta reunión incluyen una reunión de operadores de relays organizada por Colin Childs, aka Phoul en el IRC.</span></div>
<div id="magicdomid26"> </div>
<div id="magicdomid27"><span>Vení y conocé a otros operadores de relays y escuchá las últimas noticias sobre Tor: tendremos camisetas y calcomanías para equipar sus dispositivos. Si no maneja un nodo o <a href="https://trac.torproject.org/projects/tor/wiki/TorRelayGuide">relay</a> pero le gustaría saber más, habrá muchas personas en la reunión dispuestas a ayudarlo.</span></div>
<div id="magicdomid28"> </div>
<h3 id="magicdomid29"><span>Coloquio <a href="https://priv-anon.unam.mx/">"Mecanismos de Privacidad y Anonimato en Redes"</a></span></h3>
<div id="magicdomid30"><span>4-5 de octubre, de 10 a.m. a 6 p.m. Auditorio Sotero Prieto, Facultad de Ingeniería, en la Universidad Nacional Autónoma de México (UNAM). Este evento se llevará a cabo en español e inglés.</span></div>
<div id="magicdomid31"> </div>
<div id="magicdomid32"><span>Después de la reunión, se llevará a cabo un coloquio centrado en la privacidad y la seguridad en la UNAM. El evento está siendo coordinado por gwolf. Haga clic aquí para obtener información <a href="https://osm.org/go/S8c~0Dwg1-?m=">del mapa</a> y <a href="https://priv-anon.unam.mx/">programa</a>.</span></div>
<div id="magicdomid33"> </div>
<h3 id="magicdomid34"><span>Tor Meetup Feminista</span></h3>
<p><img alt="Tormenta: Tor Meetup feminista in Mexico City" src="/static/images/blog/inline-images/cartel-tormenta_0.png" class="align-center" /></p>
<div id="magicdomid35"><span>Tormenta: diálogos feministas para las libertades y autocuidados digitales</span></div>
<div id="magicdomid36"><span>4 de octubre, de 4 a 8 p.m. Facultad de Ingeniería, UNAM. Sala de videoconferencia, en el sótano del Centro de Ingeniería Avanzada. En Español.</span></div>
<div> </div>
<div id="magicdomid37"><span>Invitación a un encuentro de activistas feministas de México a las mujeres y personas no binarias de la comunidad de Tor, para conversar y compartir experiencias sobre el uso de internet, y el desarrollo de herramientas técnicas para el anonimato. El objetivo de este encuentro es reconocer el trabajo y las apuestas que están detrás de algunas de las herramientas de protección digital que utilizamos todos los días. Será un encuentro informal de entrada libre, abierta y gratuita.</span></div>
<div> </div>
<hr />
<p>At the end of September, Tor folks from around the world will convene in Mexico City for one of our biannual meetings. We’ll discuss the future of Tor as an organization and decide what protocols and features to focus our efforts on. It’s a chance for the various distributed teams at Tor to spend some time face-to-face and figure out what to work on in the coming months and years.</p>
<p>As part of this meeting, we’re also having two open hack days everyone is welcome to join. The open days for the Mexico dev meeting will be Tuesday, October 2, and Wednesday, October 3 at the <a href="http://www.sheratonmexicocitymariaisabel.com/">Sheraton María Isabel</a>. For more information, check out the <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2018MexicoCity">meeting wiki</a>.</p>
<h2>What are Open Days?</h2>
<p>These days are opportunities for developers and non-technical folks alike to come and hang out with some of the Tor team. You can learn more about Tor, hack on something with us, or just meet some cool folks who are also interested in free and open source software, online privacy, and free expression.</p>
<p>These days are open to everyone, irrespective of your level of technical skill. If you’re interested in contributing to Tor (either by <a href="https://www.torproject.org/getinvolved/volunteer.html.en">volunteering</a> or through a <a href="https://www.torproject.org/about/jobs">paid position</a>), this is also a great time to learn more about what we’re up to and join our community.</p>
<p>This year, we’ll have several sessions designed for newcomers. These will include introductions to Tor software for beginners (in English and Spanish/en ingles y español) and a “State of the Onion” talk on where we are and where we’re going. We’ll be developing the schedule for the days right up until the days themselves, but you can check out <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2018MexicoCity/PublicDays">what we have planned so far</a>.</p>
<h2>Come contribute to Tor!</h2>
<p>The Tor meeting’s open days, like all Tor events, run on our <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2017Amsterdam/ParticipantGuidelines">participant guidelines</a> and <a href="https://gitweb.torproject.org/community/policies.git/tree/code_of_conduct.txt">code of conduct</a>. So, bring your ideas and questions, and we look forward to hanging and hacking with you! Most sessions will be conducted in English, but some will be in Spanish. There will be speakers of multiple languages attending.</p>
<h2>What else is on</h2>
<h3>Relay Meetup</h3>
<p>Oct. 2, 6:30pm. Sheraton María Isabel. This event will be held in English.</p>
<p>The Tor network is made up of thousands of volunteers who give their time and bandwidth to make the world a better place by running relays. The open days of this meeting include a relay operator meetup organized by Colin Childs, aka Phoul on IRC.</p>
<p>Come along and meet other relay operators and hear the latest news from Tor — we’ll have T-shirts and stickers to kit out your devices. If you don’t <a href="https://trac.torproject.org/projects/tor/wiki/TorRelayGuide">run a relay</a> but would like to know more, there will be lots of people at the meetup happy to help you.</p>
<h3><a href="https://priv-anon.unam.mx/">Privacy and Anonymity</a> Colloquium</h3>
<p>October 4-5, 10am-6pm. Auditorio Sotero Prieto, Facultad de Ingeniería, in the Universidad Nacional Autónoma de México (UNAM). This event will be held in Spanish and English.</p>
<p>After the meeting, a colloquium focused on privacy and security will take place at UNAM. The event is being coordinated by gwolf. <a href="https://osm.org/go/S8c~0Dwg1-?m=">Map</a> and <a href="https://priv-anon.unam.mx/">Schedule</a>.</p>
<h3>Storm: feminist dialogues for digital liberties and self-care strategies</h3>
<p>Oct. 4th, 4-8pm.  Faculty of Engineering, UNAM.  Video conference room, in the basement of the Advanced Engineering Center. This event will be held in Spanish.</p>
<p>An open invitation to a gathering of feminist activists from Mexico and women and non-binary members of the Tor community to chat and share experiences regarding the internet and the development of technical tools for anonymity. The objective of this meeting is to recognize the work and collaborations behind some of the digital protection tools we use everyday. It’s going to be an informal event, with free and open entrance for everyone.</p>
<p>See you there.</p>

---
_comments:

<a id="comment-277067"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277067" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 09, 2018</p>
    </div>
    <a href="#comment-277067">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277067" class="permalink" rel="bookmark">At a long-range planning…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>At a long-range planning meetup, thinking "outside the box" is particularly important.</p>
<p>One project which I think is challenging but doable would be developing anti-stylometry software which can be easily used with various applications such as gedit, which is already incorporated in Tails, which is currently probably the general purpose amnesiac OS for resistance by ordinary citizens.</p>
<p>The situation is currently so awful that even a beta tool would be invaluable.</p>
<p>The field of stylometry, also known as "authorship identification", embraces techniques which can be used to identify authors of anonymous or pseudonymous documents (such as social media posts via Tor or Op Eds in the stubbornly-refusing-to-fail NYT).  DARPA and IARPA fund research into this, but of course their true interest is in improving their own methods in order to harm us.</p>
<p>Thinking about the problem in terms of information theory is essential.  This also helps to bring to the forefront many useful analogies with the problems of hindering cryptanalysis, traffic analysis, and Tor circuit de-anonymization.</p>
<p>The bad news is that stylometric analysis can exploit many types of clues, such as variations in vocabulary, grammatical structure, punctuation, plus colloquialisms, regionalisms, acronyms, characteristic misspellings and capitalizations, as well as semantics and mood.  Even variant versions of "well known" quotations or references to popular memes can offer potentially dangerous clues. The fact that any definable feature whatsoever can acquire a probabilistic characteristic over time and space makes comprehensive anti-stylometry quite a challenge.  </p>
<p>But there is some good news too.  First, natural language is so flexible that the stylometrists encounter difficulties in robotically reducing texts into a form suitable for their analysis, so stylometry is not quite as easy as DARPA propagandists claim.  Second, some types of clues are easily blunted, and anything which substantially increases the entropy of the targeted text (increases the difficulty of picking an individual candidate out of the crowd of potential authors) is valuable.  In particular, simply increasing the size of the crowd, by writing in a common language such as Spanish or English, or by working to enlarge the global community of Tor users, helps to keep us safe.</p>
<p>Currently, the only widely available type of application useful for stylometry-resistance is the humble spell checker (because alternative spellings offer information to our enemies).  </p>
<p>One other simple trick anyone can use in their next social-media post: instead of adopting the name of your favorite Star Wars character, it would be wise to use one of the many Open Source tools to choose a generic name.  </p>
<p>(One tool even alleges to choose a name from a particular ethnic group, a politically incorrect trick which both RU and US government trolls have exploited to the fullest when targeting BLM activists.  Not very nice at all.  But reporters could use this tool to choose a "black" name, and measure how their search results and what kinds of mass-mailing adverts they receive changes.  They will be amazed, even if they think they can guess what to expect.)</p>
<p>But Tor users need much more than this, and the Tor community must work to provide the missing tools.</p>
<p>Currently, English appears to be the one language for which the resources needed for effective anti-stylometry tools are readily available.  We need to work to change this.  Considerable effort will be needed, but if we work together we can get the job done.</p>
<p>The goal of an anti-stylometry application is to help authors modify their draft before posting the text, in order to blunt the clues which the original draft will quite certainly offer up to stylometric analysis.  </p>
<p>One of the most basic category of clues comes from vocabulary.  English in particular is a language richly endowed with multiple synonyms, and unfortunately for the resistance authors tend to prefer particular choices.  This is dangerous.  And this danger is not limited to English language posts.</p>
<p>Good open source word lists for many languages including Spanish are readily available, but anti-stylometry apps need good information on the probabilities that each word will appear in a generic on-line text (or even better, in a text with a particular subject matter, such as environmental protest).</p>
<p>Fortunately, the vast Google trillion word corpus for English is available on line, and this can easily be exploited to compute relative probabilities for nouns, verbs, adjectives, and adverbs.  There are also a number of excellent Open Source English dictionaries which can be used, along with the invaluable WordNet, to compile lists of synonyms for each word.  Any good anti-stylometry app should integrate with gedit (say) to suggest the most common synonym for each word, especially for any rare words which appear in the draft.  This will be especially valuable if coupled with even a rudimentary POS tagger, since many words can serve as both a noun and an adjective.</p>
<p>Google has a trillion word corpus for Spanish too, and I hope that Mexican academics will use their influence to persuade Google to make it freely available.  Similarly for Russian, Mandarin, and other widely used languages.  We also need WordNet for Spanish and other languages.</p>
<p>Currently, parts of speech taggers for English appear to be available only for Windows, but link-grammar's link-parser tool can be adapted for use as a tagger.  Currently, it appears that there is no link-grammar dictionary for Spanish and making one will be a large but doable community effort.  </p>
<p>One very useful property of link-grammar dictionaries is that they can be useful even in preliminary form; they get better (and more useful for anti-stylometry) as they are enlarged.  Further, link-grammar has the remarkable and useful property of being local, i.e. being based upon juxtaposed words rather than entire sentences. For this and other reasons it can probably be re-purposed as a workable POS tagger for the most common languages, including Chinese, Farsi, Russian and other languages which are not closely related to English or Spanish.  Indeed, there are already dictionaries for Russian and Farsi.</p>
<p>POS tagging is vital for suggesting common synonyms, but grammatical characteristics are also used for stylometric analysis and applications like link-parser can be used to try to combat that.  Note that the Google trillion word English corpus includes word trigram data and it is vital to obtain the same for Spanish.  In both cases, this can be exploited to identify and warn against dangerously rare grammatical patterns in English or Spanish.</p>
<p>Punctuation analysis can also be important; Google knows about that too and academics should try to persuade them to share.  Rewriting the old unix utility "style" in order to support languages other than English would be useful, but this effort should focus on outputting POS tagging and various stylistic scores and the app should produce output in a form which is easily piped to other applications.  For example, using "deanonymizing" viz "de-anonymizing" is potentially dangerous, so this post is self-referential in an ungood/un-good way.  Placement of articles, commas, semicolons, colons, hyphens, and even how one uses question marks can offer up to the bad guys much potentially dangerous identifying information.</p>
<p>Incidentally ("by the way", "speaking of which"?), the man page for the Debian version of link-parser is not correct: batch processing doesn't work the way the man page claims.  And the output of link-parser is not well suited for piping to other applications.  Pressure should be applied to persuade Debian to correct these deficiencies.</p>
<p>Another area which needs attention is that in the current internet environment, English words and phrases often appear in posts primarily written in other languages.  For this reason, English language de-anonymization-resistance is useful even to authors who belong to non-English linguistic communities.</p>
<p>One area where resources for Spanish and closely related languages including Brazilian Portuguese are currently far in advance of other languages is machine translation (see apertium in the Debian software repositories).  In fact these are already so good that one easy way for a Spanish language author to misdirect the bad guys would be to use apertium to translate their text into Catalan or Portuguese, possibly followed by machine translation back to Spanish.  Results can induce ROFL hilarity, but may nonetheless be useful for suggesting stylometry resisting modifications to the original draft.</p>
<p>There is a research group in the US which for many years has claimed to be "de-anonymizing" everything which has ever appeared in any language anywhere on the Internet.  It is of course funded by USIC.  As far as I can tell, these people overstate what they can actually accomplish at scale, but they must be regarded as the direct enemy of every Tor user, at least until well-tested and effective anti-stylometry applications become available in the most common languages.</p>
<p>There may be no cure for the fact that writing in a less common language aids de-anonymizing simply because individuals who belong to a smaller community are more easily identified.  This is analogous to the problem with posting from countries with small populations.</p>
<p>In general, the authors of effective anti-stylometry applications should expect to engage in an ongoing "arms race" with the authors of stylometry applications.  I urge them not to even consider seeking funding from entities such as DARPA, which will be eager to offer it, because anything the bad guys learn from you will be used to harm people such as bloggers and leakers and would-be governmental policy-moderators.</p>
<p>Tails has just introduced an invaluable new feature which makes it easier to install specialist software such as apertium for an individual Tails session.  This feature will be even more convenient for those who use Tails booted from a USB stick. Coders should find the new feature useful for preliminary research, but ultimately I hope to see well-tested anti-stylometry apps incorporated into Tails.  It is important to keep base Tails "small" (currently 1.1 GB), but the new feature should make it easy for users to load the particular language databases they need (which will probably be large even if compressed) from removable media or even via onion from a remote site, even if they boot Tails from a DVD.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277737"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277737" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 29, 2018</p>
    </div>
    <a href="#comment-277737">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277737" class="permalink" rel="bookmark">I would like to attend the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would like to attend the open days, what do I have to do to attend?<br />
Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277943"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277943" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>A (not verified)</span> said:</p>
      <p class="date-time">October 06, 2018</p>
    </div>
    <a href="#comment-277943">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277943" class="permalink" rel="bookmark">An open invitation to a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>An open invitation to a gathering of feminist activists from Mexico and women and non-binary members of the Tor community</p></blockquote>
<blockquote><p>It’s going to be an informal event, with free and open entrance for everyone.</p></blockquote>
<p>Is it for everyone-everyone, or are only everyone who isn't male invited? This was confusing and seemingly contradictory.</p>
</div>
  </div>
</article>
<!-- Comment END -->
