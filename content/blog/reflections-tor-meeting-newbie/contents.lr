title: Reflections from a Tor Meeting Newbie
---
pub_date: 2018-10-16
---
author: Sarah
---
tags: meeting
---
summary: I learned this month at my first meeting in Mexico City that there is one aspect of Tor that can not be encapsulated in documentation—the people.
---
_html_body:

<p> </p>
<p>When I joined the the Tor Project in July as Fundraising Director, I quickly learned that most questions I had about Tor—what it does, its history, what makes up the greater ecosystem, even detailed notes from all past meetings—can be found online. The beauty of an open source culture became apparent to me immediately. I learned this month at my first meeting in Mexico City that there is one aspect of Tor that can not be encapsulated in documentation—the people.</p>
<p>I heard many voices at the meeting reflect on how the Tor Project has grown and changed in positive ways over the past few years. Without a doubt, this is a testament to the quality of the people working on Tor and the values we share. <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2018MexicoCity/ParticipantGuidelines">These values</a>—knowledge, engagement, inclusivity, collaboration, and fluidity—were apparent throughout my five days in Mexico.</p>
<p>The people of Tor—staff, volunteers, friends, and community partners—are not only knowledgeable about privacy and security, they are citizens of the world and are eager to talk about philosophy, politics, and the ways in which people interact with systems and power. Without exception, the attendees of every session I went to were attentive and engaged.</p>
<p>The people of Tor are intentional about making everyone feel welcome and valued. From adherence to pronouns to making sessions accessible to non-English speakers, these efforts to enhance accessibility could be seen and felt everywhere. Each session began with a reminder to make space for all types of people to speak and be heard. Although I was meeting most of the attendees for the first time, I was welcomed with open arms and never once felt excluded.</p>
<p>This meeting was inspirational in its engagement of local attendees on the public days. Many sessions were led in English and Spanish, including the State of the Onion address. Members of Tor teams, including myself, talked about what they’d been up to and what was on the horizon, and we took questions from the group. The questions were intimate and thoughtful, and this opening session set an inclusive and collaborative tone for the public days.</p>
<p>I have never witnessed collaboration be so effective and efficient. People who have been working on Tor since the very beginning shared a space at the table with community members and people who were just hired. Roadmaps were created and new ideas were hatched.</p>
<p>I have to admit, I didn’t fully understand the structure of the meeting when I viewed the <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2018MexicoCity">meeting wiki </a>beforehand. I quickly realized the loose structure was by design and facilitated a space where new sessions could be added as the value became apparent. Content collisions were worked out on the spot with session leads happy to accomodate the people that wanted to attend their session. People flowed in and out of sessions and small meetings seamlessly.</p>
<p>Everyone I met was genuinely happy to be there. Old friends and new laughed, shared stories, and during the midweek party, we toasted mezcal and compared dance moves. People also connected through games of Mario Kart and Magic the Gathering.</p>
<p>All of these values coalesce around the ultimate goal of Tor—making the world a better place. Essential human rights cannot be achieved without private and safe access to the internet. The work we do at Tor saves lives. Meeting our Tor community in Mexico City energized me and made me proud of the small part I play in this essential work.</p>
<p><a href="https://newsletter.torproject.org">Sign up for Tor News</a> to stay informed about everything Tor-related, including our upcoming events, and I hope to see you at a meeting soon.</p>
<p><img alt="Sarah Stevenson - The Tor Project" src="/static/images/blog/inline-images/Sarah in Mexico.jpg" class="align-center" /></p>
<p><em>Sarah is the Fundraising Director at the Tor Project, a 501(c)(3) nonprofit organization developing software for privacy and freedom online. We can do the work we do because of <a href="https://donate.torproject.org">your support</a>.</em></p>
<p> </p>

---
_comments:

<a id="comment-278086"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278086" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 16, 2018</p>
    </div>
    <a href="#comment-278086">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278086" class="permalink" rel="bookmark">Sorry but &quot;Essential human…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry but "Essential human rights cannot be achieved without private and safe access to the internet" that's kind of backwards. "Human rights" is one framing that some people like to use when talking to certain non-Tor-using audiences about the virtues of Tor, VPNs and the like, but in fact Privacy and Freedom is the purpose, the end goal of these systems, not a means.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278089"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278089" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 16, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278086" class="permalink" rel="bookmark">Sorry but &quot;Essential human…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278089">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278089" class="permalink" rel="bookmark">&gt; that&#039;s kind of backwards.
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; that's kind of backwards.</p>
<p>You mean it should read "private and safe access to the internet cannot be achieved without [pre-existing, unambiguous, and readily enforceable guarantees of] essential human rights"?</p>
<p>If that were true, we'd all be sunk.  Because there are so few places left where no citizen need fear that their fundamental human rights might be violated by the elite.</p>
<p>&gt; "Human rights" is one framing that some people like to use when talking to certain non-Tor-using audiences about the virtues of Tor, VPNs and the like, </p>
<p>I can assure you that few if any people who espouse a deep belief in human rights are engaged in some kind of PR exercise, if that is what you meant to suggest.  </p>
<p>The book Secrets by Dan Ellsberg might interest you--- it shows how he underwent a profound transfiguration from an unquestioning apparatchnik* to a true believer in human rights.</p>
<p>*As a young marine, Ellsberg didn't bat an eye when asked to draw up a perfectly serious plan for the US invasion of Tel Aviv, of all cities--- an intriguing biographical detail which ought to convince anyone that there's much more to Dan Ellsberg than the "leaker" label.</p>
<p>&gt; but in fact Privacy and Freedom is the purpose, the end goal of these systems, not a means.</p>
<p>But aren't privacy and certain core freedoms--- such as freedom of religion, freedom of association, and freedom of information--- examples of inalienable human rights?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278155"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278155" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278089" class="permalink" rel="bookmark">&gt; that&#039;s kind of backwards.
…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278155">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278155" class="permalink" rel="bookmark">Human rights in practice are…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Human rights in practice are a subjective matter of opinion. If you asked someone who believes only in human rights, "who decides what human rights are, and whether something is compatible with them or not?" they might give any number of answers, none of them satisfactory to one person, although they are to another. Or perhaps no answer at all, dismissing the question. Or an insult.</p>
<p>But one's own privacy and safety are not subjective in the same way; not a matter of the whims of others. This is important.</p>
<p>As for PR exercises, it does happen.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278622"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278622" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 27, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278155" class="permalink" rel="bookmark">Human rights in practice are…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278622">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278622" class="permalink" rel="bookmark">You appear to be trying to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You appear to be trying to argue that "human rights" are somehow less well defined [sic] and less important [sic] than "privacy" and "safety".   It seems to me that this claim is not even wrong: its nonsense.</p>
<p>Many Constitutions around the world more or less explicitly recognize certain rights as fundamental human rights, and privacy is often among them.   The first "modern" Constitution, the U.S. Constitution, predates technological developments which made it easy for bad actors to violate something the Framers took for granted, privacy, but that Constitution does mention the right to security in ones person and papers, which meant feeling that one could feel safe from unwarranted government intrusion inside ones home and also to feel safe from arbitrary arrest and seizure by government agents when one ventures out on private business.</p>
<p>So it seems to me that I am on solid ground in saying that international law generally agrees that privacy and safety from unwarranted governmental intrusion are two fundamental examples of particular universally recognized human rights.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-278088"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278088" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 16, 2018</p>
    </div>
    <a href="#comment-278088">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278088" class="permalink" rel="bookmark">Your first-person account of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Your first-person account of the Mexico City meetup is extremely encouraging!</p>
<p>&gt; I heard many voices at the meeting reflect on how the Tor Project has grown and changed in positive ways over the past few years. ... The people of Tor—staff, volunteers, friends, and community partners—are not only knowledgeable about privacy and security, they are citizens of the world and are eager to talk about philosophy, politics, and the ways in which people interact with systems and power.</p>
<p>Yes, the relatively new (last few years) political and (so to speak) organizational sophistication of Tor Project (for which we owe thanks to Shari, I think) is not only a very welcome development to some of Tor's most ardent supporters, but in an age when Tor is under not only determined and sophisticated technical attack but also intense political and legislative attack from (seemingly) every direction, is IMO absolutely essential to ensuring that TP continues to be available to the many, many people all over the world who need it.</p>
<p>I am particularly happy to see so much evidence that TP is very serious about reaching out to extra-US/EU communities.  No region needs Tor more than Latin America, and recent political earthquakes suggest that in the years to come Tor is likely to be (perhaps literally) a lifesaver to many threatened populations in South, Central and North America.  It is also very encouraging to see the expansion of the Tor community in India.</p>
<p>While we continue to strengthen these growing communities, somehow we also have to not lose track of what's needed in Russia, China, and the Middle East, and we need to think about increasing Tor use in Africa.  And if there is not already a Tor node in Antarctica, someone should make one there--- climate scientists are definitely numbered among the people who badly need Tor, even if they mostly do not yet know what Tor can do to help keep them safe.</p>
<p>IMO we must also to reaffirm the goal of making Tor primarily grassroots user-funded (or at least, funded by users who reside in places where Tor is not yet literally illegal) rather than primarily USG funded.  We haven't heard much about that lately, and AFAIK the long promised updated financials have not been posted, which worries me.</p>
<p>So--- with improved morale on our side--- the struggle continues...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278115"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278115" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>sstevenson (not verified)</span> said:</p>
      <p class="date-time">October 17, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278088" class="permalink" rel="bookmark">Your first-person account of…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278115">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278115" class="permalink" rel="bookmark">Thanks for your comments!
&gt;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for your comments!</p>
<p>&gt; IMO we must also to reaffirm the goal of making Tor primarily grassroots user-funded (or at least, funded by users who reside in places where Tor is not yet literally illegal) rather than primarily USG funded. We haven't heard much about that lately, and AFAIK the long promised updated financials have not been posted, which worries me.</p>
<p>Yes! We are about to begin our year-end crowd sourcing campaign and hope that as many Tor users as possible contribute with the understanding that Tor is strengthened by funding from a wide variety of sources. As you point out, the most vulnerable people most likely cannot financially contribute, so it is up to the rest of us to fund this critically important work. As for our financials, we shifted our fiscal year and are about to complete our audit and will release the results when they are available.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278141"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278141" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 19, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278115" class="permalink" rel="bookmark">Thanks for your comments!
&gt;…</a> by <span>sstevenson (not verified)</span></p>
    <a href="#comment-278141">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278141" class="permalink" rel="bookmark">OK, I&#039;m mentally earmarking…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OK, I'm mentally earmarking something for next month, because I appreciate the enormous value of contributing more in order to support Tor users who cannot--- often because they live in some nation where Tor is more or less illegal.  But what on Earth are Americans to do if Tor becomes illegal in USA?  I wish I could suggest an answer, but the best I can come up with is "prepare to go underground and function as a globally illegal stateless organization".  However one might try to do that...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278305"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278305" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Shava Nerad (not verified)</span> said:</p>
      <p class="date-time">October 28, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278115" class="permalink" rel="bookmark">Thanks for your comments!
&gt;…</a> by <span>sstevenson (not verified)</span></p>
    <a href="#comment-278305">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278305" class="permalink" rel="bookmark">When I was the first…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I was the first executive director of Tor, our goal was to diversify the funding among grassroots, major donor, grant, and governmental funding such that if any funder tried to "pull strings" we could tell them to go jump, lol.  We never took funding that had strings attached (other than reports on how we used the money and other accountability), and we actively refused money we thought might throw a shadow on the project (for example, we were approached by offshore gambling interests at one point, and it was a little off somehow -- and after I refused them, they became personally threatening. Being involved with Tor can be overly exciting sometimes...).</p>
<p>Any money that the project takes in is likely to seem controversial to some number of our constituencies though -- this is the nature of a free speech project.  I personally got involved in the project out of a concern for journalism safety and human rights.  My first trip on Tor's behalf was to human rights groups in NYC and DC asking them to help us by including mentions of Tor in their web pages and newsletters, showing how they were using and training people on the software, because a web search on Tor a dozen years ago was not exactly an altruist's dream, even though so many of our use cases are.  But it is hard to raise money from people who are anonymous and may not feel comfortable leaving personal information (as in routing info or a credit card, etc.).  When I joined the project we were broke.  Big government and foundation grants let us ramp up.</p>
<p>No one asked me to say this, but I've thought about it a lot over the years.  I'm quite much retired from everything now, and don't even volunteer unless you count answering questions on Quora or what-not.  And maybe they've got plans.  </p>
<p>But it seems to me they'll need a lot of engagement if the big grants are going to be replaced with small donations, and that generally means a lot of word of mouth, something the security/privacy community has not historically be great at in terms of fundraising.  But then again, we do like challenges!</p>
<p>Anonymity loves company.  So just as access to public libraries is often funded by people who can afford books on their own, anonymous access to the net must often be subsidized by those who have the means.  I look forward to seeing more people support Tor on a grassroots level.  </p>
<p>But that also depends on many of you evangelizing the onion, as it were.  There's no way the staff can do it on their own, to support the whole project.  And we tend to be very private people, go fig.</p>
<p>So think about how you can help.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278408"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278408" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  Sarah
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Sarah</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Sarah said:</p>
      <p class="date-time">November 06, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278305" class="permalink" rel="bookmark">When I was the first…</a> by <span>Shava Nerad (not verified)</span></p>
    <a href="#comment-278408">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278408" class="permalink" rel="bookmark">Shava - thanks so much for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Shava - thanks so much for your perspective. In a lot of ways, things haven't changed very much. It is definitely a challenge to get the masses to understand the threats against privacy and freedom online, but I am seeing signs that more people are waking up and taking a stand.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-278114"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278114" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2018</p>
    </div>
    <a href="#comment-278114">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278114" class="permalink" rel="bookmark">It&#039;s a good idea to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's a good idea to internationalize the Tor community but so far it doesn't seem to work regarding infrastructure. The vast majority of nodes is still from EU and US. It's rare to encounter even a middle node from South America, Asia or Africa. Is it really that hard and dangerous to set up nodes in these regions or do people just lack information? This is something you should work on. More non-western nodes would greatly increase the strenght of the Tor network against surveillance.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278118"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278118" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278114" class="permalink" rel="bookmark">It&#039;s a good idea to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278118">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278118" class="permalink" rel="bookmark">Those are all good points.  …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Those are all good points.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278183"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278183" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>someone (not verified)</span> said:</p>
      <p class="date-time">October 23, 2018</p>
    </div>
    <a href="#comment-278183">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278183" class="permalink" rel="bookmark">I love Tor!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I love Tor!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278332"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278332" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>many names it seems  (not verified)</span> said:</p>
      <p class="date-time">October 30, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278183" class="permalink" rel="bookmark">I love Tor!</a> by <span>someone (not verified)</span></p>
    <a href="#comment-278332">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278332" class="permalink" rel="bookmark">I have questions as to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have questions as to rights of privacy...although I also see the other side of the coin.<br />
many secrets have helped to create the situation at hand.<br />
I am not completely avoiding fault in the situation .I made the choices ..even if one....a human has mental issues it should notbe used as a crutch..instead a challenge.<br />
with the true compassion &amp; understanding of those who surround one said human the challenge is much more attainable. Unfortunately..that is not always completely  the situation.<br />
may god bless us all on our paths to conquerbour challenges and may we all be surrounded with much compassion, understanding and love.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278273"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278273" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>publius (not verified)</span> said:</p>
      <p class="date-time">October 25, 2018</p>
    </div>
    <a href="#comment-278273">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278273" class="permalink" rel="bookmark">The hyperlinks in this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The hyperlinks in this article such as "<a href="https://blog.torproject.org/newsletter.torproject.org" rel="nofollow">https://blog.torproject.org/newsletter.torproject.org</a>" and "<a href="https://blog.torproject.org/donate.torproject.org" rel="nofollow">https://blog.torproject.org/donate.torproject.org</a>" are broken. Perhaps they should be "<a href="https://newsletter.torproject.org" rel="nofollow">https://newsletter.torproject.org</a>" and "<a href="https://donate.torproject.org" rel="nofollow">https://donate.torproject.org</a>"?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278375"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278375" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 03, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278273" class="permalink" rel="bookmark">The hyperlinks in this…</a> by <span>publius (not verified)</span></p>
    <a href="#comment-278375">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278375" class="permalink" rel="bookmark">Jeez, you&#039;re right. I&#039;ve…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Jeez, you're right. I've fixed it. Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278343"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278343" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ReplaceTheGOP (not verified)</span> said:</p>
      <p class="date-time">October 31, 2018</p>
    </div>
    <a href="#comment-278343">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278343" class="permalink" rel="bookmark">&quot;When I joined the Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"When I joined the Tor Project in July as Fundraising Director, I quickly learned that most questions I had about Tor—what it does, its history, what makes up the greater ecosystem, even detailed notes from all past meetings—can be found online."</p>
<p>Very frustrating to me. You are admitting you were NOT really qualified to assume (especially) Director as you didn't know what you were fundraising for.</p>
<p>As a 20+ yr IT Engineer vet w/ a Masters degree, I have been seeking a Director level position unsuccessfully.<br />
Amazing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278409"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278409" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  Sarah
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Sarah</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Sarah said:</p>
      <p class="date-time">November 06, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278343" class="permalink" rel="bookmark">&quot;When I joined the Tor…</a> by <span>ReplaceTheGOP (not verified)</span></p>
    <a href="#comment-278409">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278409" class="permalink" rel="bookmark">I&#039;m sorry you&#039;ve struggled…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm sorry you've struggled to find the position you desire! The point of my quote was to express how easy it was to get up to speed on Tor so that I could hit the ground running with the skills I've honed in my 14 years as a professional fundraiser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
