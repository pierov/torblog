title: Quick thoughts on tor2web
---
pub_date: 2008-12-16
---
author: phobos
---
tags:

hidden services
onion addresses
tor2web
---
categories: onion services
---
_html_body:

<p>Aaron and Virgil's <a href="http://tor2web.com" rel="nofollow">tor2web</a> site has been picked up by <a href="http://blog.wired.com/27bstroke6/2008/12/tor-anonymized.html" rel="nofollow">Wired's 27bstroke6 blog</a> and <a href="http://arstechnica.com/news.ars/post/20081215-tor2web-brings-anonymous-tor-sites-to-the-regular-web.html" rel="nofollow">Ars Technica</a>.</p>

<p>First off, I think it's a neat implementation of allowing non-Tor users access to the realm of .onion, aka hidden services.  While I think using the <a href="https://www.torproject.org/torbrowser/" rel="nofollow">Tor Browser Bundle</a> is incredibly easy, not everyone agrees with me.  Neither Tor nor tor2web host any of the hidden service content.  We don't know who does, nor who runs the hidden service.  This brings me to my next thought.</p>

<p>Part of the challenge is that right now there is usually some place in the world that will publish your thing for you if it's interesting. Wikileaks is this year's example. But down the road, it may become the case that some things are just too hot for a public site to touch, and besides not everybody can convince Wikileaks to put up their document. Remember Usenet in the 80s?  Remember BBSes?  Remember NCSA Mosaic and the web in the early '90s?  Technical people gravitated to these technologies first.  The content reflected the people running the nodes; and the size of the community.  People post things just because they can.  Teenagers continue to post their plans for world domination; I've read them on BBSes, usenet, .plan files,  and their web pages over the decades.  Why should hidden services be any different?</p>

<p>Hidden Services are fairly new.  They can be slow.  They are a work in progress, but we believe them to be very secure and anonymous.  They are an example of an application that can run on top of an anonymized network layer, such as Tor.  Much like any new technology, the userbase is probably small.  Sure, all Tor users have access to them, but it takes a lot of motivation and skill to setup a website correctly for hidden services.  There are lots of ways to do it wrong and expose where the hidden service runs on the public Internet.  This leads to less useful content generated for the masses.  Make it easier, and perhaps the users will come in droves.</p>

<p>A few thoughts about potential services, merely copying from the current public Internet now;
</p>

<ul>
<li>anonymous blogging (think wordpress, movable type, blogspot)</li>
<li>anonymous microblogging (aka twitter)</li>
<li>anonymous forums for dissidents, abuse survivors, cancer survivors, human rights activists</li>
<li>anonymous dropbox or other personal document sharing (for stuff you just want to access when remote)</li>
<li>anonymous instant messaging</li>
</ul>

<p>Why couldn't twitter, wordpress/MT/Blogspot, and forum providers setup a hidden service to their current offerings?  Wikileaks and IndyMedia do already.  If your customers are on a heavily censored Internet connection, hidden services may be the only way they can access your service.  Some of these suggestions exist today, but they suffer from the lack of "network effect".  </p>

<p>There is a lot of potential for hidden services to be valuable and host great content.  Don't judge them by the little known content that exists today.</p>

<p>Hidden services aren't limited to websites either.  I use them heavily for anonymous ssh access and getting access to my file systems behind NAT and firewalls.</p>

<p>Be aware that if tor2web logs IP addresses, they have yours; I don't believe they do log, however.  I wonder if Google, Yahoo, and others will crawl tor2web and start indexing the content.</p>

<p>P.S. Wired, we're torproject.org, not tor.eff.org anymore.</p>

---
_comments:

<a id="comment-436"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-436" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://candrews.integralblue.com">Craig (not verified)</a> said:</p>
      <p class="date-time">December 15, 2008</p>
    </div>
    <a href="#comment-436">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-436" class="permalink" rel="bookmark">Microblogging</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Setting up a hidden service microblogging site would be pretty easy. Just grab a copy of laconica (<a href="http://laconi.ca" rel="nofollow">http://laconi.ca</a>) and set it up. It can even federate with other members of the openmicroblogging network, like identi.ca - which would be very cool. Perhaps the Tor Project itself could consider getting such an effort started.</p>
<p>For non-"hidden" sites (such as well known ones with an establish "conventional" web presence) would it be beneficial if they offered a hidden service presence (.onion) as well? For example, would it be beneficial if any way if <a href="http://identi.ca" rel="nofollow">http://identi.ca</a> offered a .onion address as well, that went to the exact same site?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1986"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1986" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>oniku (not verified)</span> said:</p>
      <p class="date-time">August 03, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-436" class="permalink" rel="bookmark">Microblogging</a> by <a rel="nofollow" href="http://candrews.integralblue.com">Craig (not verified)</a></p>
    <a href="#comment-1986">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1986" class="permalink" rel="bookmark">Thats a nice idea. But you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thats a nice idea. But you have to register at the laconica-site with a valid email-address or change all the code about the validation and email-notification-services.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2064"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2064" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 13, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1986" class="permalink" rel="bookmark">Thats a nice idea. But you</a> by <span>oniku (not verified)</span></p>
    <a href="#comment-2064">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2064" class="permalink" rel="bookmark">People should start offering</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>People should start offering mail via tor :)<br />
I think it would be very good if big services started offering access via tor. An example would be Freenode's tor hidden service.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2111"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2111" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2064" class="permalink" rel="bookmark">People should start offering</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2111">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2111" class="permalink" rel="bookmark">They do</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are a few hidden services that offer mail inside .onion-space already.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13787"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13787" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 04, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-13787">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13787" class="permalink" rel="bookmark">Have a look at tormail.net,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have a look at tormail.net, it is also from inside .onion out.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-437"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-437" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://k3lvinmitnick.blogspot.com">Anonymous (not verified)</a> said:</p>
      <p class="date-time">December 16, 2008</p>
    </div>
    <a href="#comment-437">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-437" class="permalink" rel="bookmark">how to change ip after a period time ?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I used Tor with addon in firefox to download from rapidshare very fast.<br />
Thanks Tor first, </p>
<p>But I want to change the IP after a period of time (E.x: 10 minutes), how can I ?</p>
<p>(I must Stop Tor and Start Tor again by hand, i hate that work)</p>
<p>Plz email to me: <a href="mailto:k3lvinmitnick@gmail.com" rel="nofollow">k3lvinmitnick@gmail.com</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-438"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-438" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>AD (not verified)</span> said:</p>
      <p class="date-time">December 16, 2008</p>
    </div>
    <a href="#comment-438">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-438" class="permalink" rel="bookmark">Indexing has started</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>phobos wrote, “I wonder if Google, Yahoo, and others will crawl tor2web and start indexing the content.”</p>
<p>That one can be answered.  Try the search ‘site:tor2web.com’ in Google.  The answer is yes.  What’s more, Google appears to be correlating .onion pages with the regular web.  If you look at Google’s cached content for some of the .onion pages it has accessed through tor2web, the URL in the header is that for the regular (non-Tor) web page.  I suppose it’s more efficient to point to what Google considers to be the canonical URL wherever possible.</p>
<p><a href="http://74.125.77.132/search?q=cache:FHbFHIav7usJ:gaddbiwdftapglkq.tor2web.com/" rel="nofollow">http://74.125.77.132/search?q=cache:FHbFHIav7usJ:gaddbiwdftapglkq.tor2w…</a><br />
<a href="http://74.125.77.132/search?q=cache:RbZxo1POzoAJ:rjgcfnw4sd2jaqfu.tor2web.com/" rel="nofollow">http://74.125.77.132/search?q=cache:RbZxo1POzoAJ:rjgcfnw4sd2jaqfu.tor2w…</a><br />
<a href="http://74.125.77.132/search?q=cache:rKMFPXORTAAJ:5pnauannuco7bsma.tor2web.com/movies.htm" rel="nofollow">http://74.125.77.132/search?q=cache:rKMFPXORTAAJ:5pnauannuco7bsma.tor2w…</a></p>
<p>It works in the other direction too.  Searching for ‘rjgcfnw4sd2jaqfu.tor2web.com’ currently gives one result – the URL for the underlying website.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-439"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-439" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://moblog.wiredwings.com/archives/20081218/Proposal-for-Tor-Hidden-Services.html">Moritz Bartl (not verified)</a> said:</p>
      <p class="date-time">December 18, 2008</p>
    </div>
    <a href="#comment-439">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-439" class="permalink" rel="bookmark">Adaption of software necessary</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As a hidden service provider, you need to protect yourself, too. What would it help if Twitter offered a hidden service interface? The service is well-known, public, can easily be forced to give away user credentials and remove "improper content". If you're using the Tor client, you can already use existing platforms anonymously.</p>
<p>More importantly, we need to adapt (or write new) blogging software and file dumps that completely abandon IP logging, and stop publishers and users from linking external content.</p>
<p><a href="http://moblog.wiredwings.com/archives/20081218/Proposal-for-Tor-Hidden-Services.html" rel="nofollow">http://moblog.wiredwings.com/archives/20081218/Proposal-for-Tor-Hidden-…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-441"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-441" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-439" class="permalink" rel="bookmark">Adaption of software necessary</a> by <a rel="nofollow" href="http://moblog.wiredwings.com/archives/20081218/Proposal-for-Tor-Hidden-Services.html">Moritz Bartl (not verified)</a></p>
    <a href="#comment-441">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-441" class="permalink" rel="bookmark">censorship resistance</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think phobos' idea was that hidden services can't be found and since they work inside tor, they defeat censorship or filtering.  This also means that an exit relay can't watch what you do and build up a pattern over time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-843"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-843" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2009</p>
    </div>
    <a href="#comment-843">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-843" class="permalink" rel="bookmark">http://tor2web.com/tortodo
&quot;R</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://tor2web.com/tortodo" rel="nofollow">http://tor2web.com/tortodo</a></p>
<p>"Right now the only way to access hidden services is by using the Tor client, which routes all Internet traffic through the Tor network and so slows down normal Internet usage. It would be nice if one could run the Tor client in a mode where it only handled *.onion requests (i.e. requests to hidden services). That way users could transparently access hidden services without any degradation in normal Internet service."</p>
<p>Just modify the Privoxy configuration file.</p>
<p>Change the line that says:<br />
forward-socks4a / 127.0.0.1:9050 . </p>
<p>To:<br />
forward-socks4a .onion 127.0.0.1:9050 . </p>
<p>Of course, you should only do this if you are only using Tor to access hidden services, but not to protect your anonymity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3400"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3400" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>KeK (not verified)</span> said:</p>
      <p class="date-time">December 03, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-843" class="permalink" rel="bookmark">http://tor2web.com/tortodo
&quot;R</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-3400">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3400" class="permalink" rel="bookmark">Privoxy seems to be removed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Privoxy seems to be removed from the newer versions for vidalia bundle. How do you do this with the newest version of TOR?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3411"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3411" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 04, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3400" class="permalink" rel="bookmark">Privoxy seems to be removed</a> by <span>KeK (not verified)</span></p>
    <a href="#comment-3411">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3411" class="permalink" rel="bookmark">You can install privoxy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can install privoxy yourself, or use the polipo we've included and configured for you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-844"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-844" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>JL (not verified)</span> said:</p>
      <p class="date-time">March 20, 2009</p>
    </div>
    <a href="#comment-844">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-844" class="permalink" rel="bookmark">Bundle installation failure</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A tried to install the bundle for Mac OSX and there was an installation failrue. The alert said it could not run a postflight script for Tor. Any thoughts?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-850"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-850" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 21, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-844" class="permalink" rel="bookmark">Bundle installation failure</a> by <span>JL (not verified)</span></p>
    <a href="#comment-850">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-850" class="permalink" rel="bookmark">I bet it ran</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I bet the postflight script ran, but couldn't start up Firefox automatically to install Torbutton.  That's typically the case.  Tor is probably installed and working fine.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-4407"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4407" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 24, 2010</p>
    </div>
    <a href="#comment-4407">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4407" class="permalink" rel="bookmark">&quot;Right now the only way to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Right now the only way to access hidden services is by using the Tor client, which routes all Internet traffic through the Tor network and so slows down normal Internet usage.<a href="http://www.filmizleee.com" rel="nofollow">film izle</a> It would be nice if one could run the Tor client in a mode where it only handled *.onion requests (i.e. requests to hidden services). That way users could transparently access hidden services without any degradation in normal Internet service."</p>
<p>Just modify the Privoxy configuration file.</p>
<p>Change the line that says:<br />
forward-socks4a / 127.0.0.1:9050 .<br />
<a href="http://www.nettefilm.net" rel="nofollow">film izle</a><br />
To:<br />
forward-socks4a .onion 127.0.0.1:9050 .</p>
<p>Of course, you should only do this if you are only using Tor to access hidden services, but not to protect your anonymity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5490"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5490" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 27, 2010</p>
    </div>
    <a href="#comment-5490">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5490" class="permalink" rel="bookmark">I do not believe to2web has</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I do not believe to2web has any real future. It is so slow taking up to an hour to connect. If this is the way forward, let's all go back to DOS, at least this was faster.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7112"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7112" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 26, 2010</p>
    </div>
    <a href="#comment-7112">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7112" class="permalink" rel="bookmark">Using the Tor Bundle is not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Using the Tor Bundle is not that easy. Some people have difficulties using a normal browser, so they couldn't install and use the Tor Bundle. Plus, they would use it badly (ie: visiting a .onion website and then log into their webmail account, revealing their password to an exit node, it's hard to explain to a novice that Tor can both protect you and harm you).</p>
<p>So tor2web is great, and I'm impressed by its speed. I set up a hidden service to test the speed and it was almost as fast as a normal website. I can't imagine Tor without tor2web.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7123"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7123" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 27, 2010</p>
    </div>
    <a href="#comment-7123">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7123" class="permalink" rel="bookmark">tor2web sends a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>tor2web sends a X-Forwarded-For header field to the hidden service. This field contains your IP address. This is ridiculous. Just try to run an hidden service to see by yourself, if you don't believe me.</p>
<p>I know that tor2web aims at protecting publishers rather than readers, but still, there's no point in revealing the user's IP address to the hidden service. We don't use Tor to give our IP to the sites we visit.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7125"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7125" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 27, 2010</p>
    </div>
    <a href="#comment-7125">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7125" class="permalink" rel="bookmark">Update to my previous</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Update to my previous comment: tor2web's admin fixed the x-forwarded-for issue after I emailed him. It was unintentional. The problem has probably been there since the beginning of tor2web (see <a href="http://l6nvqsqivhrunqvs.tor2web.com/?do=topic&amp;id=7116" rel="nofollow">http://l6nvqsqivhrunqvs.tor2web.com/?do=topic&amp;id=7116</a> ).</p>
</div>
  </div>
</article>
<!-- Comment END -->
