title: Tor Weekly News — November 27th, 2013
---
pub_date: 2013-11-27
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-second issue of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Round of updated Tor Browser Bundles</h1>

<p>Mozilla put out an <a href="https://www.mozilla.org/en-US/firefox/17.0.11/releasenotes/" rel="nofollow">urgent security release</a> of the stable Firefox branch with version 17.0.11esr. The <a href="https://blog.torproject.org/blog/new-tor-browser-bundles-firefox-17011esr-and-tor-02418-rc" rel="nofollow">stable version of the Tor Browser Bundle has been updated</a> accordingly. The 2.4 release candidate also received an update, together with the latest incarnation of tor 0.2.4.18-rc. Both were then given <a href="https://blog.torproject.org/blog/64-bit-gnulinux-tor-browser-bundles-updated" rel="nofollow">a further update</a> due to an issue on 64 bit GNU/Linux systems.</p>

<p>The 3.0 branch saw the <a href="https://blog.torproject.org/blog/tor-browser-bundle-30rc1-released" rel="nofollow">release of 3.0rc1</a> which — on top of updating its base software — fixed a build reproducibility issue on Windows, and a few other small fixes.</p>

<p>An <a href="https://mailman.boum.org/pipermail/tails-dev/2013-November/004152.html" rel="nofollow">updated version of Tails</a> and the pluggable transport bundle are still in the making at the time of writing.</p>

<h1>Tor is looking for a Browser Hacker and an Extension Developer!</h1>

<p>Mike Perry <a href="https://blog.torproject.org/blog/tor-looking-browser-hacker-and-extension-developer" rel="nofollow">wrote a blog post</a> to announce two new positions available at the Tor Project: “We are looking for a <a href="https://www.torproject.org/about/jobs-browserhacker.html" rel="nofollow">C++ browser developer</a> to work on our Firefox-based browser, and a <a href="https://www.torproject.org/about/jobs-extdev.html.en" rel="nofollow">Firefox extension developer</a> to work on our growing number of Firefox extensions. Our ideal candidates would be comfortable in both roles, but we are also interested in hearing from people with either skillset.”</p>

<p>Look at the job descriptions for more details and how to apply for these exciting opportunities to make Tor software even better.</p>

<h1>“Safeplug”</h1>

<p>Roman Mamedov <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031199.html" rel="nofollow">reported</a> that the Californian company Cloud Engines is now shipping a device called the “Safeplug”. Exactly how the device works is unclear, but according to their FAQ, it looks like a router which transparently directs its client connections through Tor.</p>

<p>Such an approach is known to be flawed. <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031200.html" rel="nofollow">Sean Alexandre</a> was prompt in reminding everyone that “application protocols can still reveal your identity”, and quoted the <a href="https://www.torproject.org/download/download-easy.html#warning" rel="nofollow">warning on Tor’s download page</a>: “To avoid problems with Tor configuration, we strongly recommend you use the Tor Browser Bundle. It is pre-configured to protect your privacy and anonymity on the web as long as you’re browsing with the Tor Browser itself. Almost any other web browser configuration is likely to be unsafe to use with Tor.”</p>

<p>Aaron Gibson <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031215.html" rel="nofollow">detailed other concerns</a>, namely the absence of source code or design documents, the mandatory router registration procedure, issues with the automatic update system, and the terms of service. He also criticized the “torified everything” approach and outlined an alternative which he had discussed with Roger Dingledine: “providing a captive portal that would instruct a user to download a copy of TBB and use the local router device as a first hop into the Tor network, perhaps by configuring the device as a bridge.”</p>

<p>On the upside, Andrew Lewman <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031204.html" rel="nofollow">views the product</a> as “a fine test case for consumer-level torouter market analysis. It would be great to learn 6 months from now how many they sold and a summary of customer feedback.” Despite having “<a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031235.html" rel="nofollow">lots of concerns</a>”, Andrew is “trying to discuss them with Cloud Engines” and praised the community for “doing a fine job of raising questions”.</p>

<h1>Miscellaneous news</h1>

<p>Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005836.html" rel="nofollow">gave the number 223</a> to Esfandiar Mohammadi’s proposal titled “<a href="https://gitweb.torproject.org/torspec.git/blob_plain/HEAD:/proposals/223-ace-handshake.txt" rel="nofollow">Ace: Improved circuit-creation key exchange</a>”.</p>

<p>Matt Pagan <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000385.html" rel="nofollow">reported </a> on his trip to Washington, D.C., USA for the Rally Against Mass Surveillance. He gave an account of his talk during the cryptoparty and the march that happened the next day.</p>

<p>Arturo Filastò sent his <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000386.html" rel="nofollow">report</a> about his activities in October.</p>

<p>Nathan Freitas reported on his <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005857.html" rel="nofollow">efforts to use GeckoView on Android 4.4</a>, which can be seen as the “first step towards Tor Browser on Android”.</p>

<p>Kevin Dyer <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005861.html" rel="nofollow">announced</a> a new release of a <a href="https://fteproxy.org/" rel="nofollow">pluggable transport powered by Format-Transforming Encryption</a>. Cross-platform builds of the pluggable transport Tor Browser Bundle are available for download for the adventurous.</p>

<h1>Tor help desk roundup</h1>

<p>Echoing the tor-talk thread summarized above, multiple people asked whether or not the Tor Project could recommend the Safeplug device.</p>

<p>An OS X user asked if it was always necessary to open the Tor Browser folder in order to start the Tor Browser Bundle. It is possible to create an alias in Mac OS or a shortcut in Windows to the “Start Tor Browser” script and place that alias or shortcut in a convenient place,<br />
such as the Desktop.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, Matt Pagan, harmony, Philipp Winter, and dope457.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

