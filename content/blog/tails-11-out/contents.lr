title: Tails 1.1 is out
---
pub_date: 2014-07-22
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.1, is out.</p>

<p>All users must <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> as soon as possible: this release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.0.1/" rel="nofollow">numerous security issues</a>.</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Rebase on Debian Wheezy
<ul>
<li>Upgrade literally thousands of packages.</li>
<li>Migrate to GNOME3 fallback mode.</li>
<li>Install LibreOffice instead of OpenOffice.</li>
</ul>
</li>
<li>Major new features
<ul>
<li>UEFI boot support, which should make Tails boot on modern hardware and Mac computers.</li>
<li>Replace the Windows XP camouflage with a Windows 8 camouflage.</li>
<li>Bring back VirtualBox guest modules, installed from Wheezy backports. Full functionality is only available when using the 32-bit kernel.</li>
</ul>
</li>
<li>Security fixes
<ul>
<li>Fix write access to boot medium via udisks (<a href="https://labs.riseup.net/code/issues/6172" rel="nofollow">ticket #6172</a>).</li>
<li>Upgrade the web browser to 24.7.0esr-0+tails1~bpo70+1 (Firefox 24.7.0esr + Iceweasel patches + Torbrowser patches).</li>
<li>Upgrade to Linux 3.14.12-1 (fixes CVE-2014-4699).</li>
<li>Make persistent file permissions safer (<a href="https://labs.riseup.net/code/issues/7443" rel="nofollow">ticket #7443</a>).</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Fix quick search in Tails Greeter's Other languages window (Closes: <a href="https://labs.riseup.net/code/issues/5387" rel="nofollow">ticket #5387</a>)</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Don't install Gobby 0.4 anymore. Gobby 0.5 has been available in Debian since Squeeze, now is a good time to drop the obsolete 0.4 implementation.</li>
<li>Require a bit less free memory before checking for upgrades with Tails Upgrader. The general goal is to avoid displaying "Not enough memory available to check for upgrades" too often due to over-cautious memory requirements checked in the wrapper.</li>
<li>Whisperback now sanitizes attached logs better with respect to DMI data, IPv6 addresses, and serial numbers (<a href="https://labs.riseup.net/code/issues/6797" rel="nofollow">ticket #6797</a>, <a href="https://labs.riseup.net/code/issues/6798" rel="nofollow">ticket #6798</a>, <a href="https://labs.riseup.net/code/issues/6804" rel="nofollow">ticket #6804</a>).</li>
<li>Install the BookletImposer PDF imposition toolkit.</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<ul>
<li><a href="https://tails.boum.org/news/version_1.1/#index2h1" rel="nofollow">New known issues in Tails 1.1</a>.</li>
<li><a href="https://tails.boum.org/support/known_issues/" rel="nofollow">Longstanding known issues</a>.</li>
</ul>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<p>Note that for this release there are some special actions needed when <a href="https://tails.boum.org/news/version_1.1/#index5h1" rel="nofollow">upgrading from ISO</a> and <a href="https://tails.boum.org/news/version_1.1/#index6h1" rel="nofollow">automatically upgrading from Tails 1.1~rc1</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for September 2.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

<p><strong>Support and feedback</strong></p>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

