title: New Release: Tor Browser 9.5a13
---
pub_date: 2020-05-22
---
author: sysrqb
---
tags:

tbb
tor browser
tbb-9.5
---
categories: applications
---
_html_body:

<p>Tor Browser 9.5a13 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/9.5a13/">distribution directory.</a></p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-9010">latest stable release</a> instead.</p>
<p>This release updates NoScript to version 11.0.26, and Tor to 0.4.3.5.</p>
<p>This is expected to be the final alpha release of Tor Browser 9.5, and it will become the new stable version in the coming weeks. Please report any problems you discover.</p>
<p>Also, we want to offer a big thank you to Sanketh for resolving some website breakage bugs. This release includes one fix, backported from Mozilla Firefox, and future releases will include additional fixes. Thank you for your help with making the web a better place.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">changelog</a> since Tor Browser 9.5a12 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Bump NoScript to 11.0.26</li>
<li>Bump Tor to 0.4.3.5</li>
<li>Translations update</li>
<li><a href="https://bugs.torproject.org/34157">Bug 34157</a>: Backport fix for Mozilla Bug 1511941</li>
</ul>
</li>
<li> Windows + OS X + Linux
<ul>
<li>Update Tor Launcher to 0.2.21.7
<ul>
<li>Translations update</li>
</ul>
</li>
<li><a href="https://bugs.torproject.org/34196">Bug 34196:</a> Update site info URL with the onion name</li>
<li><a href="https://bugs.torproject.org/34043">Bug 34043:</a> Update snowflake to persist sessions across proxies</li>
</ul>
</li>
<li> Windows
<ul>
<li><a href="https://bugs.torproject.org/33113">Bug 33113:</a> Bump NSIS version to 3.05</li>
</ul>
</li>
<li> Build System
<ul>
<li>All Platforms
<ul>
<li>Bump Go to 1.13.11</li>
<li><a href="https://bugs.torproject.org/34242">Bug 34242:</a> Fix creation of Linux containers</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-287926"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287926" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 22, 2020</p>
    </div>
    <a href="#comment-287926">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287926" class="permalink" rel="bookmark">What about https://www…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about <a href="https://www.mozilla.org/en-US/firefox/android/68.8.1/releasenotes/" rel="nofollow">https://www.mozilla.org/en-US/firefox/android/68.8.1/releasenotes/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287956"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287956" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">May 26, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287926" class="permalink" rel="bookmark">What about https://www…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287956">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287956" class="permalink" rel="bookmark">That release was Android…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That release was Android-only and it only added data collection Mozilla wanted.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287928"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287928" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 22, 2020</p>
    </div>
    <a href="#comment-287928">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287928" class="permalink" rel="bookmark">pospeselr, how about re…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>pospeselr, how about re-landing <a href="https://trac.torproject.org/projects/tor/ticket/28546#comment:9" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/28546#comment:9</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287929"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287929" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 22, 2020</p>
    </div>
    <a href="#comment-287929">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287929" class="permalink" rel="bookmark">some jerk modified my…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>some jerk modified my comment, and it is still not reverted, which is scare: <a href="https://trac.torproject.org/projects/tor/ticket/30318#comment:4" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/30318#comment:4</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287930"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287930" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 22, 2020</p>
    </div>
    <a href="#comment-287930">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287930" class="permalink" rel="bookmark">&gt; Bug 34157: Backport fix…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Bug 34157: Backport fix for Mozilla Bug 1511941<br />
We should define a clear policy about what we spoof and what we don't expose.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287932"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287932" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 23, 2020</p>
    </div>
    <a href="#comment-287932">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287932" class="permalink" rel="bookmark">torbrowser-install-9.5a13_en…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>torbrowser-install-9.5a13_en-US.exe doesn't even allow to select English in the drop-down list. What a mess.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287961"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287961" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">May 26, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287932" class="permalink" rel="bookmark">torbrowser-install-9.5a13_en…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287961">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287961" class="permalink" rel="bookmark">Which drop-down list? At…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Which drop-down list? At installation? English should be the only available language/locale in that file.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287970"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287970" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 27, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-287970">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287970" class="permalink" rel="bookmark">The installer has only one…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The installer has only one drop-down list. A few languages there don't contain English.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288077"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288077" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 03, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-288077">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288077" class="permalink" rel="bookmark">64-bit Win installer is OK.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>64-bit Win installer is OK.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-287935"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287935" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>feson (not verified)</span> said:</p>
      <p class="date-time">May 23, 2020</p>
    </div>
    <a href="#comment-287935">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287935" class="permalink" rel="bookmark">ok but when will we get…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ok but when will we get snowflake in the stable release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287960"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287960" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">May 26, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287935" class="permalink" rel="bookmark">ok but when will we get…</a> by <span>feson (not verified)</span></p>
    <a href="#comment-287960">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287960" class="permalink" rel="bookmark">Snowflake will be included…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Snowflake will be included in a Stable release in a few months. It is nearly ready for production use.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287963"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287963" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  dcf
  </article>
    <div class="comment-header">
      <p class="comment__submitted">dcf said:</p>
      <p class="date-time">May 26, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287935" class="permalink" rel="bookmark">ok but when will we get…</a> by <span>feson (not verified)</span></p>
    <a href="#comment-287963">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287963" class="permalink" rel="bookmark">See https://bugs.torproject…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>See <a href="https://bugs.torproject.org/19001" rel="nofollow">https://bugs.torproject.org/19001</a> and its child tickets for the plans toward making Snowflake stable.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287938"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287938" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anonymous (not verified)</span> said:</p>
      <p class="date-time">May 23, 2020</p>
    </div>
    <a href="#comment-287938">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287938" class="permalink" rel="bookmark">Thanks for the new release!…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the new release! I'm seeing a couple help-related issues:<br />
1. Settings -&gt; The Tor Project Tor Browser -&gt; About Tor Browser shows default Firefox ESR 68 page<br />
2. Clicking on Help in the menu takes me to default Mozilla support page. I'm not sure what 'Bug 33698: Update "About Tor Browser" links in Tor Browser' in 9.5a12 was about.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287959"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287959" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">May 26, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287938" class="permalink" rel="bookmark">Thanks for the new release!…</a> by <span>anonymous (not verified)</span></p>
    <a href="#comment-287959">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287959" class="permalink" rel="bookmark">Thanks for reporting this.
1…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for reporting this.<br />
1) We will work on that in the next version<br />
2) That was a mistake in the changelog of 9.5a12, thanks for noticing it. Ticket 33698 only affected Windows/MacOS/Linux, but not Android.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287939"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287939" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 23, 2020</p>
    </div>
    <a href="#comment-287939">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287939" class="permalink" rel="bookmark">NoScript still broken.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>NoScript still broken.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287958"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287958" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">May 26, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287939" class="permalink" rel="bookmark">NoScript still broken.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287958">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287958" class="permalink" rel="bookmark">Can you be more specific?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you be more specific?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287975"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287975" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-287975">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287975" class="permalink" rel="bookmark">No CTP in frames.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No CTP in frames.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-287940"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287940" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 23, 2020</p>
    </div>
    <a href="#comment-287940">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287940" class="permalink" rel="bookmark">FWIW, Turbo Tunnel increases…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>FWIW, Turbo Tunnel increases throughput from ~1 Mbps to ~1.5 Mbps max on Windows.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287964"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287964" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  dcf
  </article>
    <div class="comment-header">
      <p class="comment__submitted">dcf said:</p>
      <p class="date-time">May 26, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287940" class="permalink" rel="bookmark">FWIW, Turbo Tunnel increases…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287964">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287964" class="permalink" rel="bookmark">It&#039;s good to hear that it&#039;s…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's good to hear that it's working well for you. But the Turbo Tunnel features added to Snowflake in 9.5a13 (<a href="https://bugs.torproject.org/34043" rel="nofollow">#34043</a>) do not increase throughput, only reliability. It's possible that you just got a faster Snowflake proxy than usual. The Turbo Tunnel design will, however, make possible certain throughput enhancements, like <a href="https://bugs.torproject.org/25723" rel="nofollow">#25723</a> to use several Snowflake proxies at once.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287952"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287952" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">May 26, 2020</p>
    </div>
    <a href="#comment-287952">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287952" class="permalink" rel="bookmark">You say &quot;All Platforms: Bump…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You say "All Platforms: Bump Tor to ..." in every blog post, yet Android is still on 0.4.1.5-rc for a long time. What gives?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287955"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287955" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">May 26, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287952" class="permalink" rel="bookmark">You say &quot;All Platforms: Bump…</a> by <span>anon (not verified)</span></p>
    <a href="#comment-287955">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287955" class="permalink" rel="bookmark">That is caused by a bug in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That is caused by a bug in the UI. We're tracking it with <a href="https://trac.torproject.org/projects/tor/ticket/33930" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/33930</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287973"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287973" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Dev bele (not verified)</span> said:</p>
      <p class="date-time">May 27, 2020</p>
    </div>
    <a href="#comment-287973">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287973" class="permalink" rel="bookmark">Good working</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good working</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287977"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287977" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Tandino (not verified)</span> said:</p>
      <p class="date-time">May 28, 2020</p>
    </div>
    <a href="#comment-287977">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287977" class="permalink" rel="bookmark">Window 10  did the update …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Window 10  did the update  and when i tied to go back in  the to mail box  i got this message<br />
Warning: Potential Security Risk Ahead</p>
<p>Tor Browser detected an issue and did not continue to torbox3uiot6wchz.onion.sh. The website is either misconfigured or your computer clock is set to the wrong time.</p>
<p>It’s likely the website’s certificate is expired, which prevents Tor Browser from connecting securely. If you visit this site, attackers could try to steal information like your passwords, emails, or credit card details.</p>
<p>What can you do about it?</p>
<p>Your computer clock is set to Thursday, May 28, 2020. Make sure your computer is set to the correct date, time, and time zone in your system settings, and then refresh torbox3uiot6wchz.onion.sh.</p>
<p>If your clock is already set to the right time, the website is likely misconfigured, and there is nothing you can do to resolve the issue. You can notify the website’s administrator about the problem.<br />
 does anybody have the same issue?<br />
Thanks<br />
Learn more…</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287979"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287979" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2020</p>
    </div>
    <a href="#comment-287979">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287979" class="permalink" rel="bookmark">not work https://speed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>not work <a href="https://speed.cloudflare.com/" rel="nofollow">https://speed.cloudflare.com/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287991"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287991" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
    <a href="#comment-287991">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287991" class="permalink" rel="bookmark">Censored Turkey exit 185.65…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Censored Turkey exit 185.65.205.98 makes loading indefinite and prevents tor from normal operation.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288006"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288006" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Derek Plymouth (not verified)</span> said:</p>
      <p class="date-time">May 31, 2020</p>
    </div>
    <a href="#comment-288006">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288006" class="permalink" rel="bookmark">German node IP 185.220.XXX…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>German node IP 185.220.XXX.XXX getting used too many times - Randomization of Tor node is affected.</p>
<p>Not the Guard Node that is. Talking about the Exit Node.</p>
</div>
  </div>
</article>
<!-- Comment END -->
