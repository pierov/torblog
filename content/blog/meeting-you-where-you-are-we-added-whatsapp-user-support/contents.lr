title: Meeting You Where You Are: We've added WhatsApp User Support
---
author: pavel
---
pub_date: 2023-02-23
---
categories: 

circumvention
community
human rights
usability
---
summary: Providing necessary tools for those who need it the most is one of our top priorities. That's why we've added WhatsApp User Support.
---
body:

Providing support and necessary tools for those who find themselves in places where Tor access is restricted has become one of our top priorities. And as our community continues to grow, we are constantly looking for ways that make it easier and safer to connect with us. That's why, over the last two years, we have been working on improving Tor online user support: from launching the [Tor Forum](https://blog.torproject.org/tor-forum-a-new-discussion-platform/) and bridging [Tor IRC channels to Matrix](https://blog.torproject.org/entering-the-matrix/) in 2021 to rolling out support channels on messaging apps like [Telegram](https://newsletter.torproject.org/archive/2022-09-07-telegram-tor-board-arti-1-0-0/text/) and [Signal](https://forum.torproject.net/t/tor-user-support-a-new-encrypted-way-to-help-tor-users-via-signal/4428) in 2022.

Today, we are excited to announce the addition of our [WhatsApp user support](https://wa.me/447421000612) -- providing another option for users across the world to get in touch with us.

## WhatsApp Support Now Live

Message us on [WhatsApp](https://wa.me/447421000612) if you have trouble accessing the Tor Browser or have support questions when services are blocked in your country. You will be able to connect with a real user support agent via text message -- video and voice are currently not supported.

For the fastest response times, please consider writing in English, Russian, Spanish, and/or Portuguese if you can. Of course, you can contact us in a language you feel most comfortable with, but please keep in mind that it will take us longer to get back with an answer while we work on a translation.

## How to Best Engage With Us

Our aim is to resolve any issues as they arise with a priority on helping those that need our resources the most. To make this experience as successful as it can be, we have mapped out a couple of different scenarios that allow you to get our attention fast for whatever circumstance you find yourself in.

1. **If you find yourself in geographies with unrestricted access to the internet and want to connect with us, please consider using the forum and other public Tor channels.**
  - **Tor Forum:** Our [forum](https://forum.torproject.net/) is publicly available and serves as a repository of verified information around Tor services, including common support issues and questions, general discussions, events and news.
  - **Tor IRC bridge to Matrix:** As our community kept growing, we wanted to find ways to make the user experience more accessible. By bridging our [IRC community to the Matrix platform  ](https://blog.torproject.org/entering-the-matrix/)contributors can use a chat client or app to join and follow our discussions more easily.

2. **If you find yourself in regions that are experiencing censorship and where access to Tor has been blocked, we recommend using our messaging channels to get in touch with us.**
  - **Telegram:** We have multiple channels on [Telegram](https://t.me/TorProject) that allow our community to request bridges to access the Tor network or Tor news, and get assistance in downloading the Tor Browser. If you need help with circumventing censorship, please select from the menu options which region you are connecting from as it will allow us to follow up more quickly.
  - **Signal:** For end-to-end encrypted support, users can contact our official [Signal channel](https://signal.me/#p/+17787431312) by sending text messages to the number [+17787431312](https://signal.me/#p/+17787431312). Similar to our other text messaging channels, video and voice calls are currently not supported. After sending a message, our support agents will guide you and help troubleshoot your issue.
  - **Email:** For users living in countries where Tor Project domains are blocked, like China, Iran, Turkey, Egypt, and others, we'll continue providing support by email via [frontdesk@torproject.org](mailto:frontdestk@torproject.org).

To ensure that you receive the best and safest support from us, we encourage you to verify that you're connecting with us through our official Tor Projet channels. You can find the full list of legitimate links, email-addresses and numbers on our [official support page](https://tb-manual.torproject.org/support/), in the help section of the Tor Browser, or simply by typing 'about:manual#support' in the URL Bar. Please always take a moment to double-check before sharing any sensitive information.