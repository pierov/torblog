title: Pride and Privacy
---
pub_date: 2019-06-05
---
author: al
---
tags: The Tor Project, Pride
---
_html_body:

<p>Fifty years ago, in New York City’s Greenwich Village, the NYPD raided a gay bar. This wasn’t a new occurrence. Patrons at the Stonewall Inn were familiar with being targeted by the police, arrested for “cross dressing,” hassled for being gender nonconforming or queer or transgender or otherwise outside of the rigid expectations around morality.</p>
<p>Something different happened, though, on June 28, 1969. The patrons at the Stonewall Inn resisted the police raid and fought back. Their resistance sparked a week of protests and decades of collaborative activism in the name of LGBTQ+ recognition and safety.</p>
<p>In the fifty years since the resistance at Stonewall, the global LGBTQ+ community has fought for, and in some cases won, important visibility and protections.</p>
<p>Pride and the month of June can serve as a reminder of this progress. Pride can be an opportunity to celebrate without shame. Pride can be a reminder that all people deserve to safely gather, build community, swap resources, tell their stories, and be unapologetically who they are. </p>
<p>Pride can also be an opportunity to reflect on how many LGBTQ+ people still face prejudice, injustice, and violence. In the same way safety was not a given in 1969 at the Stonewall Inn, safety is still not a given for LGBTQ+ people in 2019, particularly in places where there are negative social stigmas around being queer or transgender. </p>
<p>Today, despite our progress, <a href="https://smallmedia.org.uk/media/projects/files/BreakingTheSilence_2018.pdf">not all LGBTQ+ people can be public about their identities</a>. Many people still face extreme consequences for being who they are, and risk being abandoned by family and friends, harassed, fired, deported, jailed, or killed for being out—or outed. The internet is not safe from these consequences.<a href="https://eipr.org/en/publications/trap-punishing-sexual-difference-egypt"> In fact, the proliferation of dating sites and social media has been used to track down and entrap LGBTQ+ people.</a></p>
<p>To put it simply, privacy can be critical to safety.</p>
<p>The fact that many LGBTQ+ people need a private, anonymous internet to communicate with their peers or find important resources without being tracked and outed is one of the many reasons why we do what we do at the Tor Project.</p>
<p><img alt="Tor stories, transgender user quote" src="/static/images/blog/inline-images/tor-stories-transgender_0.png" width="500" class="align-center" /></p>
<p>That is why we monitor the availability of LGBTQ+ sites in <a href="https://ooni.torproject.org/">OONI</a> tests, so we can better understand which countries are censoring these sites and who needs circumvention technology. That’s why we travel to countries where governments outlaw or punish being LGBTQ+ and lead workshops for community organizations about how to protect their privacy online. That’s why we partner with LGBTQ+, feminist, sex worker, and human rights groups to ensure that we’re learning about how they use privacy tech, and what they need from Tor in order to stay safe when using the internet.</p>
<p>We are proud that our tools can serve the LGBTQ+ community. We hope that by offering a way to privately access the internet, allowing people to get online without fear, that we can communicate with one another to change the world. We all deserve to live in a world where we can express who we are without shame.</p>
<p>This June and year round, the Tor Project stands in solidarity with the LGBTQ+ community. Happy Pride!</p>

